-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 08, 2015 at 12:24 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bdgcente_leave`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_approve_desc`
--

CREATE TABLE IF NOT EXISTS `t_approve_desc` (
  `approve_id` int(11) NOT NULL,
  `approve_desc` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_approve_desc`
--

INSERT INTO `t_approve_desc` (`approve_id`, `approve_desc`) VALUES
(0, 'รออนุมัติ'),
(1, 'อนุมัติแล้ว'),
(2, 'ไม่อนุมัติ');

-- --------------------------------------------------------

--
-- Table structure for table `t_company`
--

CREATE TABLE IF NOT EXISTS `t_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_name` varchar(255) NOT NULL,
  `comp_logo` varchar(255) NOT NULL,
  `comp_address` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `leave_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `leave_group_id` (`leave_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `t_company`
--

INSERT INTO `t_company` (`id`, `comp_name`, `comp_logo`, `comp_address`, `create_date`, `leave_group_id`) VALUES
(19, 'บริษัท อังกฤษตรางู (แอล.พี.) จำกัด', '', 'สำนักงานใหญ่\n100/105-108 อาคารว่องวานิช บี ชั้น 31 ถ.พระราม 9\nแขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10320 ', '2014-12-23 06:37:26', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_dept`
--

CREATE TABLE IF NOT EXISTS `t_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `t_dept`
--

INSERT INTO `t_dept` (`id`, `dept_id`, `dept_name`) VALUES
(1, 1, 'BD-HO'),
(2, 2, 'BD-BD120'),
(3, 3, 'BD-Inter'),
(4, 4, 'BD-Oem'),
(5, 5, 'BD-Trading'),
(6, 6, 'BD-MIS'),
(7, 7, 'H/0'),
(8, 8, 'Marketing-PC'),
(9, 9, 'CRSM'),
(10, 10, 'Marketing-HC'),
(11, 11, 'Sales-HC'),
(12, 12, 'Fin&Acct'),
(13, 13, 'Financial'),
(14, 14, 'Accounting'),
(15, 15, 'MIS'),
(16, 16, 'Admin'),
(17, 17, 'Corporate'),
(18, 18, 'Art'),
(19, 19, 'HR'),
(20, 20, 'Purchasing'),
(21, 21, 'R&NPD'),
(22, 22, 'Marketing-PCL');

-- --------------------------------------------------------

--
-- Table structure for table `t_employee`
--

CREATE TABLE IF NOT EXISTS `t_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(50) NOT NULL,
  `emp_lastname` varchar(100) NOT NULL,
  `emp_email` varchar(100) NOT NULL,
  `emp_password` varchar(10) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `approver_id` int(11) NOT NULL,
  `emp_status` int(11) NOT NULL,
  `working_startdate` date NOT NULL,
  `flag_change_pass` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `t_employee`
--

INSERT INTO `t_employee` (`id`, `emp_id`, `emp_name`, `emp_lastname`, `emp_email`, `emp_password`, `comp_id`, `role_id`, `level_id`, `dept_id`, `approver_id`, `emp_status`, `working_startdate`, `flag_change_pass`) VALUES
(1, 530, 'อารีย์', 'สังข์สอน', 'aree_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 1, 108, 1, '2003-01-02', 0),
(2, 2514, 'สุวิมล', 'โสภณนรินทร์', 'suwimol_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 2, 132, 1, '2014-01-02', 0),
(3, 347, 'ทวิช', 'เกียรติวรางกูร', 'tony@britishdispensary.com', 'bdlp1234', 1, 1, 0, 3, 132, 1, '1996-07-11', 0),
(4, 1614, 'ศิริอัปสร', 'อุดมยันต์', 'siriupsorn_u@britishdispensary.com', 'bdlp1234', 1, 1, 0, 3, 347, 1, '2002-08-19', 0),
(5, 1960, 'HE', 'NA', 'he_n@britishdispensary.com', 'bdlp1234', 1, 1, 0, 3, 347, 1, '2006-06-05', 0),
(6, 2124, 'ยุพาพร', 'พรมท้าว', 'yupaporn_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 2476, 1, '2009-08-10', 0),
(7, 2173, 'กนกอร', 'มนตรี', 'kanokon_n@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 2476, 1, '2010-03-15', 0),
(8, 2446, 'สมพร', 'ศรแก้ว', 'somporn_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 2476, 1, '2013-05-02', 0),
(9, 2476, 'วลัยลักษณ์', 'ดำรงธรรมวุฒิ', 'walailuk_d@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 347, 1, '2013-07-15', 0),
(10, 2478, 'ปิยพันธ์', 'โฉมจิตร', 'piyapan_c@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 2476, 1, '2013-07-23', 0),
(11, 2488, 'เอื้อมพร', 'พิฤกษ์', 'auamporn_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 2476, 1, '2003-09-17', 0),
(12, 2555, 'ภิรมณ', 'จารุพงศ์เดชา', 'piramon_j@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 2476, 1, '2014-03-24', 0),
(13, 2684, 'รัตนา', 'ช่วงศรี', 'ratana_c@britishdispensary.com', 'bdlp1234', 1, 1, 0, 4, 2476, 1, '2015-02-11', 0),
(14, 1746, 'ทรงศิริ', 'กุโรรัตน์', 'songsiri_k@britishdispensary.com', 'bdlp1234', 1, 1, 0, 5, 2476, 1, '2003-05-06', 0),
(15, 2386, 'ธีรนุช', 'ปัทถาพงษ์', 'theeranuch_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 5, 2476, 1, '2012-09-03', 0),
(16, 2604, 'สุธาทิพย์', 'แสนเลิศ', 'suthathip@britishdispensary.com', 'bdlp1234', 1, 1, 0, 6, 1921, 1, '2008-07-21', 0),
(17, 2605, 'สราวุธ', 'เมืองธรรม', 'saravudh_m@britishdispensary.com', 'bdlp1234', 1, 1, 0, 6, 1921, 1, '2012-04-10', 0),
(18, 132, 'อนุรุธ', 'ว่องวานิช', 'anurut@britishdispensary.com', 'bdlp1234', 1, 1, 0, 7, 0, 1, '1985-01-01', 0),
(19, 147, 'ศิวิสา', 'นิธิฐิมณีรัตน์', 'sivisa_n@britishdispensary.com', 'bdlp1234', 1, 1, 0, 7, 132, 1, '1988-02-22', 0),
(20, 194, 'เพิ่มหญิง', 'ว่องวานิช', 'permying@britishdispensary.com', 'bdlp1234', 1, 1, 0, 7, 132, 1, '1990-10-01', 0),
(21, 2240, 'ยุพยง', 'บุลวัธนา', 'yupayong_b@britishdispensary.com', 'bdlp1234', 1, 1, 0, 7, 194, 1, '2010-12-20', 0),
(22, 2321, 'นิธินันท์', 'วชิรเศรษฐหิรัญ', 'nitinun_w@britishdispensary.com', 'bdlp1234', 1, 1, 0, 7, 132, 1, '2012-01-05', 0),
(23, 994, 'วิไลรัตน์', 'กาลายี', 'wilairatn_k@britishdispensary.com', 'bdlp1234', 1, 1, 0, 8, 194, 1, '2000-11-01', 0),
(24, 2046, 'ทวีรัชต์', 'สังขโย', 'thew_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 8, 994, 1, '2008-08-18', 0),
(25, 2170, 'อะรินทร์ดา', 'พงษ์วัน', 'arinda_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 8, 2187, 1, '2010-03-04', 0),
(26, 2187, 'มุทิตา', 'หลักชัยวิบูลย์', 'muthita_l@britishdispensary.com', 'bdlp1234', 1, 1, 0, 8, 194, 1, '2010-07-01', 0),
(27, 2596, 'รัชตา', 'เมฆปั่น', 'rachata_m@britishdispensary.com', 'bdlp1234', 1, 1, 0, 8, 994, 1, '2011-07-01', 0),
(28, 2606, 'มาศบดี', 'มณีสะอาด', 'mardbordee_m@britishdispensary.com', 'bdlp1234', 1, 1, 0, 8, 2187, 1, '2014-07-28', 0),
(29, 2406, 'อาภรณ์', 'ฐานานุวัฒน์', 'arporn_t@britishdispensary.com', 'bdlp1234', 1, 1, 0, 9, 194, 1, '1985-06-17', 0),
(30, 2407, 'จิรยุทธ', 'ลาภเจริญชัย', 'jirayuth_l@britishdispensary.com', 'bdlp1234', 1, 1, 0, 9, 2407, 1, '2003-04-03', 0),
(31, 2408, 'มงคล', 'จันทร์เขียว', 'mongkol_c@britishdispensary.com', 'bdlp1234', 1, 1, 0, 9, 2407, 1, '2006-01-01', 0),
(32, 2409, 'จักรวุธ', 'มุ้งทอง', 'jakrawut_m@britishdispensary.com', 'bdlp1234', 1, 1, 0, 9, 2407, 1, '2008-04-29', 0),
(33, 2597, 'สุทธิพงศ์', 'อุปพงศ์', 'suttiphong_o@britishdispensary.com', 'bdlp1234', 1, 1, 0, 9, 2407, 1, '2010-11-01', 0),
(34, 2598, 'นงเยาว์', 'มีเหาะ', 'nongyao_m@britishdispensary.com', 'bdlp1234', 1, 1, 0, 9, 2407, 1, '2010-11-01', 0),
(35, 2599, 'ณกีรดา', 'จิตรรุ่งเรือง', 'nakirada_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 9, 2407, 1, '2011-01-17', 0),
(36, 1723, 'วงษ์เดือน', 'สมวงษ์', 'wongduan_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 10, 2189, 1, '2003-03-27', 0),
(37, 2174, 'นิสิตา', 'คมสาคร', 'nisita_k@britishdispensary.com', 'bdlp1234', 1, 1, 0, 10, 2189, 1, '2010-03-15', 0),
(38, 2189, 'บุญช่วย', 'ว่องประพิณกุล', 'boonchuay@britishdispensary.com', 'bdlp1234', 1, 1, 0, 10, 194, 1, '2010-07-20', 0),
(39, 2338, 'อัญญาวรรณ', 'นุสันรัมย์', 'unyavan.n@gmail.com', 'bdlp1234', 1, 1, 0, 10, 2174, 1, '2012-02-27', 0),
(40, 2449, 'พิชญา', 'บุญญะพานิชสกุล', 'pitchaya_b@britishdispensary.com', 'bdlp1234', 1, 1, 0, 10, 2174, 1, '2013-05-07', 0),
(41, 2487, 'ฉัตรวิไล', 'วรธำรง', 'chatwilai_v@britishdispensary.com', 'bdlp1234', 1, 1, 0, 10, 2174, 1, '2013-08-01', 0),
(42, 2319, 'สุชาติ', 'โฆษิตารัตน์', 'suchart_k@britishdispensary.com', 'bdlp1234', 1, 1, 0, 11, 2189, 1, '2011-09-01', 0),
(43, 2346, 'ธัญรดา', 'สงวนยวง', 'thunrada_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 11, 2189, 1, '2012-03-15', 0),
(44, 2378, 'รัศมี', 'ฉายาชวลิต', 'russamee_c@britishdispensary.com', 'bdlp1234', 1, 1, 0, 11, 2189, 1, '2012-07-23', 0),
(45, 90, 'ภิรัตน์', 'อังสุโวทัย', 'pirat@britishdispensary.com', 'bdlp1234', 1, 1, 0, 12, 194, 1, '1978-11-04', 0),
(46, 69, 'โสภา', 'ศิรเลิศมุกุล', 'sopa_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 13, 239, 1, '1973-02-10', 0),
(47, 95, 'สนั่น', 'หาญศิริพงษ์สกุล', 'sanan_h@britishdispensary.com', 'bdlp1234', 1, 1, 0, 13, 239, 1, '1979-02-01', 0),
(48, 239, 'พรพิมล', 'ธนะสีลังกูร', 'pornpimon_t@britishdispensary.com', 'bdlp1234', 1, 1, 0, 13, 90, 1, '1993-05-21', 0),
(49, 1780, 'เยาวนาถ', 'นิพาภรณ์', 'yaowanart_n@britishdispensary.com', 'bdlp1234', 1, 1, 0, 13, 239, 1, '2003-11-10', 0),
(50, 2228, 'ศิริพร', 'เสนีย์', 'senee1981@hotmail.com', 'bdlp1234', 1, 1, 0, 13, 239, 1, '2010-12-01', 0),
(51, 2513, 'กัญญาวุฒิ', 'ปัญญาวุฒิ', 'kanyawut_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 13, 239, 1, '2014-01-02', 0),
(52, 108, 'ปิยนุช', 'บูรณพงษ์ศํกดิ์', 'piyanuch_b@britishdispensary.com', 'bdlp1234', 1, 1, 0, 14, 90, 1, '1981-02-02', 0),
(53, 2236, 'นภาลัย', 'วรญาโณ', 'naphalai_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 14, 108, 1, '2010-12-15', 0),
(54, 2245, 'ภาธินี', 'จงมีทรัพย์มาก', 'phathini_c@britishdispensary.com', 'bdlp1234', 1, 1, 0, 14, 108, 1, '2011-01-04', 0),
(55, 2374, 'ไพฑูรย์', 'พุ่มพงษ์', 'pitoon_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 14, 108, 1, '2012-06-11', 0),
(56, 2473, 'จุรีรัตน์', 'แซ่อุ้ย', 'jureerat_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 14, 108, 1, '2013-06-17', 0),
(57, 2580, 'ปุณยนุช', 'ตั้งวงศ์สุคนธ์', 'punyanuch_t@britishdispensary.com', 'bdlp1234', 1, 1, 0, 14, 90, 1, '2010-08-02', 0),
(58, 1921, 'อัครพล', 'นุกูลวุฒิโอภาส', 'akarapon_n@britishdispensary.com', 'bdlp', 1, 2, 0, 15, 90, 1, '2005-09-19', 1),
(59, 2250, 'สุชีรา', 'นาชะนาง', 'sucheera_n@britishdispensary.com', 'bdlp1234', 1, 1, 0, 15, 1921, 1, '2011-02-07', 0),
(60, 93, 'ผ่องพรรณ', 'ตติยารัตน์', 'pongphan@britishdispensary.com', 'bdlp1234', 1, 1, 0, 16, 194, 1, '1978-09-01', 0),
(61, 1145, 'ทัศนีย์', 'ธรรมศักดิ์', 'tassanee_k@britishdispensary.com', 'bdlp1234', 1, 1, 0, 17, 93, 1, '2001-04-02', 0),
(62, 214, 'สามารถ', 'โสภา', 'samart_s@britishdispensary.com', 'martmai', 1, 1, 0, 18, 93, 1, '1991-10-21', 0),
(63, 1888, 'อำนาจ', 'บัวแก้ว', 'amnat_b@britishdispensary.com', 'bdlp1234', 1, 1, 0, 18, 214, 1, '2005-05-30', 0),
(64, 2379, 'ทศพร', 'สงวนสัตย์', 'sa_ngvansat27@hotmail.com', 'bdlp1234', 1, 1, 0, 18, 214, 1, '2012-08-01', 0),
(65, 1791, 'ชุติกาญจน์', 'วัฒนา', 'chutikarn_w@britishdispensary.com', 'bdlp1234', 1, 1, 0, 19, 93, 1, '2004-04-01', 0),
(66, 2221, 'ปัญจรัตน์', 'จันทร์ศรีสุขใจ', 'punjarat_j@britishdispensary.com', 'tony4969', 1, 1, 0, 19, 1791, 1, '2010-11-15', 1),
(67, 2345, 'พรพรรณ', 'พลโลก', 'rabbit-p@windowslive.com', 'pon1601252', 1, 1, 0, 19, 1791, 1, '2012-03-12', 1),
(68, 2360, 'ทวีทรัพย์', 'กิ่งมะลิ', 'thaweesup_k@britishdispensary.com', 'bdlp1234', 1, 1, 0, 19, 1791, 1, '2012-05-02', 0),
(69, 142, 'วิไล', 'พงษ์เวียง', 'wilai_p@britishdispensary.com', 'bdlp1234', 1, 1, 0, 20, 93, 1, '1987-01-12', 0),
(70, 2103, 'ระพีพร', 'บาลดี', 'rapeeporn_b@britishdispensary.com', 'bdlp1234', 1, 1, 0, 20, 142, 1, '2009-04-01', 0),
(71, 208, 'ณรินทร์พร', 'จันนา', 'narinporn@britishdispensary.com', 'bdlp1234', 1, 1, 0, 21, 194, 1, '1991-06-25', 0),
(72, 1748, 'จิตติมา', 'จิตต์อารี', 'jittima_j@britishdispensary.com', 'bdlp1234', 1, 1, 0, 21, 208, 1, '2003-05-06', 0),
(73, 1800, 'อโนชา', 'ทองคำ', 'anocha_t@britishdispensary.com', 'bdlp1234', 1, 1, 0, 21, 208, 1, '2004-05-17', 0),
(74, 30045, 'ณัฐณิชา', 'ผลพิลา', 'natnicha_s@britishdispensary.com', 'bdlp1234', 1, 1, 0, 22, 994, 1, '2011-09-01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_hr_confirm`
--

CREATE TABLE IF NOT EXISTS `t_hr_confirm` (
  `id_stat` int(11) NOT NULL,
  `hr_confirm_desc` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_hr_confirm`
--

INSERT INTO `t_hr_confirm` (`id_stat`, `hr_confirm_desc`) VALUES
(0, 'รอ'),
(1, 'รับทราบ');

-- --------------------------------------------------------

--
-- Table structure for table `t_leave_group_comp`
--

CREATE TABLE IF NOT EXISTS `t_leave_group_comp` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_id` int(11) NOT NULL,
  `leave_type_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_leave_group_comp`
--

INSERT INTO `t_leave_group_comp` (`group_id`, `comp_id`, `leave_type_id`, `create_date`) VALUES
(1, 1, 1, '2015-02-09 04:16:45'),
(2, 1, 2, '2015-02-09 04:16:45'),
(4, 1, 3, '2015-02-09 04:17:00'),
(5, 1, 4, '2015-02-16 02:36:08');

-- --------------------------------------------------------

--
-- Table structure for table `t_leave_prorate`
--

CREATE TABLE IF NOT EXISTS `t_leave_prorate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leave_type_id` int(11) NOT NULL,
  `start_month` int(11) NOT NULL,
  `prorate_leave_day` float(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `t_leave_prorate`
--

INSERT INTO `t_leave_prorate` (`id`, `leave_type_id`, `start_month`, `prorate_leave_day`) VALUES
(1, 1, 1, 6.00),
(2, 1, 2, 5.50),
(3, 1, 3, 5.00),
(4, 1, 4, 4.50),
(5, 1, 5, 4.00),
(6, 1, 6, 3.50),
(7, 1, 7, 3.00),
(8, 1, 8, 2.50),
(9, 1, 9, 2.00),
(10, 1, 10, 1.50),
(11, 1, 11, 1.00),
(12, 1, 12, 0.50),
(13, 3, 1, 6.00),
(15, 3, 2, 5.50),
(16, 3, 3, 5.00),
(17, 3, 4, 4.50),
(18, 3, 5, 4.00),
(19, 3, 6, 3.50),
(20, 3, 7, 3.00),
(21, 3, 8, 2.50),
(22, 3, 9, 2.00),
(23, 3, 10, 1.50),
(24, 3, 11, 1.00),
(25, 3, 12, 0.50);

-- --------------------------------------------------------

--
-- Table structure for table `t_leave_time`
--

CREATE TABLE IF NOT EXISTS `t_leave_time` (
  `time_id` int(11) NOT NULL AUTO_INCREMENT,
  `time_slot` decimal(4,2) NOT NULL,
  PRIMARY KEY (`time_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `t_leave_time`
--

INSERT INTO `t_leave_time` (`time_id`, `time_slot`) VALUES
(6, '8.00'),
(7, '8.30'),
(8, '9.00'),
(9, '9.30'),
(10, '10.00'),
(11, '10.30'),
(12, '11.00'),
(13, '11.30'),
(14, '12.00'),
(15, '13.00'),
(16, '13.30'),
(17, '14.00'),
(18, '14.30'),
(19, '15.00'),
(20, '15.30'),
(21, '16.00'),
(22, '16.30'),
(23, '17.00'),
(24, '17.30'),
(25, '18.00');

-- --------------------------------------------------------

--
-- Table structure for table `t_leave_transaction`
--

CREATE TABLE IF NOT EXISTS `t_leave_transaction` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `leave_date` datetime NOT NULL,
  `emp_id` int(11) NOT NULL,
  `submit_stat` int(11) NOT NULL DEFAULT '0',
  `reject_stat` int(11) NOT NULL DEFAULT '0',
  `reject_date` date NOT NULL,
  `approve_stat` int(11) NOT NULL DEFAULT '0',
  `approve_date` date NOT NULL,
  `hr_confrim_stat` int(11) NOT NULL DEFAULT '0',
  `leave_type_id` int(11) NOT NULL,
  `working_hour_id` int(11) NOT NULL,
  `start_leave` decimal(4,2) NOT NULL,
  `end_leave` decimal(4,2) NOT NULL,
  `leave_reason` varchar(255) NOT NULL,
  `leave_hour` decimal(4,2) NOT NULL,
  `cancel_leave_reason` varchar(255) NOT NULL,
  `reject_leave_reason` varchar(255) NOT NULL,
  PRIMARY KEY (`trans_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=248 ;

--
-- Dumping data for table `t_leave_transaction`
--

INSERT INTO `t_leave_transaction` (`trans_id`, `create_date`, `leave_date`, `emp_id`, `submit_stat`, `reject_stat`, `reject_date`, `approve_stat`, `approve_date`, `hr_confrim_stat`, `leave_type_id`, `working_hour_id`, `start_leave`, `end_leave`, `leave_reason`, `leave_hour`, `cancel_leave_reason`, `reject_leave_reason`) VALUES
(160, '2015-02-23 04:02:11', '2015-01-09 00:00:00', 2103, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '16.00', '17.30', 'ไปต่างจังหวัด', '1.50', '', ''),
(161, '2015-02-23 04:02:25', '2015-01-12 00:00:00', 2103, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ไปต่างจังหวัด', '8.00', '', ''),
(162, '2015-02-23 04:02:39', '2015-01-23 00:00:00', 2103, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'พามารดาไปพบแพทย์', '8.00', '', ''),
(163, '2015-02-23 04:02:55', '2015-03-03 00:00:00', 2103, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'พบแพทย์', '8.00', '', ''),
(164, '2015-02-23 04:25:11', '2015-01-06 00:00:00', 142, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ลูกรับปริญญา', '8.00', '', ''),
(165, '2015-02-23 04:24:58', '2015-02-18 00:00:00', 142, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'เทศกาลตรุษจีน', '8.00', '', ''),
(166, '2015-02-23 04:25:35', '2015-02-03 00:00:00', 1145, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'ลากิจ', '8.00', '', ''),
(167, '2015-02-23 04:25:23', '2015-02-18 00:00:00', 1145, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ลาพักร้อน', '8.00', '', ''),
(168, '2015-04-03 02:51:22', '2015-01-05 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'ธุระที่ต่างจังหวัด', '8.00', '', ''),
(169, '2015-04-03 02:51:14', '2015-01-26 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'ธุระที่ต่างจังหวัด', '8.00', '', ''),
(170, '2015-04-03 02:51:03', '2015-02-03 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(171, '2015-04-03 02:50:55', '2015-02-11 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(172, '2015-04-03 02:50:48', '2015-02-16 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(173, '2015-04-03 02:51:44', '2015-01-28 00:00:00', 2360, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.30', 'มาสาย', '1.00', '', ''),
(174, '2015-04-03 02:50:29', '2015-01-20 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.30', '', '1.00', '', ''),
(175, '2015-04-03 02:50:19', '2015-01-21 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 8, '8.30', '17.30', 'พบหมอ', '8.00', '', ''),
(176, '2015-04-03 02:50:05', '2015-02-18 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'พักร้อน', '8.00', '', ''),
(177, '2015-04-03 02:49:52', '2015-02-16 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'เข้าสาย', '0.50', '', ''),
(178, '2015-02-24 08:06:39', '2015-02-23 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'เข้าสาย', '0.50', '', ''),
(179, '2015-02-24 08:05:28', '2015-02-23 00:00:00', 1791, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 2, 8, '8.30', '17.30', 'ปวดฟัน', '8.00', '', ''),
(180, '2015-03-02 06:51:02', '2015-01-06 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 5, '13.00', '17.30', 'เฝ้าย่าที่โรงพยาบาล', '4.50', '', ''),
(181, '2015-03-02 06:50:49', '2015-01-23 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 0, '8.30', '9.30', 'ป่วย', '1.00', '', ''),
(182, '2015-03-02 06:50:32', '2015-01-23 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 0, '8.30', '9.30', 'ป่วย', '1.00', '', ''),
(183, '2015-03-02 06:49:18', '2015-01-05 00:00:00', 214, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ไปต่างจังหวัด', '8.00', '', ''),
(184, '2015-03-02 07:07:42', '2015-01-26 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 8, '8.30', '17.30', 'ไมแกน', '8.00', '', ''),
(185, '2015-03-02 07:07:30', '2015-01-30 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 0, '8.30', '9.30', 'ป่วย', '1.00', '', ''),
(186, '2015-03-02 07:07:13', '2015-02-02 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 0, '8.30', '9.00', 'สาย', '0.50', '', ''),
(187, '2015-03-02 07:07:01', '2015-02-03 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 0, '8.30', '9.00', 'สาย', '0.50', '', ''),
(188, '2015-03-05 04:05:13', '2015-02-17 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'เฝ้าย่าที่โรงพยาบาล', '8.00', '', ''),
(189, '2015-03-05 04:05:03', '2015-02-18 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'เฝ้าย่าที่โรงพยาบาล', '8.00', '', ''),
(190, '2015-03-05 04:04:53', '2015-02-23 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 0, '8.30', '9.00', 'สาย', '0.50', '', ''),
(191, '2015-03-02 10:09:11', '2015-02-25 00:00:00', 2379, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 0, '8.30', '9.00', 'สาย', '0.50', '', ''),
(192, '2015-04-03 02:48:37', '2015-03-02 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'เข้างานสาย', '0.50', '', ''),
(193, '2015-03-05 04:07:10', '2015-02-09 00:00:00', 214, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'นัดช่างทำฝ้าบ้านและประตูรั้วบ้าน', '8.00', '', ''),
(194, '2015-03-05 04:08:46', '2015-02-13 00:00:00', 214, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.30', 'รถติดมากมาสายครับ', '1.00', '', ''),
(195, '2015-03-06 06:50:13', '2015-03-09 00:00:00', 1921, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 5, '13.00', '17.30', 'test tset', '4.50', '', ''),
(196, '2015-03-09 02:39:52', '2015-03-09 00:00:00', 1888, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.30', 'แสกนนิ้วเข้าทำงานสาย', '1.00', '', ''),
(197, '2015-04-03 02:48:28', '2015-03-10 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'เข้างานสาย', '0.50', '', ''),
(198, '2015-03-13 02:51:13', '2015-03-16 00:00:00', 2103, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'พาแม่ไปกิจธุระที่ต่างจังหวัด', '8.00', '', ''),
(199, '2015-04-03 02:50:37', '2015-03-18 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(200, '2015-03-19 06:18:56', '2015-03-20 00:00:00', 2221, 0, 1, '2015-03-19', 0, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'test---การย้าย server', '8.00', 'test-- ระบบ', ''),
(201, '2015-04-03 02:51:37', '2015-03-19 00:00:00', 2360, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.30', 'สาย', '1.00', '', ''),
(202, '2015-03-20 08:04:41', '2015-03-20 00:00:00', 2221, 0, 1, '2015-03-20', 0, '0000-00-00', 0, 5, 5, '13.00', '17.30', 'ทดสอบ-บันทึกการไปทำงานนอกสถานที่', '4.50', 'ทดสอบระบบ', ''),
(203, '2015-03-20 08:03:27', '2015-03-20 00:00:00', 2221, 0, 1, '2015-03-20', 0, '0000-00-00', 0, 6, 4, '8.30', '12.00', 'ลืมบันทึกเวลาเข้างาน', '3.50', 'ทดสอบระบบ', ''),
(204, '2015-03-23 09:11:59', '2015-03-23 00:00:00', 2221, 0, 1, '2015-03-23', 0, '0000-00-00', 0, 5, 0, '14.00', '15.00', 'Test -- ไปสำนักงานประกันสังคม', '1.00', 'ยกเลิก test', ''),
(205, '2015-04-03 02:48:16', '2015-04-03 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'เข้างานสาย', '0.50', '', ''),
(206, '2015-06-23 06:51:58', '2015-04-10 00:00:00', 2360, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '13.00', '17.30', 'เดินทางกลับต่างจังหวัด', '4.50', '', ''),
(207, '2015-04-16 05:45:40', '2015-04-07 00:00:00', 2221, 0, 1, '2015-04-16', 0, '0000-00-00', 0, 5, 0, '8.30', '9.30', 'ทดสอบการแจ้งเตือนทาง email', '1.00', 'ทดสอบระบบ', ''),
(208, '2015-06-23 06:51:16', '2015-04-30 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(209, '2015-06-23 06:51:06', '2015-05-07 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(210, '2015-06-11 09:17:14', '2015-04-17 00:00:00', 2221, 0, 1, '2015-06-11', 0, '0000-00-00', 0, 2, 5, '13.00', '17.30', 'พบหมอ', '4.50', 'ยกเลิก', ''),
(211, '2015-06-23 06:50:13', '2015-05-11 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'ทำกิจธุระที่ต่างจังหวัด', '8.00', '', ''),
(212, '2015-06-23 06:50:01', '2015-05-15 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '15.00', '17.30', 'ลาพักร้อน', '2.50', '', ''),
(213, '2015-06-23 06:49:48', '2015-05-28 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ลาพักร้อน', '8.00', '', ''),
(214, '2015-06-23 06:49:35', '2015-05-29 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ลาพักร้อน', '8.00', '', ''),
(215, '2015-06-23 06:50:58', '2015-05-15 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(216, '2015-06-23 06:51:47', '2015-05-27 00:00:00', 2360, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'งานศพ', '8.00', '', ''),
(217, '2015-05-29 02:39:31', '2015-05-15 00:00:00', 2345, 0, 1, '2015-05-29', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', 'เขียนวันที่ในการลาผิดวัน', ''),
(218, '2015-06-23 06:50:48', '2015-05-29 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(219, '2015-06-23 05:57:36', '2015-06-08 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 2, 8, '8.30', '17.30', 'ไม่สบายเป็นไข้', '8.00', '', ''),
(220, '2015-06-23 06:50:36', '2015-06-16 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(221, '2015-06-23 06:50:24', '2015-06-16 00:00:00', 2345, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(222, '2015-06-18 01:42:23', '2015-06-17 00:00:00', 2360, 0, 1, '2015-06-18', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', 'ทางบริษัทฯอนุโลมให้ค่ะ (เนื่องจากฝนตก)', ''),
(223, '2015-06-23 05:57:18', '2015-06-19 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 1, 8, '8.30', '17.30', 'ทำธุระที่ต่างจังหวัด', '8.00', '', ''),
(224, '2015-06-23 05:57:05', '2015-06-22 00:00:00', 2221, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'เข้างานสาย', '0.50', '', ''),
(225, '2015-06-23 06:51:28', '2015-06-22 00:00:00', 2360, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(226, '2015-06-23 08:19:13', '2015-07-03 00:00:00', 2360, 0, 0, '0000-00-00', 1, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ย้ายทะเบียนบ้าน', '8.00', '', ''),
(227, '2015-07-02 08:32:30', '2015-07-24 00:00:00', 1888, 0, 1, '2015-07-02', 0, '0000-00-00', 0, 3, 5, '13.00', '17.30', 'กลับบ้านไปรับพ่อแม่มางานรับหมวกน้องสาว', '4.50', 'เขียนลาผิดเดือน', ''),
(228, '2015-07-02 08:24:44', '2015-06-26 00:00:00', 1888, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'ไปงานรับหมวกน้องสาว', '8.00', '', ''),
(229, '2015-07-02 08:35:32', '2015-06-24 00:00:00', 1888, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 5, '13.00', '17.30', 'กลับบ้านไปรับพ่อแม่มางานรับหมวกน้องสาว', '4.50', '', ''),
(230, '2015-07-03 02:07:08', '2015-06-30 00:00:00', 2345, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(231, '2015-07-03 02:07:53', '2015-07-03 00:00:00', 2345, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(232, '2015-07-06 01:51:45', '2015-07-06 00:00:00', 2345, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(233, '2015-07-06 03:13:35', '2015-07-02 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(234, '2015-07-06 03:14:23', '2015-07-02 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '17.00', '17.30', 'เดินทางกลับต่างจังหวัด', '0.50', '', ''),
(235, '2015-07-06 05:44:04', '2015-07-06 00:00:00', 2221, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'เข้างานสาย', '0.50', '', ''),
(236, '2015-07-16 07:07:12', '2015-07-08 00:00:00', 2345, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 1, 0, '8.30', '11.00', 'พาแม่ไปโรงพยาบาล', '2.50', '', ''),
(237, '2015-07-16 07:07:41', '2015-07-16 00:00:00', 2345, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(238, '2015-07-16 10:04:50', '2015-07-01 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 6, 4, '8.30', '12.00', 'ลืมสแกนนิ้วค่ะ', '3.50', '', ''),
(239, '2015-08-03 01:36:06', '2015-07-31 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 2, 0, '13.30', '17.30', 'มีไข้ - เวียนศีรษะ', '4.00', '', ''),
(240, '2015-08-03 02:58:47', '2015-08-03 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 1, 0, '8.30', '9.00', 'มาสาย', '0.50', '', ''),
(241, '2015-08-05 09:39:27', '2015-08-13 00:00:00', 2345, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'กลับบ้านต่างจังหวัด', '8.00', '', ''),
(242, '2015-08-05 09:40:03', '2015-08-14 00:00:00', 2345, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'กลับบ้านต่างจังหวัด', '8.00', '', ''),
(243, '2015-08-05 09:43:41', '2015-08-14 00:00:00', 2345, 0, 1, '2015-08-05', 0, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'กลับบ้านต่างจังหวัด', '8.00', 'บันทึกข้อมูลซ้ำ ', ''),
(244, '2015-08-13 09:20:08', '2015-08-13 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 1, 0, '8.30', '9.00', 'สาย', '0.50', '', ''),
(245, '2015-08-17 10:17:57', '2015-08-17 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 1, 0, '8.30', '9.00', 'สาย', '0.50', '', ''),
(246, '2015-08-17 10:19:58', '2015-08-21 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 3, 8, '8.30', '17.30', 'กลับต่างจังหวัด', '8.00', '', ''),
(247, '2015-09-01 05:54:11', '2015-09-01 00:00:00', 2360, 0, 0, '0000-00-00', 0, '0000-00-00', 0, 1, 0, '8.30', '9.30', 'มาสาย', '1.00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_leave_type`
--

CREATE TABLE IF NOT EXISTS `t_leave_type` (
  `type_id` int(11) NOT NULL,
  `leave_description` varchar(100) NOT NULL,
  `bonus_limit` float(5,2) NOT NULL,
  `legal_limit` float(5,2) NOT NULL,
  `fav_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_leave_type`
--

INSERT INTO `t_leave_type` (`type_id`, `leave_description`, `bonus_limit`, `legal_limit`, `fav_flag`) VALUES
(1, 'ลากิจ', 24.00, 48.00, 1),
(2, 'ลาป่วย', 40.00, 240.00, 1),
(3, 'ลาพักร้อน', 48.00, 48.00, 1),
(4, 'ลาคลอดบุตร', 240.00, 720.00, 1),
(5, 'ปฏิบัติงานนอกสถานที่', 0.00, 0.00, 0),
(6, 'ลืมบันทึกเวลาเข้า-ออก', 0.00, 0.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_level`
--

CREATE TABLE IF NOT EXISTS `t_level` (
  `id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `level_description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_reject_desc`
--

CREATE TABLE IF NOT EXISTS `t_reject_desc` (
  `id_reject` int(2) NOT NULL,
  `id_desc` varchar(200) NOT NULL,
  PRIMARY KEY (`id_reject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_reject_desc`
--

INSERT INTO `t_reject_desc` (`id_reject`, `id_desc`) VALUES
(0, 'ยังไม่ยกเลิก'),
(1, 'ยกเลิก ก่อน ได้รับการอนุมัติ'),
(2, 'ยกเลิก หลัง ได้รับการอนุมัติแล้ว'),
(3, 'ฝ่ายบุคคลรับทราบ ยกเลิกการลา'),
(4, 'ฝ่ายบุคคล ไม่อนุมัติการยกเลิก');

-- --------------------------------------------------------

--
-- Table structure for table `t_user_role`
--

CREATE TABLE IF NOT EXISTS `t_user_role` (
  `role_id` int(11) NOT NULL,
  `role_description` varchar(50) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_user_role`
--

INSERT INTO `t_user_role` (`role_id`, `role_description`) VALUES
(1, 'user'),
(2, 'HR Admin'),
(3, 'HR Manager'),
(4, 'System Admin');

-- --------------------------------------------------------

--
-- Table structure for table `t_working_hour`
--

CREATE TABLE IF NOT EXISTS `t_working_hour` (
  `working_hour_id` int(11) NOT NULL AUTO_INCREMENT,
  `work_description` varchar(100) NOT NULL,
  `working_hour` decimal(11,2) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`working_hour_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `t_working_hour`
--

INSERT INTO `t_working_hour` (`working_hour_id`, `work_description`, `working_hour`, `create_date`) VALUES
(1, 'ลาทั้งวัน', '8.00', '2014-12-22 05:59:25'),
(2, 'ครึ่งวันเช้า', '3.50', '2014-12-24 04:53:34'),
(3, 'ครึ่งวันบ่าย', '4.50', '2014-12-24 04:53:34'),
(4, 'ลาเป็นรายชั่วโมง', '0.00', '2014-12-24 07:53:13');

-- --------------------------------------------------------

--
-- Table structure for table `t_year`
--

CREATE TABLE IF NOT EXISTS `t_year` (
  `id_year` int(11) NOT NULL AUTO_INCREMENT,
  `year` year(4) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `year_active` int(11) NOT NULL,
  PRIMARY KEY (`id_year`,`year`,`year_active`),
  UNIQUE KEY `year` (`year`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `t_year`
--

INSERT INTO `t_year` (`id_year`, `year`, `start_date`, `end_date`, `year_active`) VALUES
(1, 2014, '2014-01-01', '2014-12-31', 0),
(2, 2015, '2015-01-01', '2015-12-31', 1),
(5, 2016, '2016-01-01', '2016-12-31', 0),
(6, 2017, '2017-01-01', '2017-12-31', 0);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_approver_email`
--
CREATE TABLE IF NOT EXISTS `v_approver_email` (
`id` int(11)
,`emp_email` varchar(100)
,`emp_password` varchar(10)
,`emp_name` varchar(50)
,`emp_lastname` varchar(100)
,`emp_id` int(11)
,`approver_id` int(11)
,`approver_name` varchar(50)
,`approver_email` varchar(100)
,`role_id` int(11)
,`role_description` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_change_pass`
--
CREATE TABLE IF NOT EXISTS `v_change_pass` (
`emp_id` int(11)
,`flag_change_pass` int(1)
,`flag_change_pass_new` varchar(75)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_count_leave_request`
--
CREATE TABLE IF NOT EXISTS `v_count_leave_request` (
`emp_id` int(11)
,`approver_id` int(11)
,`approve_stat` int(11)
,`approve_desc` char(50)
,`leave_num` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_leave_balance`
--
CREATE TABLE IF NOT EXISTS `v_leave_balance` (
`emp_id` int(11)
,`leave_type_id` int(11)
,`total_leave_hour` decimal(26,2)
,`leave_use_day` decimal(16,0)
,`leave_use_hour` decimal(26,2)
,`leave_description` varchar(100)
,`bonus_limit` float(5,2)
,`legal_limit` float(5,2)
,`your_bonus_limit` double(19,2)
,`your_comp_limit` double(19,2)
,`bonus_balance` double(19,2)
,`bonus_balance_day` double(17,0)
,`bonus_balance_hour` double(19,2)
,`legal_balance` double(19,2)
,`legal_balance_day` double(17,0)
,`legal_balance_hour` double(19,2)
,`fav_flag` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_leave_transaction`
--
CREATE TABLE IF NOT EXISTS `v_leave_transaction` (
`trans_id` int(11)
,`create_date` timestamp
,`leave_date` date
,`emp_id` int(11)
,`submit_stat` int(11)
,`reject_stat` int(11)
,`id_desc` varchar(200)
,`approve_stat` int(11)
,`approve_desc` char(50)
,`hr_confrim_stat` int(11)
,`hr_confirm_desc` varchar(30)
,`leave_type_id` int(11)
,`leave_description` varchar(100)
,`working_hour_id` int(11)
,`work_description` varchar(100)
,`start_leave` decimal(4,2)
,`end_leave` decimal(4,2)
,`leave_reason` varchar(255)
,`leave_hour` decimal(4,2)
,`approver_id` int(11)
,`emp_name` varchar(50)
,`emp_lastname` varchar(100)
,`request_email` varchar(100)
,`reject_date` date
,`cancel_leave_reason` varchar(255)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_total_leave_hour`
--
CREATE TABLE IF NOT EXISTS `v_total_leave_hour` (
`emp_id` int(11)
,`leave_type_id` int(11)
,`total_leave_hour` decimal(26,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_working_calculation`
--
CREATE TABLE IF NOT EXISTS `v_working_calculation` (
`emp_id` int(11)
,`emp_name` varchar(50)
,`emp_lastname` varchar(100)
,`working_startdate` date
,`today` date
,`working_days` int(7)
,`working_months` bigint(21)
,`working_years` decimal(16,0)
,`working_months_remain` bigint(21)
,`day_of_month` int(2)
,`working_months_cal` bigint(22)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_working_leave_limit`
--
CREATE TABLE IF NOT EXISTS `v_working_leave_limit` (
`emp_id` int(11)
,`emp_name` varchar(50)
,`probation` varchar(1)
,`working_year` int(4)
,`leave_type_id` int(11)
,`leave_description` varchar(100)
,`working_startdate` date
,`working_year_start` int(4)
,`working_month_start` int(2)
,`cutoff_working_month_start` int(3)
,`your_bonus_limit` double(19,2)
,`your_comp_limit` double(19,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_working_probation_cal`
--
CREATE TABLE IF NOT EXISTS `v_working_probation_cal` (
`emp_id` int(11)
,`emp_name` varchar(50)
,`comp_id` int(11)
,`working_startdate` date
,`today` date
,`working_days` int(8)
,`probation` varchar(1)
,`working_year` int(4)
,`leave_type_id` int(11)
,`leave_description` varchar(100)
,`bonus_limit` float(5,2)
,`legal_limit` float(5,2)
,`working_day_start` int(2)
,`working_month_start` int(2)
,`working_year_start` int(4)
,`cutoff_working_month_start` int(3)
,`prorate_leave_day` float(4,2)
,`prorate_leave_hour` double(19,2)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `v_working_year`
--
CREATE TABLE IF NOT EXISTS `v_working_year` (
`emp_id` int(11)
,`emp_name` varchar(50)
,`comp_id` int(11)
,`working_startdate` date
,`today` date
,`working_days` int(8)
,`probation` varchar(1)
,`working_year` int(4)
,`leave_type_id` int(11)
,`leave_description` varchar(100)
,`bonus_limit` float(5,2)
,`legal_limit` float(5,2)
,`working_day_start` int(2)
,`working_month_start` int(2)
,`working_year_start` int(4)
,`cutoff_working_month_start` int(3)
);
-- --------------------------------------------------------

--
-- Structure for view `v_approver_email`
--
DROP TABLE IF EXISTS `v_approver_email`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_approver_email` AS select `t1`.`id` AS `id`,`t1`.`emp_email` AS `emp_email`,`t1`.`emp_password` AS `emp_password`,`t1`.`emp_name` AS `emp_name`,`t1`.`emp_lastname` AS `emp_lastname`,`t1`.`emp_id` AS `emp_id`,`t1`.`approver_id` AS `approver_id`,`t2`.`emp_name` AS `approver_name`,`t2`.`emp_email` AS `approver_email`,`t1`.`role_id` AS `role_id`,`t3`.`role_description` AS `role_description` from ((`t_employee` `t1` left join `t_employee` `t2` on((`t1`.`approver_id` = `t2`.`emp_id`))) left join `t_user_role` `t3` on((`t1`.`role_id` = `t3`.`role_id`)));

-- --------------------------------------------------------

--
-- Structure for view `v_change_pass`
--
DROP TABLE IF EXISTS `v_change_pass`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_change_pass` AS select `t_employee`.`emp_id` AS `emp_id`,`t_employee`.`flag_change_pass` AS `flag_change_pass`,(case when (`t_employee`.`flag_change_pass` = 0) then _utf8'คุณยังไม่ได้ทำการเปลี่ยน รหัสผ่าน กดที่นี่เพื่อไปยังหน้าเปลี่ยนรหัสผ่านใหม่' else _utf8'แจ้งข่าวสารใหม่' end) AS `flag_change_pass_new` from `t_employee`;

-- --------------------------------------------------------

--
-- Structure for view `v_count_leave_request`
--
DROP TABLE IF EXISTS `v_count_leave_request`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_count_leave_request` AS select `v_leave_transaction`.`emp_id` AS `emp_id`,`v_leave_transaction`.`approver_id` AS `approver_id`,`v_leave_transaction`.`approve_stat` AS `approve_stat`,`v_leave_transaction`.`approve_desc` AS `approve_desc`,count(`v_leave_transaction`.`approve_stat`) AS `leave_num` from `v_leave_transaction` where (`v_leave_transaction`.`reject_stat` = 0) group by `v_leave_transaction`.`emp_id`,`v_leave_transaction`.`approver_id`,`v_leave_transaction`.`approve_stat`,`v_leave_transaction`.`approve_desc`;

-- --------------------------------------------------------

--
-- Structure for view `v_leave_balance`
--
DROP TABLE IF EXISTS `v_leave_balance`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_leave_balance` AS select `t1`.`emp_id` AS `emp_id`,`t1`.`leave_type_id` AS `leave_type_id`,`t1`.`total_leave_hour` AS `total_leave_hour`,floor((`t1`.`total_leave_hour` / (select `t_working_hour`.`working_hour` AS `working_hour` from `t_working_hour` where (`t_working_hour`.`working_hour_id` = 1)))) AS `leave_use_day`,(`t1`.`total_leave_hour` % (select `t_working_hour`.`working_hour` AS `working_hour` from `t_working_hour` where (`t_working_hour`.`working_hour_id` = 1))) AS `leave_use_hour`,`t2`.`leave_description` AS `leave_description`,`t2`.`bonus_limit` AS `bonus_limit`,`t2`.`legal_limit` AS `legal_limit`,`v2`.`your_bonus_limit` AS `your_bonus_limit`,`v2`.`your_comp_limit` AS `your_comp_limit`,(`v2`.`your_bonus_limit` - `t1`.`total_leave_hour`) AS `bonus_balance`,floor(((`v2`.`your_bonus_limit` - `t1`.`total_leave_hour`) / (select `t_working_hour`.`working_hour` AS `working_hour` from `t_working_hour` where (`t_working_hour`.`working_hour_id` = 1)))) AS `bonus_balance_day`,((`v2`.`your_bonus_limit` - `t1`.`total_leave_hour`) % (select `t_working_hour`.`working_hour` AS `working_hour` from `t_working_hour` where (`t_working_hour`.`working_hour_id` = 1))) AS `bonus_balance_hour`,(`v2`.`your_comp_limit` - `t1`.`total_leave_hour`) AS `legal_balance`,floor(((`v2`.`your_comp_limit` - `t1`.`total_leave_hour`) / (select `t_working_hour`.`working_hour` AS `working_hour` from `t_working_hour` where (`t_working_hour`.`working_hour_id` = 1)))) AS `legal_balance_day`,((`v2`.`your_comp_limit` - `t1`.`total_leave_hour`) % (select `t_working_hour`.`working_hour` AS `working_hour` from `t_working_hour` where (`t_working_hour`.`working_hour_id` = 1))) AS `legal_balance_hour`,`t2`.`fav_flag` AS `fav_flag` from ((`v_total_leave_hour` `t1` left join `t_leave_type` `t2` on((`t1`.`leave_type_id` = `t2`.`type_id`))) left join `v_working_leave_limit` `v2` on(((`t1`.`emp_id` = `v2`.`emp_id`) and (`t1`.`leave_type_id` = `v2`.`leave_type_id`))));

-- --------------------------------------------------------

--
-- Structure for view `v_leave_transaction`
--
DROP TABLE IF EXISTS `v_leave_transaction`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_leave_transaction` AS select `t1`.`trans_id` AS `trans_id`,`t1`.`create_date` AS `create_date`,cast(`t1`.`leave_date` as date) AS `leave_date`,`t1`.`emp_id` AS `emp_id`,`t1`.`submit_stat` AS `submit_stat`,`t1`.`reject_stat` AS `reject_stat`,`t8`.`id_desc` AS `id_desc`,`t1`.`approve_stat` AS `approve_stat`,`t2`.`approve_desc` AS `approve_desc`,`t1`.`hr_confrim_stat` AS `hr_confrim_stat`,`t4`.`hr_confirm_desc` AS `hr_confirm_desc`,`t1`.`leave_type_id` AS `leave_type_id`,`t3`.`leave_description` AS `leave_description`,`t1`.`working_hour_id` AS `working_hour_id`,`t7`.`work_description` AS `work_description`,`t1`.`start_leave` AS `start_leave`,`t1`.`end_leave` AS `end_leave`,`t1`.`leave_reason` AS `leave_reason`,`t1`.`leave_hour` AS `leave_hour`,`t5`.`approver_id` AS `approver_id`,`t6`.`emp_name` AS `emp_name`,`t6`.`emp_lastname` AS `emp_lastname`,`t6`.`emp_email` AS `request_email`,`t1`.`reject_date` AS `reject_date`,`t1`.`cancel_leave_reason` AS `cancel_leave_reason` from (((((((`t_leave_transaction` `t1` left join `t_approve_desc` `t2` on((`t1`.`approve_stat` = `t2`.`approve_id`))) left join `t_leave_type` `t3` on((`t1`.`leave_type_id` = `t3`.`type_id`))) left join `t_hr_confirm` `t4` on((`t1`.`hr_confrim_stat` = `t4`.`id_stat`))) left join `t_employee` `t5` on((`t1`.`emp_id` = `t5`.`emp_id`))) left join `t_employee` `t6` on((`t1`.`emp_id` = `t6`.`emp_id`))) left join `t_working_hour` `t7` on((`t1`.`working_hour_id` = `t7`.`working_hour_id`))) left join `t_reject_desc` `t8` on((`t1`.`reject_stat` = `t8`.`id_reject`))) where (`t1`.`leave_date` between (select `t_year`.`start_date` AS `start_date` from `t_year` where (`t_year`.`year_active` = 1)) and (select `t_year`.`end_date` AS `end_date` from `t_year` where (`t_year`.`year_active` = 1)));

-- --------------------------------------------------------

--
-- Structure for view `v_total_leave_hour`
--
DROP TABLE IF EXISTS `v_total_leave_hour`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_total_leave_hour` AS select `t_leave_transaction`.`emp_id` AS `emp_id`,`t_leave_transaction`.`leave_type_id` AS `leave_type_id`,sum(`t_leave_transaction`.`leave_hour`) AS `total_leave_hour` from `t_leave_transaction` where ((`t_leave_transaction`.`leave_date` between (select `t_year`.`start_date` AS `start_date` from `t_year` where (`t_year`.`year_active` = 1)) and (select `t_year`.`end_date` AS `end_date` from `t_year` where (`t_year`.`year_active` = 1))) and (`t_leave_transaction`.`approve_stat` < 2) and (`t_leave_transaction`.`reject_stat` not in (_utf8'1',_utf8'3'))) group by `t_leave_transaction`.`emp_id`,`t_leave_transaction`.`leave_type_id`;

-- --------------------------------------------------------

--
-- Structure for view `v_working_calculation`
--
DROP TABLE IF EXISTS `v_working_calculation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_working_calculation` AS select `t_employee`.`emp_id` AS `emp_id`,`t_employee`.`emp_name` AS `emp_name`,`t_employee`.`emp_lastname` AS `emp_lastname`,`t_employee`.`working_startdate` AS `working_startdate`,cast(now() as date) AS `today`,(to_days(cast(now() as date)) - to_days(`t_employee`.`working_startdate`)) AS `working_days`,timestampdiff(MONTH,`t_employee`.`working_startdate`,cast(now() as date)) AS `working_months`,floor((timestampdiff(MONTH,`t_employee`.`working_startdate`,cast(now() as date)) / 12)) AS `working_years`,(timestampdiff(MONTH,`t_employee`.`working_startdate`,cast(now() as date)) % 12) AS `working_months_remain`,dayofmonth(`t_employee`.`working_startdate`) AS `day_of_month`,(case when (dayofmonth(`t_employee`.`working_startdate`) > 15) then (timestampdiff(MONTH,`t_employee`.`working_startdate`,cast(now() as date)) % 12) else ((timestampdiff(MONTH,`t_employee`.`working_startdate`,cast(now() as date)) % 12) + 1) end) AS `working_months_cal` from `t_employee`;

-- --------------------------------------------------------

--
-- Structure for view `v_working_leave_limit`
--
DROP TABLE IF EXISTS `v_working_leave_limit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_working_leave_limit` AS select `v1`.`emp_id` AS `emp_id`,`v1`.`emp_name` AS `emp_name`,`v1`.`probation` AS `probation`,`v1`.`working_year` AS `working_year`,`v1`.`leave_type_id` AS `leave_type_id`,`v1`.`leave_description` AS `leave_description`,`v1`.`working_startdate` AS `working_startdate`,`v1`.`working_year_start` AS `working_year_start`,`v1`.`working_month_start` AS `working_month_start`,`v1`.`cutoff_working_month_start` AS `cutoff_working_month_start`,(case when ((`v1`.`probation` = _utf8'n') and (`v1`.`leave_type_id` = 1)) then 0 when ((`v1`.`probation` = _utf8'n') and (`v1`.`leave_type_id` = 2)) then 0 when ((`v1`.`probation` = _utf8'n') and (`v1`.`leave_type_id` = 3)) then 0 when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 1)) then `v1`.`bonus_limit` when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 2) and (`v1`.`working_year` < 1)) then `v1`.`bonus_limit` when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 9)) then (`v1`.`bonus_limit` + (2 * 8)) when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 10)) then (`v1`.`bonus_limit` + (1.5 * 8)) when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 11)) then (`v1`.`bonus_limit` + (1 * 8)) when ((`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 12)) then (`v1`.`bonus_limit` + (0.5 * 8)) when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year` < 1)) then 0 else `v1`.`bonus_limit` end) AS `your_bonus_limit`,(case when ((`v1`.`leave_type_id` = 1) and (year(now()) = `v1`.`working_year_start`)) then `v1`.`prorate_leave_hour` when ((`v1`.`leave_type_id` = 1) and (year(now()) <> `v1`.`working_year_start`)) then `v1`.`legal_limit` when ((`v1`.`probation` = _utf8'n') and (`v1`.`leave_type_id` = 2)) then `v1`.`legal_limit` when ((`v1`.`probation` = _utf8'n') and (`v1`.`leave_type_id` = 3)) then 0 when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 1) and (`v1`.`working_year` < 1)) then `v1`.`prorate_leave_hour` when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 2) and (`v1`.`working_year` < 1)) then `v1`.`legal_limit` when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 9)) then (`v1`.`bonus_limit` + (2 * 8)) when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 10)) then (`v1`.`bonus_limit` + (1.5 * 8)) when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 11)) then (`v1`.`bonus_limit` + (1 * 8)) when ((`v1`.`working_year_start` >= 2015) and ((year(now()) - `v1`.`working_year_start`) = 1) and (`v1`.`cutoff_working_month_start` = 12)) then (`v1`.`bonus_limit` + (0.5 * 8)) when ((`v1`.`probation` = _utf8'y') and (`v1`.`leave_type_id` = 3) and (`v1`.`working_year` < 1)) then `v1`.`prorate_leave_hour` else `v1`.`legal_limit` end) AS `your_comp_limit` from `v_working_probation_cal` `v1`;

-- --------------------------------------------------------

--
-- Structure for view `v_working_probation_cal`
--
DROP TABLE IF EXISTS `v_working_probation_cal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_working_probation_cal` AS select `v1`.`emp_id` AS `emp_id`,`v1`.`emp_name` AS `emp_name`,`v1`.`comp_id` AS `comp_id`,`v1`.`working_startdate` AS `working_startdate`,`v1`.`today` AS `today`,`v1`.`working_days` AS `working_days`,`v1`.`probation` AS `probation`,`v1`.`working_year` AS `working_year`,`v1`.`leave_type_id` AS `leave_type_id`,`v1`.`leave_description` AS `leave_description`,`v1`.`bonus_limit` AS `bonus_limit`,`v1`.`legal_limit` AS `legal_limit`,`v1`.`working_day_start` AS `working_day_start`,`v1`.`working_month_start` AS `working_month_start`,`v1`.`working_year_start` AS `working_year_start`,`v1`.`cutoff_working_month_start` AS `cutoff_working_month_start`,`t2`.`prorate_leave_day` AS `prorate_leave_day`,(`t2`.`prorate_leave_day` * 8) AS `prorate_leave_hour` from (`v_working_year` `v1` left join `t_leave_prorate` `t2` on(((`v1`.`leave_type_id` = `t2`.`leave_type_id`) and (`v1`.`cutoff_working_month_start` = `t2`.`start_month`))));

-- --------------------------------------------------------

--
-- Structure for view `v_working_year`
--
DROP TABLE IF EXISTS `v_working_year`;

CREATE ALGORITHM=UNDEFINED DEFINER=`bdgcente`@`localhost` SQL SECURITY DEFINER VIEW `v_working_year` AS select `t1`.`emp_id` AS `emp_id`,`t1`.`emp_name` AS `emp_name`,`t1`.`comp_id` AS `comp_id`,`t1`.`working_startdate` AS `working_startdate`,cast(now() as date) AS `today`,((to_days(cast(now() as date)) - to_days(`t1`.`working_startdate`)) + 1) AS `working_days`,(case when (((to_days(cast(now() as date)) - to_days(`t1`.`working_startdate`)) + 1) > 119) then _utf8'y' else _utf8'n' end) AS `probation`,year(from_days(((to_days(now()) - to_days(`t1`.`working_startdate`)) + 1))) AS `working_year`,`t2`.`leave_type_id` AS `leave_type_id`,`t3`.`leave_description` AS `leave_description`,`t3`.`bonus_limit` AS `bonus_limit`,`t3`.`legal_limit` AS `legal_limit`,dayofmonth(`t1`.`working_startdate`) AS `working_day_start`,month(`t1`.`working_startdate`) AS `working_month_start`,year(`t1`.`working_startdate`) AS `working_year_start`,(case when (dayofmonth(`t1`.`working_startdate`) > 15) then (month(`t1`.`working_startdate`) + 1) else month(`t1`.`working_startdate`) end) AS `cutoff_working_month_start` from ((`t_employee` `t1` left join `t_leave_group_comp` `t2` on((`t1`.`comp_id` = `t2`.`comp_id`))) left join `t_leave_type` `t3` on((`t2`.`leave_type_id` = `t3`.`type_id`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
