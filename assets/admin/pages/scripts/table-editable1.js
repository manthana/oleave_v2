var TableEditable = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }


        function editRow(oTable, nRow) {
            var aData = oTable.split(',');;
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML = '<input type="hidden" name="i_number[]" class="form-control input-small"  value="' + aData[0] + '">'+aData[0];
            jqTds[1].innerHTML =  aData[1];
            jqTds[2].innerHTML = '<input type="hidden" name="i_code[]" class="form-control input-small"  value="' + aData[1] + '"><input type="hidden" name="i_name[]" class="form-control input-small"  value="' + aData[2] + '"><input type="hidden" name="i_description[]" class="form-control input-small"  value="' + aData[4] + '"><input type="hidden" name="i_id[]" class="form-control input-small"  value="' + aData[5] + '"><input type="hidden" name="i_price[]" class="form-control input-small"  value="' + aData[6] + '"><input type="hidden" name="i_cost[]" class="form-control input-small"  value="' + aData[7] + '">'+aData[2];


            jqTds[3].innerHTML = '<input type="hidden" name="moi_quantity[]" class="form-control input-small" min="1" value="' + aData[3] + '">' + aData[3] ;
//            jqTds[4].innerHTML = '<input type="text" name="moi_special_order[]" class="form-control input-small"  value="">';
//            jqTds[5].innerHTML = '<input type="text" name="moi_comment[]" class="form-control input-small"  value="">';
            jqTds[4].innerHTML = '<a class="delete btn btn-danger" href="">Delete</a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
//            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
//            oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
//            oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
            oTable.fnUpdate('<a class="edit btn btn-success" href="">Edit</a><a class="delete btn btn-danger" href="">Delete</a>', nRow, 4, false);
            oTable.fnDraw();
        }



        var table = $('#sample_editable_1');

        var oTable = table.dataTable({
//            "lengthMenu": [
////                [5, 15, 20, -1],
////                [5, 15, 20, "All"] // change per page values here
//            ],
            // set the initial value
//            "pageLength": -1,
            "paging":   false,
            "searching":   false,
            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0,1,2]
            },{ // set default column settings
                'orderable': false,
                'targets': [3,4]
            }, {
                "searchable": false,
                "targets": [3,4]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper");

        tableWrapper.find(".dataTables_length select").select2({
            showSearchInput: false //hide search box with special css class
        }); // initialize select2 dropdown

        var nEditing = null;
        var nNew = false;

        $("#confirm_item").click(function(e){
            e.preventDefault();

            $.get("crudorder.php",{action:7,b:$("select[name='pre_i_id']").val(),m_id:$("input[name='m_id']").val()==undefined?$("select[name='m_id']").val():$("input[name='m_id']").val(),quantity:$("input[name='pre_ti_quantity']").val()},function(data){
                if(data=="Error"){
                    alert("Cannot Add this item Please refresh page");
                }else if(data=="Error out of stock"){
                    alert("Cannot Add this item");
                }else{

                    $("input[name='pre_ti_quantity']").val("1");
                    $("select[name='m_id']").attr("readonly","readonly");
                    var aiNew = oTable.fnAddData(['', '', '', '', '', '','']);
                    var nRow = oTable.fnGetNodes(aiNew[0]);
                    editRow(data, nRow);
                    nEditing = nRow;
                    nNew = true;
                }

            });
        });
        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this row ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            oTable.fnDeleteRow(nRow);
//            alert("Deleted! Do not forget to do some ajax to sync with backend :)");
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();

            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            if (nEditing !== null && nEditing != nRow) {
                /* Currently editing - but not this row - restore the old before continuing to edit mode */
                restoreRow(oTable, nEditing);
                editRow(oTable, nRow);
                nEditing = nRow;
            } else if (nEditing == nRow && this.innerHTML == "Save") {
                /* Editing this row and want to save it */
                saveRow(oTable, nEditing);
                nEditing = null;
//                alert("Updated! Do not forget to do some ajax to sync with backend :)");
            } else {
                /* No edit in progress - let's start one */
                editRow(oTable, nRow);
                nEditing = nRow;
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();