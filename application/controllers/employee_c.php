<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_c extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		

		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

 		$emp_id =  $session_data['emp_id'];

 		$sql = "select * from t_employee where emp_id = '$emp_id'";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
		

		       
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}



		$this->load->view('change_password',$data);
	}
	// end of index (select) function



	public function change_password()
	{
	

		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

 		$emp_id =  $session_data['emp_id'];

 		$sql = "select * from t_employee where emp_id = '$emp_id'";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

 				if($this->input->post("btsave")!=null)
				{
					$data_emp = array(

		   				'emp_password' 	=> $this->input->post("new_pass") ,
		   				'flag_change_pass' 		=> $this->input->post("flag_pass_hid")
					);
					$this->db->where('emp_id',$emp_id);
					$this->db->update('t_employee', $data_emp); 
					$this->load->view('change_complated',$data);
					exit();
				}
				

 		
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}



		$this->load->view('change_password',$data);
		// edit record


		/*

		if($this->input->post("btsave")!=null)
		{
			
			$data_comp = array(

   				//'emp_password' 		=> $this->input->post("new_pass"),
   				'flag_change_pass' 		=> $this->input->post("flag_pass_hid")

			);
			$this->db->where('id',$id);
			$this->db->update('t_company', $data_comp); 
			redirect("company_c","refresh");
			exit();
		}

		// end of edit record

		// select for show to edit

		$sql = "select * from t_company where id = '$id'";
		$rs = $this->db->query($sql);

		if($rs->num_rows()==0)
		{
			$data['rs'] = array();
		}	
		else
		{
			$data['rs'] = $rs->row_array();
		}	
		// end of edit
		
		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

		$emp_id =  $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}


		$this->load->view('update_comp',$data);
	*/
		
	} // end of change password function

	// public function employee_list()
	// {
		

	// 	$sql = "select * from t_employee join t_dept 
	// 	where t_employee.dept_id = t_dept.dept_id 
	// 	order by t_dept.dept_name asc , t_employee.emp_id asc";
	// 	$rs = $this->db->query($sql);
	// 	$data['rs'] = $rs->result_array();

	// 	if($this->session->userdata('logged_in'))
 //      			{
	// 	        $session_data = $this->session->userdata('logged_in');
	// 	        $data['username'] = $session_data['username'];

	// 	        $data['emp_name'] = $session_data['emp_name'];
	// 	        $data['emp_lastname'] = $session_data['emp_lastname'];
	// 	        $data['role_id'] = $session_data['role_id'];
 //        		$data['role_description'] = $session_data['role_description'];
	// 	        $data['emp_id'] = $session_data['emp_id'];

 // 		$emp_id =  $session_data['emp_id'];

 //        // number of alert leave request
 //        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
 //        $rs   = $this->db->query($sql);
 //        $data['rs_count_request'] = $rs->result_array();

 //         // number of alert approved leave request
 //        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
 //        $rs   = $this->db->query($sql);
 //        $data['rs_count_approved_request'] = $rs->result_array();

 //        // number of alert approve leave
 //        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
 //        $rs   = $this->db->query($sql);
 //        $data['rs_count_approve'] = $rs->result_array();
		

		       
		       
	// 	}
	// 		      else
	// 	{
	// 		        //If no session, redirect to login page
	// 		        redirect('login', 'refresh');
	// 	}



	// 	$this->load->view('employee_view',$data);
	// }
	// end of index (select) function


	public function add_employee()
	{
		if($this->input->post("btsave")!=null)
		{
			$data_comp = array(
   				'comp_name' 	=> $this->input->post("comp_name") ,
   				'comp_address' 	=> $this->input->post("comp_addr")  ,
   				'comp_logo' 	=> $this->input->post("comp_logo") 
			);

			$this->db->insert('t_employee', $data); 
			redirect("employee_c","refresh");
			exit();
		}
		// end of insert method


		if($this->session->userdata('logged_in'))
      {
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        $data['emp_name'] = $session_data['emp_name'];
        $data['emp_lastname'] = $session_data['emp_lastname'];
        $data['emp_id'] = $session_data['emp_id'];
        $data['role_id'] = $session_data['role_id'];
        $data['role_description'] = $session_data['role_description'];
      

        $emp_id = $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0 ";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

        // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
        
        
        // select my request list to show
        $sql  = "select * from v_leave_transaction where emp_id = '$emp_id' and approve_stat = 0 and reject_stat not in ('1','3') order by trans_id desc";
        $rs   = $this->db->query($sql);
        $data['rs'] = $rs->result_array();
        

        // select my leave balance to show
        $stat = 1;
        $query = $this->db->get_where('v_leave_balance',array('emp_id' => $emp_id , 'fav_flag' => $stat ) );
        $data['result_leave_balance'] = $query;

        // select leave limit to show
        $sql  = "select working_startdate, probation, working_year, leave_description,
        your_bonus_limit,round((your_bonus_limit/8),1) as your_bonus_limit_days , your_comp_limit
        ,round((your_comp_limit/8),1) as your_comp_limit_days  
        from v_working_leave_limit where emp_id = '$emp_id' ";
        $rs   = $this->db->query($sql);
        $data['result_leave_limit'] = $rs->result_array();


        

        $this->load->view('add_employee',$data);
    }
            else
      {
        //If no session, redirect to login page
        redirect('login', 'refresh');
	  }
		//$this->load->view('add_employee');






	}





}/* End of file employee controller */
/* Location: ./application/controllers/welcome.php */
