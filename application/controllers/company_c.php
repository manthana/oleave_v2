<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company_c extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		

		$sql = "select * from t_company order by id";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();

		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

 		$emp_id =  $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
		

		       
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}



		$this->load->view('company_addr',$data);
	}
	// end of index (select) function
	public function add_comp()
	{
		
		if($this->input->post("btsave")!=null)
		{
			$data_comp = array(
   				'comp_name' 	=> $this->input->post("comp_name") ,
   				'comp_address' 	=> $this->input->post("comp_addr")  ,
   				'comp_logo' 	=> $this->input->post("comp_logo") 
			);

			$this->db->insert('t_company', $data_comp); 
			redirect("company_c","refresh");
			exit();
		}
		// end of insert method

		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];
		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

		$emp_id =  $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

	
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}
		// end of user session

		$this->load->view('create_comp',$data);
	}

	public function del_comp($id)
	{
		
		 
		$this->db->delete('t_company', array('id' => $id)); 
		redirect("company_c","refresh");
		exit();
	}

	public function update_comp($id)
	{
		// edit record

		if($this->input->post("btedit")!=null)
		{
			$data_comp = array(
   				
   				'comp_name' 	=> $this->input->post("comp_name") ,
   				'comp_address' 	=> $this->input->post("comp_addr")  ,
   				'comp_logo' 	=> $this->input->post("comp_logo") 
			);
			$this->db->where('id',$id);
			$this->db->update('t_company', $data_comp); 
			redirect("company_c","refresh");
			exit();
		}

		// end of edit record

		// select for show to edit

		$sql = "select * from t_company where id = '$id'";
		$rs = $this->db->query($sql);

		if($rs->num_rows()==0)
		{
			$data['rs'] = array();
		}	
		else
		{
			$data['rs'] = $rs->row_array();
		}	
		// end of edit
		
		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

		$emp_id =  $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}


		$this->load->view('update_comp',$data);

		
	}// end of show to edit


	// start working_year setting section

	public function active_year()
	{
		
		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

 		$emp_id =  $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
		
        $sql = "select * from t_year where year_active = 1 ";
		$rs = $this->db->query($sql);
		$data['rs_year_active'] = $rs->result_array();

		 $sql = "select * from t_year where year_active <> 1 ";
		$rs = $this->db->query($sql);
		$data['rs_year_no_active'] = $rs->result_array();

        $sql = "select * from t_year order by id_year ";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();

		$this->load->view('active_year',$data);
		       
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}



		
	}
	// end of index (select) function
	// end of action_year setting section

	public function change_active_year()
	
	{
		// edit record

		if($this->input->post("btsave")!=null)

		{
			$id_active_year = $this->input->post('active_year_hid');
			$id_new_year = $this->input->post('new_year_hid');

			$data_active_year = array(
   				
   				'year_active' 	=> 0
   			);
			$this->db->where('id_year',$id_active_year);
			$this->db->update('t_year', $data_active_year); 


			$data_new_year = array(
   				
   				'year_active' 	=> 1 
   			);
			$this->db->where('id_year',$id_new_year);
			$this->db->update('t_year', $data_new_year); 

			
			redirect("company_c/active_year","refresh");
			exit();
			
		}
		//$this->load->view('active_year',$data);
		redirect("company_c/active_year","refresh");
	}


	public function leave_show()
	{
		

		$sql = "select *, round((bonus_limit/8),1) as bonus_limit_days , round((legal_limit/8),1) as legal_limit_days
		, case when fav_flag = 1 then 'yes' 
		else 'No' 
		end as flag
		from t_leave_type order by type_id";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();

		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

 		$emp_id =  $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
		

		       
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}



		$this->load->view('view_leave',$data);
	}
	// end of index (select) function

	public function update_leave($id)
	{
		// edit record

		
		if($this->input->post("btedit")!=null)
		{

			$data_leave = array(
   				
   				//'bonus_limit' 			=> $this->input->post("bonus_limit_hid") , 
   				//'legal_limit' 			=> $this->input->post("legal_limit_hid") ,  
   				
   				'leave_description' 	=> $this->input->post("leave_description") , 
   				'fav_flag'				=> $this->input->post("fav_flag")
   			);
			$this->db->where('type_id',$id);
			$this->db->update('t_leave_type', $data_leave); 

			
			redirect("company_c/leave_show","refresh");
			//exit();
		}

		// end of edit record

		// select for show to edit

		$sql = "select *, round((bonus_limit/8),1) as bonus_limit_days , round((legal_limit/8),1) as legal_limit_days
		, case when fav_flag = 1 then 'yes' 
		else 'No' 
		end as flag
		from t_leave_type where type_id = $id order by type_id ";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();

		if($rs->num_rows()==0)
		{
			$data['rs'] = array();
		}	
		else
		{
			$data['rs'] = $rs->row_array();
		}	
		// end of edit
		
		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $data['emp_id'] = $session_data['emp_id'];

		$emp_id =  $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}


		$this->load->view('update_leave',$data);

		
	}// end of show to edit

}/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */