<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//require_once APPPATH."third_party/PHPExcel/Classes/PHPExcel.php"; 

class Export_csv_ctrl extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{


		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
      
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		// approve_stat = 0 on use
		$sql = "select * from v_leave_transaction where emp_id = '$emp_id' and approve_stat = 0  and reject_stat not in ('1','3') order by trans_id desc";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		$this->load->view('request_list',$data);
	}  // end of show request list


	public function export()
	{
	

	// defalut on load (not yet select filter) 
	if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
      
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
        
		// approve_stat = 0 on use
		$sql = "select * from v_leave_transaction ";
		//where emp_id = '$emp_id'"; 
		//and approve_stat = 0  and reject_stat not in ('1','3') order by trans_id desc";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		$sql  = "select * from t_leave_type";
        $rs   = $this->db->query($sql);
        $data['result_leave_type'] = $rs->result_array();

        $sql  = "select * from t_approve_desc ";
        $rs   = $this->db->query($sql);
        $data['result_approve_stat'] = $rs->result_array();

        $sql  = "select * from t_reject_desc ";
        $rs   = $this->db->query($sql);
        $data['result_reject_stat'] = $rs->result_array();

		$this->load->view('export_report',$data);

	}  // end of show request list




	public function export_report_filter()
	{
	if($this->input->post("btfind")!=null)
	{
		$selected_date_start = $this->input->post('leave_date_start');
		$selected_date_end = $this->input->post('leave_date_end');

		$selected_leave_type = $this->input->post('leave_type');
		$filter_leave_type = $this->input->post('leave_type_hid');

		$selected_approve_stat = $this->input->post('leave_approve');
		$filter_approve_stat = $this->input->post('approve_stat_hid');
		
		$selected_reject_stat = $this->input->post('leave_reject');
		$filter_reject_stat = $this->input->post('reject_stat_hid');

		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
      
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		// filter value to show in view

		$sql_trx = "select  * from v_leave_transaction where leave_type_id $filter_leave_type '$selected_leave_type' and approve_stat $filter_approve_stat '$selected_approve_stat' and reject_stat $filter_reject_stat '$selected_reject_stat' and leave_date between '$selected_date_start' and '$selected_date_end'"; 
		
		$rs = $this->db->query($sql_trx);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		// selection value to show in option of listbox filter
		$sql  = "select * from t_leave_type ";
        $rs   = $this->db->query($sql);
        $data['result_leave_type'] = $rs->result_array();

        
        $sql  = "select * from t_approve_desc ";
        $rs   = $this->db->query($sql);
        $data['result_approve_stat'] = $rs->result_array();

        $sql  = "select * from t_reject_desc ";
        $rs   = $this->db->query($sql);
        $data['result_reject_stat'] = $rs->result_array();
        // end of selection value


		$this->export_csv_file($sql_trx);       


       

		$this->load->view('export_report',$data);
		
		// call functionn export csv 
        //$this->export_csv_file($sql);
        // end of function
	
		} 


	}  // end of show request list

	// public function alert(){
	// 	echo '<script type="text/javascript">alert("Upload Completed!")</script>';
	// }
	  
    public function export_csv_file($sql_trx) {

		// Load database and query
    //$this->load->database();
    //$query = $this->db->get('v_leave_transaction');

    $query_trx = $this->db->query($sql_trx);
    

    // Load database utility class
    $this->load->dbutil();
    $delimiter = ",";
    $newline = "\r\n";
    // Create CSV output
    //$data_trx = mb_convert_encoding($this->dbutil->csv_from_result($query_trx),'ISO-8859-9','UTF-8');
    $data_trx = $this->dbutil->csv_from_result($query_trx, $delimiter, $newline) ;
    $data_trx = chr(239) . chr(187) . chr(191) .$data_trx;

    //mb_convert_encoding($data_trx,'UTF-8');
    // Load download helper
    $this->load->helper('download');
    // Stream download
    force_download('export_transaction_file.csv', $data_trx);
    
    }

} // end of class
    


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */