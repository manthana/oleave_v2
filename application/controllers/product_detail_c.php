<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_detail_c extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		/*
		$arr = array();
		$query = $this->db->get('news');
		$arr['result_news'] = $query;
		
		$query = $this->db->get('product');
		$arr['result_product'] = $query;
		
		$this->load->view('myproduct',$arr);
		*/


		$arr = array();
		$query = $this->db->get('tb_logo');
		$arr['result_logo'] = $query;
	/*	
		$query = $this->db->get('tb_slide');
		$arr['result_slide'] = $query;

		$query = $this->db->get('tb_product');
		$arr['result_product'] = $query;
		

		$query = $this->db->get('tb_news');
		$arr['result_news'] = $query;
*/
		
		$this->load->view('product_detail',$arr);
	}

	public function productDetail()

	{
		$arr = array();

		//$query = $this->db->get('tb_logo');
		//$arr['result_logo'] = $query;

		$query = $this->db->get('tb_product');
		$arr['result_product_recent'] = $query;

		$query = $this->db->get_where('tb_product', array('id_product' => $this->uri->segment(3)));
		//print_r($query->result_array());
		$arr['result_product'] = $query;

		$this->load->view('product_detail',$arr);
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */