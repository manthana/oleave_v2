<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//require_once APPPATH."third_party/PHPExcel/Classes/PHPExcel.php"; 

class Request_list_c extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{


		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
      
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		// approve_stat = 0 on use
		$sql = "select * from v_leave_transaction where emp_id = '$emp_id' and approve_stat = 0  and reject_stat not in ('1','3') order by trans_id desc";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		$this->load->view('request_list',$data);
	}  // end of show request list





	// show approved list for requester
	public function approved_request_list()
	{


		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		// approved_stat > 0 on use
		$sql = "select * from v_leave_transaction where emp_id = '$emp_id' and approve_stat > 0 and reject_stat = 0 order by trans_id desc";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		$this->load->view('approved_request_list',$data);
	} // end of show approved list for requester


	// show cancel request list
	public function cancel_request_list()
	{


		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		// cancel_request_list = reject stat in 1 (before approve) and 3 (hr comfirm)
		$sql = "select * from v_leave_transaction where emp_id = '$emp_id' and reject_stat  in ('1','2','3') order by trans_id desc";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		$this->load->view('cancel_request_list',$data);
	}	// end of cancel request list


	// show cancel detail
	public function view_cancel_request_detail($id)
	{
		
		
		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $emp_id = $session_data['emp_id'];


		        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		
		$sql = "select * from v_leave_transaction where trans_id = '$id' and emp_id = '$emp_id' and reject_stat in ('1','2','3') ";
		$rs = $this->db->query($sql);

		if($rs->num_rows()==0)
		{
			$data['rs'] = array();
		}	
		else
		{
			$data['rs'] = $rs->row_array();
		}	        
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}


		$this->load->view('view_cancel_request_detail',$data);

	
	}	// end of view_cancel_request_detail




	// thes function for update stat to reject leave application
	public function cancel_request_detail(){

		if($this->session->userdata('logged_in'))
			      	{
					        $session_data = $this->session->userdata('logged_in');
					        $data['username'] = $session_data['username'];
					        $data['emp_name'] = $session_data['emp_name'];
					        $data['emp_lastname'] = $session_data['emp_lastname'];
					        $data['emp_id'] = $session_data['emp_id'];
					       	$data['approver_email'] = $session_data['approver_email'];
					       	$data['role_id'] = $session_data['role_id'];
        					$data['role_description'] = $session_data['role_description'];

					        $emp_id 			= $session_data['emp_id'];
					        $emp_email 			= $session_data['username'];
					        $emp_name  			= $session_data['emp_name'];
					        $emp_lastname       = $session_data['emp_lastname'];
					        $approver_name  	= $session_data['approver_name'];
					        $approver_email  	= $session_data['approver_email'];
		


					if($this->input->post("btcancel")!=null)
					{
						
						
								$id = $this->input->post("trans_id_hid");
								$reject_stat = $this->input->post("reject_stat_hid");
								$data = array(
				                'reject_stat' 	=> $reject_stat,	 //=> $this->input->post("reject_stat_hid"),
				                'reject_date' 		 => $this->input->post("reject_date_hid"),
				                'cancel_leave_reason' => $this->input->post("reject_leave_reason")
            					);

							
							
								$this->db->where('trans_id', $id);
								$this->db->update('t_leave_transaction', $data); 
						
						//$this->db->insert('t_leave_transaction', $data_leave); 
					/*	
					// send email for alert user 
					$message_mail = "ขออนุมัติลางาน โดย";
					$message_body = $message_mail.' '.$emp_name.' '.$emp_lastname;

					$this->load->library('phpmailer');
					$this->phpmailer->IsSMTP();     // ใช้งาน SMTP
			        $this->phpmailer->SMTPAuth   = true;   // เปิดการการตรวจสอบการใช้งาน SMTP
			        $this->phpmailer->SMTPSecure = "ssl";  // protocol ที่จะใช้เชื่อมต่อกับ server
			        $this->phpmailer->Host       = "smtp.gmail.com";      // ตั้งค่า mail server ของเรานะครับ ตัวอย่างจะใช้ของ Gmail นะครับ
			        $this->phpmailer->Port       = 465;                   //  port ที่ใช้  ถ้าเป็นของ hosting ทั่วไปส่วนใหญ่เป็น 25 นะครับ
			        $this->phpmailer->Username   = "leave.bdgroup@gmail.com";  //  email address
			        $this->phpmailer->Password   = "bdlp1234";            // รหัสผ่าน Gamil
			        $this->phpmailer->SetFrom('leave.bdgroup@gmail.com', 'BDGroup Leave Management System');  //ผู้ส่ง
			        $this->phpmailer->AddReplyTo("leave.bdgroup@gmail.com","BDGroup Leave Management System");  //email ผู้รับเมื่อมีการตอบกลับนะครับ
			        $this->phpmailer->Subject    = "ขออนุมัติลางาน"; //หัวข้ออีเมล์
			        $this->phpmailer->Body      = $message_body; //ส่วนนี้รายละเอียดสามารถส่งเป็นรูปแบบ HTML ได้
			        //$this->phpmailer->AltBody    = "Plain text message"; //ส่วนนี้ส่งเป็นข้อมูลอย่างเดียวเท่าสนั้น แล้วแต่จะเปิดใช้นะครับ
			        $this->phpmailer->AddAddress( $approver_email, $approver_name); //อีกเมล์ผู้รับ  สามารถเพิ่มได้มากกว่า 1
			        $this->phpmailer->AddAddress( $emp_email, $emp_name); //ตัวอย่างการพิ่มได้มากกว่า 1
			 
			        //$this->phpmailer->AddAttachment("images/phpmailer.gif");      // การแนบไฟล์ถ้าต้องการ สามารถเพิ่มได้มากกว่า 1 เช่นกันครับ
			        //$this->phpmailer->AddAttachment("images/phpmailer_mini.gif"); // ตัวอย่างการพิ่มได้มากกว่า 1
			        if(!$this->phpmailer->Send()) {
			            $data["message"] = "Error: " . $this->phpmailer->ErrorInfo;
			        } else {
			        	
			            $data["message"] = $message_mail + $emp_name + $emp_lastname;
			        }
			        //$this->load->view('sent_mail',$data);
					// end of send email
					*/
					
			        /*
						redirect("request_list_c","refresh");
						exit();
						
					}
					// end of insert method

					
				*/	       
				}
						      else
				{
						        //If no session, redirect to login page
						        redirect('login', 'refresh');
				}
					// end of user session
				
					redirect('request_list_c', 'refresh');
				}	
		
	} // end of function cancel_request


	// show request detail
	public function view_request_detail($id)
	{
		// edit record
		/*
		if($this->input->post("btedit")!=null)
		{
			$data_comp = array(
   				'comp_name' 	=> $this->input->post("comp_name") ,
   				'comp_address' 	=> $this->input->post("comp_addr")  ,
   				'comp_logo' 	=> $this->input->post("comp_logo") 
			);
			$this->db->where('id',$id);
			$this->db->update('t_company', $data_comp); 
			redirect("company_c","refresh");
			exit();
		}
		*/
		// end of edit record

		// select for show to edit

		
		// end of select to show
		
		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $emp_id = $session_data['emp_id'];


		        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		
		$sql = "select * from v_leave_transaction where trans_id = '$id' and emp_id = '$emp_id' ";
		$rs = $this->db->query($sql);

		if($rs->num_rows()==0)
		{
			$data['rs'] = array();
		}	
		else
		{
			$data['rs'] = $rs->row_array();
		}	        
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}


		$this->load->view('view_request_detail',$data);

	}	// end of show request detail function



	public function request_report()
	{
	

	// defalut on load (not yet select filter) 
	if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
      
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
        
		// approve_stat = 0 on use
		$sql = "select * from v_leave_transaction where emp_id = '$emp_id'"; 
		//and approve_stat = 0  and reject_stat not in ('1','3') order by trans_id desc";
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		$sql  = "select * from t_leave_type";
        $rs   = $this->db->query($sql);
        $data['result_leave_type'] = $rs->result_array();

        $sql  = "select * from t_approve_desc ";
        $rs   = $this->db->query($sql);
        $data['result_approve_stat'] = $rs->result_array();

        $sql  = "select * from t_reject_desc ";
        $rs   = $this->db->query($sql);
        $data['result_reject_stat'] = $rs->result_array();

		$this->load->view('request_report',$data);

	}  // end of show request list




	public function request_report_filter()
	{
	if($this->input->post("btfind")!=null)
	{
		$selected_date_start = $this->input->post('leave_date_start');
		$selected_date_end = $this->input->post('leave_date_end');

		$selected_leave_type = $this->input->post('leave_type');
		$filter_leave_type = $this->input->post('leave_type_hid');

		$selected_approve_stat = $this->input->post('leave_approve');
		$filter_approve_stat = $this->input->post('approve_stat_hid');
		
		$selected_reject_stat = $this->input->post('leave_reject');
		$filter_reject_stat = $this->input->post('reject_stat_hid');

		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
      
		        $emp_id = $session_data['emp_id'];

		// number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		// filter value to show in view
		//$sql = "select * from v_leave_transaction where emp_id = '$emp_id' and approve_stat='$selected_approve_stat'"; 
		$sql = "select * from v_leave_transaction where emp_id = '$emp_id' and leave_type_id $filter_leave_type '$selected_leave_type' and approve_stat $filter_approve_stat '$selected_approve_stat' and reject_stat $filter_reject_stat '$selected_reject_stat' and leave_date between '$selected_date_start' and '$selected_date_end'"; 
		
		$rs = $this->db->query($sql);
		$data['rs'] = $rs->result_array();
		       
		}
			      else
		{
			        //If no session, redirect to login page
			        redirect('login', 'refresh');
		}

		// selection value to show in option of listbox filter
		$sql  = "select * from t_leave_type ";
        $rs   = $this->db->query($sql);
        $data['result_leave_type'] = $rs->result_array();

        
        $sql  = "select * from t_approve_desc ";
        $rs   = $this->db->query($sql);
        $data['result_approve_stat'] = $rs->result_array();

        $sql  = "select * from t_reject_desc ";
        $rs   = $this->db->query($sql);
        $data['result_reject_stat'] = $rs->result_array();
        // end of selection value

		$this->load->view('request_report',$data);
		
		
		
	} 


	}  // end of show request list


	public function report_request_detail($id)
	{
		// edit record
		/*
		if($this->input->post("btedit")!=null)
		{
			$data_comp = array(
   				'comp_name' 	=> $this->input->post("comp_name") ,
   				'comp_address' 	=> $this->input->post("comp_addr")  ,
   				'comp_logo' 	=> $this->input->post("comp_logo") 
			);
			$this->db->where('id',$id);
			$this->db->update('t_company', $data_comp); 
			redirect("company_c","refresh");
			exit();
		}
		*/
		// end of edit record

		// select for show to edit

		
		// end of select to show
		
		if($this->session->userdata('logged_in'))
      			{
		        $session_data = $this->session->userdata('logged_in');
		        $data['username'] = $session_data['username'];

		        $data['emp_name'] = $session_data['emp_name'];
		        $data['emp_lastname'] = $session_data['emp_lastname'];
		        $data['emp_id'] = $session_data['emp_id'];
		        $data['role_id'] = $session_data['role_id'];
        		$data['role_description'] = $session_data['role_description'];
		        $emp_id = $session_data['emp_id'];


		        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();

		
		$sql = "select * from v_leave_transaction where trans_id = '$id' and emp_id = '$emp_id' ";
		$rs = $this->db->query($sql);

		if($rs->num_rows()==0)
		{
			$data['rs'] = array();
		}	
		else
		{
				$data['rs'] = $rs->row_array();
		}	        
		       
		}
			      else
		{
			        //If no session, redirect to login page
			    redirect('login', 'refresh');
		}


		$this->load->view('view_report_request_detail',$data);

	}	// end of show request detail function
    
    public function export_report() {

		// Load database and query
    $this->load->database();
    $query = $this->db->get('v_leave_transaction');

    // Load database utility class
    $this->load->dbutil();
    // Create CSV output
    $data = $this->dbutil->csv_from_result($query);

    // Load download helper
    $this->load->helper('download');
    // Stream download
    force_download('toto.csv', $data);
    
    }

} // end of class
    


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */