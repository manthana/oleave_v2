<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Dashboard_c extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function index()
    {

      

    if($this->session->userdata('logged_in'))
      {
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        $data['emp_name'] = $session_data['emp_name'];
        $data['emp_lastname'] = $session_data['emp_lastname'];
        $data['emp_id'] = $session_data['emp_id'];
        $data['role_id'] = $session_data['role_id'];
        $data['role_description'] = $session_data['role_description'];
      

        $emp_id = $session_data['emp_id'];

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0 ";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

        // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
        
        
        // select my request list to show
        $sql  = "select * from v_leave_transaction where emp_id = '$emp_id' and approve_stat = 0 and reject_stat not in ('1','3') order by trans_id desc";
        $rs   = $this->db->query($sql);
        $data['rs'] = $rs->result_array();
        

        // select my leave balance to show
        $stat = 1;
        $query = $this->db->get_where('v_leave_balance',array('emp_id' => $emp_id , 'fav_flag' => $stat ) );
        $data['result_leave_balance'] = $query;

        // select leave limit to show
        $sql  = "select working_startdate, probation, working_year, leave_description,
        your_bonus_limit,round((your_bonus_limit/8),1) as your_bonus_limit_days , your_comp_limit
        ,round((your_comp_limit/8),1) as your_comp_limit_days  
        from v_working_leave_limit where emp_id = '$emp_id' ";
        $rs   = $this->db->query($sql);
        $data['result_leave_limit'] = $rs->result_array();
        
        // select check change new password
       
        $sql  = "select * from v_change_pass where emp_id = '$emp_id' ";
        $rs   = $this->db->query($sql);
        $data['result_change_pass'] = $rs->result_array();

        $this->load->view('dashboard',$data);
    }
            else
      {
        //If no session, redirect to login page
        redirect('login', 'refresh');
	  }

    
  }
  
  public function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    redirect('login', 'refresh');
  }


}

?>