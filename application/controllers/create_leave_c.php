<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create_leave_c extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

	if($this->session->userdata('logged_in'))
      {
        $session_data = $this->session->userdata('logged_in');
        $data['username'] = $session_data['username'];
        $data['emp_name'] = $session_data['emp_name'];
        $data['emp_lastname'] = $session_data['emp_lastname'];
        $data['emp_id'] = $session_data['emp_id'];
        $emp_id =  $session_data['emp_id'];
        $data['role_id'] = $session_data['role_id'];
        $data['role_description'] = $session_data['role_description'];
      

       

        // number of alert leave request
        $sql  = "select * from v_count_leave_request where emp_id = '$emp_id' and approve_stat = 0";
        $rs   = $this->db->query($sql);
        $data['rs_count_request'] = $rs->result_array();

         // number of alert approved leave request
        $sql  = "select emp_id, approver_id, sum(leave_num) as leave_num from v_count_leave_request where emp_id = '$emp_id' and approve_stat > 0 group by emp_id,approver_id";
        $rs   = $this->db->query($sql);
        $data['rs_count_approved_request'] = $rs->result_array();

        // number of alert approve leave
        $sql  = "select approver_id,sum(leave_num) as leave_num  from v_count_leave_request where approver_id = '$emp_id' and approve_stat = 0 having approver_id is not null";
        $rs   = $this->db->query($sql);
        $data['rs_count_approve'] = $rs->result_array();
        
        // select config table for insert select field
        
        //$query = $this->db->get('t_working_hour');
		//$data['result_working_hour'] = $query;
        $sql  = "select * from t_working_hour";
        $rs   = $this->db->query($sql);
        $data['result_working_hour'] = $rs->result_array();

     

		//$query = $this->db->get('t_leave_type');
		//$data['result_leave_type'] = $query;
		$sql  = "select * from t_leave_type";
        $rs   = $this->db->query($sql);
        $data['result_leave_type'] = $rs->result_array();

		//$query = $this->db->get('t_leave_time');
		//$data['result_leave_time'] = $query;
		$sql  = "select * from t_leave_time";
        $rs   = $this->db->query($sql);
        $data['result_leave_time'] = $rs->result_array();



        $this->load->view('create_leave',$data);
      }
      else
      {
        //If no session, redirect to login page
        redirect('login', 'refresh');
	  }
	
	}
	// end of index function

	public function add_leave()
	{
		
		if($this->session->userdata('logged_in'))
			      	{
					        $session_data = $this->session->userdata('logged_in');
					        $data['username'] = $session_data['username'];
					        $data['emp_name'] = $session_data['emp_name'];
					        $data['emp_lastname'] = $session_data['emp_lastname'];
					        $data['emp_id'] = $session_data['emp_id'];
					       	$data['approver_email'] = $session_data['approver_email'];
					       	$data['role_id'] = $session_data['role_id'];
        					$data['role_description'] = $session_data['role_description'];
      

					        $emp_id 			= $session_data['emp_id'];
					        $emp_email 			= $session_data['username'];
					        $emp_name  			= $session_data['emp_name'];
					        $emp_lastname       = $session_data['emp_lastname'];
					        $approver_name  	= $session_data['approver_name'];
					        $approver_email  	= $session_data['approver_email'];
		


					if($this->input->post("btsave")!=null)
					{
						
						
						$data_leave = array(
			   				//'comp_name' 	=> $this->input->post("comp_name") ,
			   				//'comp_address' 	=> $this->input->post("comp_addr")  ,
			   				
			   				'leave_type_id' 	=> $this->input->post("leave_type"),
			   				'working_hour_id' 	=> $this->input->post("working_hour"),
			   				'leave_date' 		=> $this->input->post("leave_date"), 
			   				'leave_hour'		=> $this->input->post("leave_hour_hid"),
			   				'start_leave'		=> $this->input->post("leave_start_hid"),
			   				'end_leave'			=> $this->input->post("leave_end_hid"),
			   				'leave_reason'		=> $this->input->post("leave_reason"),
			   				'emp_id'			=> $this->input->post("emp_id")


			 	  		
						);

						$this->db->insert('t_leave_transaction', $data_leave); 

					$date_leave =  $this->input->post("leave_date");
					$leave_reason =  $this->input->post("leave_reason");

					// send email for alert user 
					$message_mail = "ขออนุมัติลางาน โดย : ";
					
					$message_body = $message_mail.' '.$emp_name.' '.$emp_lastname .' วันที่ลา '.$date_leave .' หมายเหตุ '.$leave_reason;

					$this->load->library('phpmailer');
					$this->phpmailer->IsSMTP();     // ใช้งาน SMTP
			        $this->phpmailer->SMTPAuth   = true;   // เปิดการการตรวจสอบการใช้งาน SMTP
			        //$this->phpmailer->SMTPSecure = "ssl";  // protocol ที่จะใช้เชื่อมต่อกับ server
			       // $this->phpmailer->Host       = "smtp.gmail.com";      // ตั้งค่า mail server ของเรานะครับ ตัวอย่างจะใช้ของ Gmail นะครับ
			        $this->phpmailer->Host       = "mail.bdgcenter.com";
			        //$this->phpmailer->Port       = 465;  
			        $this->phpmailer->Port       = 25;                   //  port ที่ใช้  ถ้าเป็นของ hosting ทั่วไปส่วนใหญ่เป็น 25 นะครับ
			        $this->phpmailer->Username   = "leave@bdgcenter.com";  //  email address
			        $this->phpmailer->Password   = "leave123";            // รหัสผ่าน Gamil
			        $this->phpmailer->SetFrom('leave@bdgcenter.com', 'BDGroup Leave Management System');  //ผู้ส่ง
			        $this->phpmailer->AddReplyTo("leave@bdgcenter.com","BDGroup Leave Management System");  //email ผู้รับเมื่อมีการตอบกลับนะครับ
			        $this->phpmailer->Subject    = "ขออนุมัติลางาน"; //หัวข้ออีเมล์
			        $this->phpmailer->Body      = $message_body; //ส่วนนี้รายละเอียดสามารถส่งเป็นรูปแบบ HTML ได้
			        //$this->phpmailer->AltBody    = "Plain text message"; //ส่วนนี้ส่งเป็นข้อมูลอย่างเดียวเท่าสนั้น แล้วแต่จะเปิดใช้นะครับ
			        $this->phpmailer->AddAddress( $approver_email, $approver_name); //อีกเมล์ผู้รับ  สามารถเพิ่มได้มากกว่า 1
			        $this->phpmailer->AddAddress( $emp_email, $emp_name); //ตัวอย่างการพิ่มได้มากกว่า 1
			 
			        //$this->phpmailer->AddAttachment("images/phpmailer.gif");      // การแนบไฟล์ถ้าต้องการ สามารถเพิ่มได้มากกว่า 1 เช่นกันครับ
			        //$this->phpmailer->AddAttachment("images/phpmailer_mini.gif"); // ตัวอย่างการพิ่มได้มากกว่า 1
			        if(!$this->phpmailer->Send()) {
			            $data["message"] = "Error: " . $this->phpmailer->ErrorInfo;
			        } else {
			        	
			            $data["message"] = $message_mail + $emp_name + $emp_lastname;
			        }
			        //$this->load->view('sent_mail',$data);
					// end of send email

						redirect("request_list_c","refresh");
						exit();
						
					}
					// end of insert method

					
					       
				}
						      else
				{
						        //If no session, redirect to login page
						        redirect('login', 'refresh');
				}
					// end of user session

					$this->load->view('dashboard',$data);
				}		
		
	// end of add_function
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */