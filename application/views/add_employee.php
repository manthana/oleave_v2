<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Employee - Leave managemant system</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>

    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />




    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>


                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>

            </div>


           <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
               
                <ul class="nav navbar-nav">
                </ul>
              


                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ คุณ <b><?php echo $emp_name;?>&nbsp;<?php echo $emp_lastname;?></b> <b class="caret"></b> 
                            
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                   
                    <!-- <li class="hidden-xs">
                        <a class="toggle-left" href="#">
                            <span style="font-size:20px;" class="entypo-list-add"></span>
                        </a>
                    </li> -->
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

     <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
      ?>

    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr" style = "min-height:600px;">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>Add Employee
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                               <!--  ยินดีต้อนรับ คุณ &nbsp;
                                <strong><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></strong> -->
                                <!-- &nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM -->
                                
                            </div>


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        
                        

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Dashboard</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>

                
                <li class="pull-right">
                    <!-- <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div> -->
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->

        <div class="content-wrap">
            <!-- start table request-->
            <div class="row">
                  <?php echo form_open('employee_c/add_employee');?>
           <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> เพิ่มข้อมูลรายชื่อพนักงานใหม่ </h3>
              </div>
              <div class="panel-body">
                
                <div class="row">


            <div class="panel panel-info">
                
                <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">รหัสพนักงาน</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่รหัสพนักงาน" name ="emp_id">
                  </div>

                  <div class="col-xs-2">วันที่เริ่มงาน</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่วันที่เริ่มงาน" name ="working_startdate" DISABLED>
                  </div>
                </div>  

              </div>

               <div class="panel-heading">
                <div class="row">
                 <div class="col-xs-2">ชื่อ</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่รหัสพนักงาน" name ="emp_name">
                  </div>

                  <div class="col-xs-2">นามสกุล</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่วันที่เริ่มงาน" name ="emp_lastname">
                  </div>
                </div>
               </div>


                <div class="panel-heading">
                <div class="row">
                 <div class="col-xs-2">E-mail</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่ E-mail พนักงาน" name ="emp_email">
                  </div>

                  <div class="col-xs-2">Password</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="กำหนด password" name ="emp_password">
                  </div>
                </div>
               </div>


                <div class="panel-heading">
                <div class="row">
                 <div class="col-xs-2">สังกัดบริษัท</div>
                  <div class="col-xs-3 text-right">
                    <select class="form-control" id="leave_approve" name="leave_approve" onchange="setFilterApprove()">
                    <option value="99">ทั้งหมด</option>
                    <option value="0">บริษัท อังกฤษตรางู (แอล.พี.) จำกัด</option><option value="1">บริษัท ห้างขายยาอังกฤษ (ตรางู) จำกัด</option></select>
    <input type="hidden" id="approve_stat_hid" name="approve_stat_hid" value="<>">
                  </div>

                  <div class="col-xs-2">แผนก</div>
                   <div class="col-xs-3 text-right">
                    <select class="form-control" id="leave_approve" name="leave_approve" onchange="setFilterApprove()">
                    <option value="99">ทั้งหมด</option>
                    <option value="0">BD-HO</option><option value="1">BD-BD120</option><option value="2">BD-Inter</option><option value="3">BD-Oem</option><option value="4">BD-Trading</option><option value="5">BD-MIS</option></select>
                     <input type="hidden" id="approve_stat_hid" name="approve_stat_hid" value="<>">
                   </div>

                </div>
               </div>

               <div class="panel-heading">
                <div class="row">
                 <div class="col-xs-2">สิทธิ์ผู้ใช้งาน</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่ role id" name ="role_id">
                  </div>

                  <div class="col-xs-2">level</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="กำหนด Level" name ="comp_name" DISABLED>
                  </div>
                </div>
               </div>

               <div class="panel-heading">
                <div class="row">
                 <div class="col-xs-2">รหัสผู้อนุมัติ</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่ รหัสผู้อนุมัติ" name ="approve_id">
                  </div>

                  <div class="col-xs-2">Status</div>
                  <div class="col-xs-3 text-right">
                      <input class="form-control" placeholder="ใส่ emp_status" name ="emp_status" DISABLED>
                  </div>
                </div>
               </div>


               <div class="panel-heading">
                <div class="row">
                      <div class="col-xs-4">
                    
                        
                      </div>
                      <div class="col-xs-30px; text-right">

    <input type="submit" name="btsave" class="btn btn-primary" value ="บันทึกข้อมูล"></button>&nbsp;&nbsp;&nbsp;
    <button type="button" class="btn btn-danger" ONCLICK="window.location.href='<?php echo base_url();?>index.php/employee_c/index'">&nbsp;&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;&nbsp;</button>     
                      </div>
                </div>  

              </div>
            </div>
  
        <?php echo form_close();?>

                </div>

      <!-- end of my page-->
              </div>

            </div>
            </div>
        </div> <!-- end class="content-wrap" -->
           
    <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Oleave Managment
                        <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->

        </div> <!--  end container-fluid   -->
    </div>  
    <!--  END OF PAPER WRAP -->

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>



    
    <!-- /MAIN EFFECT -->

     <!-- JavaScript -->
   
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    
  
</body>

</html>
