<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8">

  <title>BDG leave management online system - Login Form</title>

     <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/style.css" media="screen" type="text/css" />

  </head>
  <body>
    <div class="login">
    
    <h2 style="text-align:center ; color:white;">ระบบใบลา online BDGroup </h2>
    <h3 style="text-align:center ; color:white;">version 1.0</h3>
    
    <?php echo form_open('verifylogin'); ?>
  </div>

      <div class="login">
    <div class="login-screen">
      <div class="app-title">
        <!-- <h1>Login</h1> -->
        <img src="<?php echo base_url();?>/assets/images/comp_logo.gif">
          
      </div>
      <div class="app-title">
        &nbsp;
      </div>
      <div class="login-form">
        <div class="control-group">
        <input type="text" class="login-field" value="" placeholder="your email" id="login-name" name="username">
        <label class="login-field-icon fui-user" for="login-name"></label>
        </div>

        <div class="control-group">
        <input type="password" class="login-field" value="" placeholder="password" id="login-pass" name="password">
        <label class="login-field-icon fui-lock" for="login-pass"></label>
        </div>

       
       <?php echo validation_errors(); ?>
       <input type="submit"  class="btn btn-primary btn-large btn-block" value="Login"/>
        <a class="login-link" href="#">Lost your password?</a>
      </div>
    </div>
  </div>


    </form> 
  </body>
</html>
