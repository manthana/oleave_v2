<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Employee - Leave managemant system</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->


    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css"> 

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">
	
	<link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />

	<link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">



<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
	 <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
    	<div class="container-fluid">
    		<!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>


                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>
            </div>


			<!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            	<ul class="nav navbar-nav">
                </ul>

                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                	<li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ </b> <b class="caret"></b> 
                            
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>            	
            </div>



    		
    	</div>
    </nav>

    <?php     
        $this->load->view('nav_crud');      
    ?>


    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
    	<div class="container-fluid paper-wrap bevel tlbr">

    		<!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>Employee List
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>                 
                    </div>
                </div>
            </div><!--END OF TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Dashboard</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>
                
               <!--  <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li> -->
            </ul><!-- END OF BREADCRUMB -->
	
	<div class="content-wrap">
    	<div class="col-sm-12">
    		<div id="page-wrapper">
    			<div class="nest" id="FootableClose">
    				<div style='height:20px;'></div>  
    <div>
		<?php echo $output; ?>
    </div>
    			</div>    			
    		</div>    		
    	</div>    	
    </div>


    <!-- FOOTER -->
    <div class="footer-space"></div>
    <div id="footer">
        <div class="devider-footer-left"></div>
        <div class="time">
            <p id="spanDate"></p>
            <p id="clock"></p>
        </div>
        <div class="copyright">Oleave Managment
        <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
        <div class="devider-footer"></div>
    </div>
                <!-- / END OF FOOTER -->
  		

    	</div> <!-- END OF container-fluid paper-wrap -->
    </div><!-- END OF PAPER WRAP -->



	
	<div>
		<!-- <a href='<?php echo site_url('home_c/index')?>'>Dashboard</a> | -->
		<!-- <a href='<?php echo site_url('examples/orders_management')?>'>Orders</a> |
		<a href='<?php echo site_url('examples/products_management')?>'>Products</a> |
		<a href='<?php echo site_url('examples/offices_management')?>'>Offices</a> | 
		<a href='<?php echo site_url('examples/employees_management')?>'>Employees</a> |		 
		<a href='<?php echo site_url('examples/film_management')?>'>Films</a> | 
		<a href='<?php echo site_url('examples/film_management_twitter_bootstrap')?>'>Twitter Bootstrap Theme [BETA]</a> | 
		<a href='<?php echo site_url('examples/multigrids')?>'>Multigrid [BETA]</a>
		 -->
	</div>
	


    
	
	
	<!-- END OF RIGHT SLIDER CONTENT-->
	<!--
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>
-->

	





    <!-- MAIN EFFECT -->
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>


	<!-- JavaScript -->
   
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>


</body>
</html>
