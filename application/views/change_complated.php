<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Change Password - Leave managemant system</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">
</head>

<body>

  <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>

                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>
            </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">               
                <ul class="nav navbar-nav"></ul>              
                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ คุณ <b><?php echo $emp_name;?>&nbsp;<?php echo $emp_lastname;?></b> <b class="caret"></b> 
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
      </nav> 

<!-- Sidebar -->
    <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
    ?>
                    

  <!--  PAPER WRAP -->
  <div class="wrap-fluid">
  <div class="container-fluid paper-wrap bevel tlbr" style = "min-height:600px;">
     
<!-- CONTENT -->
    <!--TITLE Open-->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>Change Password
                            </span>
                        </h2>
                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                               <!--  ยินดีต้อนรับ คุณ &nbsp;
                                <strong><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></strong> -->
                                <!-- &nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM -->                   
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>                
                       
                    </div>
                </div>
              
            </div>
            <!--/ TITLE end -->

    <!-- /END OF CONTENT -->
            
       <!-- filter section-->
       <div class="col-lg-12">
              <div class="panel panel-primary">

<!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Chang Password Completed</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>
                <li class="pull-right">
                   <!--  <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div> -->
                </li>
            </ul>
       
    <!-- END OF BREADCRUMB -->

              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> เปลี่ยน รหัสผ่านใหม่ สำเร็จ ( Change Password Completed )</h3>
              </div>
              <div class="panel-body">
                
          <!-- filter input-->
          <div class="row">
           <?php echo form_open('dashboard_c/index');?>
           <div class="col-lg-12">
            <div class="panel panel-info">
           
                <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">
                 
                  </div>
                  <div class="col-xs-10 text-left">
                      <H3>ระบบทำการเปลี่ยน รหัสผ่านใหม่ เรียบร้อยแล้ว</H3>
                <!-- <input type="password" size="30" id="my_new_pass" value="" name="my_new_pass" required onchange="checkPass()" value="aaa"/> -->
                  </div>
                </div>  
              </div>       

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">
                    
                  </div>
                  <div class="col-xs-10 text-left">

                 <br/>
                  <input type="submit" name="btsave" class="btn btn-primary" id="btsave" value ="กลับไป หน้าหลัก" ></button> 
                  </div>
                </div>  

              </div>
            </div>
          </div>

          <?php echo form_close();?>                
          </div>
          <!-- end of filter input-->

              </div>
            </div>
          </div>
          <!-- end of filter section-->

          <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Oleave Managment
                        <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
                    <div class="devider-footer"></div>
                </div>
    </div> <!--End container-fluid paper-wrap bevel tlbr-->

  <div id="wrapper">
  
      <!-- PAPER WRAP -->
      <div id="page-wrapper">

            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
              <!-- <div class="row">
              </div> -->
            </div>
            <!--  / DEVICE MANAGER -->

            <div class="content-wrap">
            <!-- start table request-->
                       
                <div class="row">
               
                </div>          
            </div><!-- content-wrap -->
      </div><!--end id=page-wrapper -->
    
  </div><!--end id=wrapper-->

 <!-- <div id="wrapper">
  
      <!-- PAPER-WRAP-->
      <div id="page-wrapper">
 
            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
              <!-- <div class="row">
              </div> -->
            </div>
            <!--  END DEVICE MANAGER -->

            <div class="content-wrap">
            <!-- start table request-->
                       
                <div class="row">
               
                </div>          
            </div><!-- content-wrap -->
      </div><!-- page-wrapper -->
      <!--  END OF PAPER WRAP -->
 </div> <!-- END class="wrap-fluid"-->

           
      <!-- /END OF CONTENT -->

       
    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>


    <!-- MAIN EFFECT -->   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>


    <!-- JavaScript -->  
    <script src="<?php echo basse_url();?>/assets/js/bootstrap.js"></script>


    <!-- Page Specific Plugins -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/morris/chart-data-morris.js"></script>
    <script src="<?php echo base_url();?>/assets/js/tablesorter/jquery.tablesorter.js"></script>
    <script src="<?php echo base_url();?>/assets/js/tablesorter/tables.js"></script>

  </body>
</html>
