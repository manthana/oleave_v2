<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>view Request Detail</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
  
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css">
    
    <!-- Add css and javascript for date pciker-->
    
    <link type="text/css" href="<?php echo base_url();?>assets/css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
    <script type="text/javascript">
     $(function () {
        var d = new Date();
        var toDay = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getFullYear() + 543);


        // กรณีต้องการใส่ปฏิทินลงไปมากกว่า 1 อันต่อหน้า ก็ให้มาเพิ่ม Code ที่บรรทัดด้านล่างด้วยครับ (1 ชุด = 1 ปฏิทิน)

       /* $("#datepicker-th").datepicker({ dateFormat: 'dd/mm/yy', isBuddhist: true, defaultDate: toDay, dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
              dayNamesMin: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.'],
              monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
              monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.']});

        $("#datepicker-th-2").datepicker({ changeMonth: true, changeYear: true,dateFormat: 'dd/mm/yy', isBuddhist: true, defaultDate: toDay,dayNames: ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์'],
              dayNamesMin: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.'],
              monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
              monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.']});
       
         $("#inline").datepicker({ dateFormat: 'dd/mm/yy', inline: true });
          */

            $("#datepicker-en").datepicker({ dateFormat: 'yy-mm-dd 00:00:00'});

       


      });
    </script>

    <style type="text/css">

      .demoHeaders { margin-top: 2em; }
      #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
      #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
      ul#icons {margin: 0; padding: 0;}
      ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
      ul#icons span.ui-icon {float: left; margin: 0 4px;}
      ul.test {list-style:none; line-height:30px;}
    </style>
    <!-- end of date picker-->


  </head>

  <body onload="getDate()">

    <div id="wrapper">

      <!-- Sidebar -->
     <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?>



      <div id="page-wrapper">


        <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> ข้อมูลรายละเอียดใบลา </h3>
              </div>
              <div class="panel-body">
                
         <div class="row">
        

          <!-- start entry leave -->
          
          <?php echo form_open('request_list_c/cancel_request_detail');?>
           <div class="col-lg-12">
            <div class="panel panel-info">

              <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <h3>leave request detail ( ข้อมูล รายละเอียด ใบลา )</h3>
                    </div>  
                  </div>
                </div>

              <div class="panel-heading">
                <div class="row">
                    <input type="hidden" id="trans_id_hid" name="trans_id_hid" value="<?php echo $rs['trans_id'];?>">
                    <input type="hidden" id="reject_stat_hid" name="reject_stat_hid" value="1">
                   
                    <input type="hidden" id="reject_stat_after_hid" name="reject_stat_after_hid" value="<?php echo $rs['reject_stat'];?>">
                    <input type="hidden" id="reject_reason_hid" name="reject_reason_hid" value="<?php echo $rs['cancel_leave_reason'];?>">
                    
                    <div class="col-xs-3">
                      สถานะ การยกเลิก ใบลา
                    </div>
                    <div class="col-xs-3 text-left">
                      <input class="form-control" id="inputSuccess" type="text" value="<?php echo $rs['id_desc'];?>" disabled="">
                    </div>
                    <div class="col-xs-2">
                      วันทียกเลิกใบลา
                    </div>
                    <div class="col-xs-2 text-center">
                      <input class="form-control" name="approve_desc" value="<?php echo $rs['reject_date'];?>" disabled="">
                    </div>
                </div>  



              </div>
              <div class="panel-heading">
                <div class="row">
                    <input type="hidden" id="trans_id_hid" name="trans_id_hid" value="<?php echo $rs['trans_id'];?>">
                    <input type="hidden" id="reject_stat_hid" name="reject_stat_hid" value="1">
                   
                    <input type="hidden" id="reject_stat_after_hid" name="reject_stat_after_hid" value="<?php echo $rs['reject_stat'];?>">
                    <input type="hidden" id="reject_reason_hid" name="reject_reason_hid" value="<?php echo $rs['cancel_leave_reason'];?>">
                    
                    <div class="col-xs-3">
                      สถานะ การอนุมัติ ใบลา
                    </div>
                    <div class="col-xs-3 text-left">
                      <input class="form-control" id="inputSuccess" type="text" value="<?php echo $rs['approve_desc'];?>" disabled="">
                    </div>
                    <div class="col-xs-2">
                      วันที่ขอลา
                    </div>
                    <div class="col-xs-2 text-center">
                      <input class="form-control" name="approve_desc" value="<?php echo $rs['leave_date'];?>" disabled="">
                    </div>
                </div>  

                

              </div>

            <!--   <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                    วันที่ลา
                  </div>
                  <div class="col-xs-9 text-left">

                <input type="text" size="40" id="datepicker-en" disabled=""  name="leave_date" value=" <?php echo $rs['leave_date'];?>"/>
                
                
                  </div>
                </div>  

              </div> -->
              

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">

                    ประเภทการลา
                  </div>
                  <div class="col-xs-3 text-right">
                   <input class="form-control" name="approve_desc" value="<?php echo $rs['leave_description'];?>" disabled="">
                  </div>
                   <div class="col-xs-1">
                      จำนวน
                    </div>
                    <div class="col-xs-2 text-center">
                      <input class="form-control" name="approve_desc" value="<?php echo $rs['leave_hour'];?>" disabled="">
                    </div>
                    <div class="col-xs-1">
                      ชั่วโมง
                    </div>
                </div>  

              </div>

             <!--   <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    ระยะเวลาการลา
                  </div>
                  <div class="col-xs-9 text-right">

                <select class="form-control" name = "working_hour" disabled="" >

                <option><?php //echo $rs['work_description'];?></option>

                </select>
                  </div>
                </div>  

              </div> -->

               

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                  เวลาเริ่มต้นที่ลา
                  </div>
                  <div class="col-xs-2 text-right">

                  <select class="form-control" name="leave_start" disabled="" >

                    <option><?php echo $rs['start_leave'];?></option>
                        
                  </select>
                  </div>
                  <div class="col-xs-2" style="text-align:center;">
                   
                  เวลาสิ้นสุดที่ลา
                  </div>
                  <div class="col-xs-2 text-right">

                      <select class="form-control" name="leave_end" disabled="" >
                    
                        <option><?php echo $rs['end_leave'];?></option>                 
        
                      </select>
                  </div>
                </div>  

              </div>

             <!--  <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    รวมจำนวนชั่วโมงที่ขอลา
                  </div>
                  <div class="col-xs-3 text-right">
                  <input class="form-control" name="leave_hour" value="<?php //echo $rs['leave_hour'];?>" disabled="">
                  
                  </div>
                  <div class="col-xs-3">
                    
                    ชั่วโมง
                  </div>
                  
                </div>  

              </div> -->

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                  เหตุผลในการลา
                  </div>
                  <div class="col-xs-9 text-right">

                    <textarea class="form-control" rows="3" name="leave_reason" disabled="" ><?php echo $rs['leave_reason'];?></textarea>
                  </div>
                </div>  

              </div>
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                  เหตุผลในขอ ยกเลิกใบลา
                  </div>
                  <div class="col-xs-9 text-right">

                    <textarea class="form-control" rows="3" id ="reject_leave_reason" name="reject_leave_reason" disabled="" ><?php echo $rs['cancel_leave_reason'];?></textarea>
                  </div>
                </div>  

              </div>

<!--                <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    เอกสารแนบ
                  </div>
                  <div class="col-xs-9 text-right">

                <input type="file">
                  </div>
                </div>  

              </div> -->

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                   <input type="hidden" name="emp_id" value="<?php echo $emp_id;?>"> 

                  </div>
                  <div class="col-xs-3 text-left">

                 
                  <!--<input type="button" name="btback" class="btn btn-primary" value ="กลับไปหน้า รายการใบลา"></button>-->
                  <?php
                  echo'<button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                  echo "'".base_url()."index.php/request_list_c/cancel_request_list'";
                  echo '">กลับไปหน้า รายการใบลา</button></td>';
                  ?>

                  </div>
                  <div class="col-xs-4 text-left">

                  <!--
                  <input type="submit" id ="btcancel" name="btcancel" class="btn btn-danger" value ="ขอยกเลิก รายการใบลานี้" ></button> 
                  -->
                  </div>
                </div>  

              </div>


              
            </div>
          </div>
          <?php echo form_close();?>  
          <!-- -->

        </div>

              </div>
            </div>
          </div>

      <!-- end of my page-->


        

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
   
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
   
    <script>

        /*
        function getDate(){

          var reject_stat = document.getElementById('reject_stat_after_hid').value
          if (reject_stat == 1 || reject_stat == 3){
            alert("cancel");
            document.getElementById('btcancel').hidden = true;

          }

          //alert("load");
         // dd = Date();
          var d = new Date();
          var curr_date = d.getDate();
          var curr_month = d.getMonth();
          var curr_year = d.getFullYear();
          //reject_date_hid
          var curr_month_add = curr_month+1;

          //alert(curr_month);
          //alert(curr_month_add);
          if (curr_month_add<10){
              var current_date = curr_year+'-0'+curr_month_add+'-'+curr_date;            
          } else{
              var current_date = curr_year+'-'+curr_month_add+'-'+curr_date;
          }

          //alert(current_date);
          document.getElementById('reject_date_hid').value = current_date;

         
          document.getElementById('btcancel').disabled = true;
         
        }
        */
    </script>

    <script>
    /*
    function check_reason(){
      var reject_reason = document.getElementById('reject_leave_reason').value ;
      //var len_reason = reject_reason;
      //alert(reject_reason);

      
      
      
      
    }
    */
    </script>


  </body>
</html>