<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create New Company</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel="stylesheet">
  
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>/assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/font-awesome/css/font-awesome.min.css">
    

   
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?>



      <div id="page-wrapper">


        <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> เพิ่มข้อมูลรายชื่อบริษัท </h3>
              </div>
              <div class="panel-body">
                
                <div class="row">


          <!-- start entry leave -->
        <?php echo form_open('company_c/add_comp');?>
           <div class="col-lg-12">

            <div class="panel panel-info">

              <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <h3>Create new company ( สร้าง รายชื่อ บริษัท )</h3>
                    </div>  
                  </div>
                </div>

              

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    ชื่อบริษัท
                  </div>
                  <div class="col-xs-9 text-right">
                      <input class="form-control" placeholder="ใส่ชื่อบริษัท" name ="comp_name">
                  </div>
                </div>  

              </div>

               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    ที่อยู่บริษัท
                  </div>
                  <div class="col-xs-9 text-right">

                <textarea class="form-control" rows="3" placeholder="ใส่ที่อยู่บริษัท" name = "comp_addr"></textarea>
                  </div>
                </div>  

              </div>


               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    รูป logo บริษัท <br> ขนาด aa px x bb px
                  </div>
                  <div class="col-xs-9 text-right">

                <input type="file" name = "comp_logo">
                  </div>
                </div>  

              </div>

              <div class="panel-heading">
                <div class="row">
                      <div class="col-xs-3">
                    
                        
                      </div>
                      <div class="col-xs-9 text-left">

                          <input type="submit" name="btsave" class="btn btn-primary" value ="บันทึกข้อมูล"></button> 
                          &nbsp;&nbsp;&nbsp;
                          <button type="button" class="btn btn-danger" ONCLICK="window.location.href='<?php echo base_url();?>index.php/company_c/index'">&nbsp;&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;&nbsp;</button>
                      </div>
                </div>  

              </div>
            </div>
  
        <?php echo form_close();?>       
          <!-- -->

        </div>

              </div>
            </div>
          </div>

      <!-- end of my page-->


      
        

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="<?php echo base_url();?>/assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.js"></script>
   

  </body>
</html>