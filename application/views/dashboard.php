<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Online Leave Management</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->



    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">



    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>

    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>




                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
               
                <ul class="nav navbar-nav">
                </ul>

               


                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ คุณ <b><?php echo $emp_name;?>&nbsp;<?php echo $emp_lastname;?></b> <b class="caret"></b> 
                            
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                   
                    <!-- <li class="hidden-xs">
                        <a class="toggle-left" href="#">
                            <span style="font-size:20px;" class="entypo-list-add"></span>
                        </a>
                    </li> -->
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

<!-- Sidebar -->
      <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?> 
        


    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">





            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>Dashboard 
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                               <!--  ยินดีต้อนรับ คุณ &nbsp;
                                <strong><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></strong> -->
                                <!-- &nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM -->
                                 <?php
                foreach($result_change_pass as $r)
                  {
                    if ($r['flag_change_pass']==0) 
                      {
                        echo '<a href="'.base_url().'index.php/employee_c">'.$r['flag_change_pass_new'].'</a>';
                      }
                    else
                      {
                        echo "คุณได้ทำการเปลี่ยนรหัสผ่านใหม่ เรียบร้อยแล้ว";
                      }
                      
                  }
             ?>
                            </div>


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        
                        

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Dashboard</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>

                
                <li class="pull-right">
                   <!--  <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div> -->
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->






           <!--  <div id="paper-middle">
                <div id="mapContainer"></div>
            </div> -->

            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <!-- <div class="row">
                   
  

                </div> -->
            </div>
            <!--  / DEVICE MANAGER -->










            <div class="content-wrap">
            <!-- start table request-->
            <div class="row">
            <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                     สิทธิ์ในการลา</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <!-- <span class="entypo-cancel"></span> -->
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

                                <!-- <p class="lead well">FooTable is a jQuery plugin that aims to make HTML tables on smaller devices look awesome - No matter how many columns of data you may have in them. And it's responsive i think this better than DataTable in some way</p>
 -->
                                <table class="table-striped footable-res footable metro-blue" data-page-size="6">
                                    <thead>
                      <tr>
                        <th>ลำดับที่<i class="fa fa-sort"></i></th>
                        <th>วันที่เริ่มทำงาน <i class="fa fa-sort"></i></th>
                        <th>ผ่านทดลองงาน <i class="fa fa-sort"></i></th>
                        <th>จำนวนปี อายุงาน <i class="fa fa-sort"></i></th>
                        <th>ประเภทวันลา<i class="fa fa-sort"></i></th>
                        <th>ชั่วโมง ลาได้ ตามประเมินผล <i class="fa fa-sort"></i></th>
                        <th>วัน ตามประเมินผล <i class="fa fa-sort"></i></th>
                        <th>ชั่วโมง ตามข้อบังคับ <i class="fa fa-sort"></i></th>
                        <th>วัน ตามข้อบังคับ <i class="fa fa-sort"></i></th>
                       
                       
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                        if(count($result_leave_limit)==0)
                        {
                          echo "<tr><td> -- ไม่มีข้อมูลใบลาที่ ขออนุมัติ --</td></tr>";
                        }
                        else
                        {
                          $no=1;
                          foreach($result_leave_limit as $r)
                          {
                            echo"<tr>";
                              echo"<td align='center'>$no</td>";
                              echo"<td>".$r['working_startdate']."</td>";
                              echo'<td>'.$r['probation'].'</td>';
                              
                              echo"<td>".$r['working_year']."</td>";
                              echo"<td>".$r['leave_description']."</td>";
                              echo"<td>".$r['your_bonus_limit']."</td>";
                              echo"<td>".$r['your_bonus_limit_days']."</td>";
                              echo"<td><b>".$r['your_comp_limit']."</td>";
                              echo"<td><b>".$r['your_comp_limit_days']."</td>";
                              //echo"<td><b>".$r['hr_confirm_desc']."</b></td>";

                              //echo'<td align="center"><button type="button" class="btn btn-primary">แก้ไข</button></td>';
                             
                              //echo'<td align="center"><button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                              //echo "'".base_url()."index.php/Request_list_c/view_request_detail/".$r['trans_id']."'";
                              //echo '">รายละเอียด</button></td>';
                              
                              //echo'<td align="center"><button type="button" class="btn btn-danger" ONCLICK="window.location.href=';
                              //echo "'".base_url()."index.php/company_c/del_comp/".$r['id']."'";
                              //echo '">ลบช้อมูล</button></td>';

                            echo"</tr>";
                            $no++;
                          }
                        }
                      ?>
                    </tbody>

                                    <!-- <tbody>
                                        <tr>
                                            <td>Isidra</td>
                                            <td><a href="#">Boudreaux</a>
                                            </td>
                                            <td>Traffic Court Referee</td>
                                            <td data-value="78025368997">22 Jun 1972</td>
                                            <td data-value="1">
                                                <span class="status-metro status-active" title="Active">Active</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Shona</td>
                                            <td>Woldt</td>
                                            <td><a href="#">Airline Transport Pilot</a>
                                            </td>
                                            <td data-value="370961043292">3 Oct 1981</td>
                                            <td data-value="2">
                                                <span class="status-metro status-disabled" title="Disabled">Disabled</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Granville</td>
                                            <td>Leonardo</td>
                                            <td>Business Services Sales Representative</td>
                                            <td data-value="-22133780420">19 Apr 1969</td>
                                            <td data-value="3">
                                                <span class="status-metro status-suspended" title="Suspended">Suspended</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Easer</td>
                                            <td>Dragoo</td>
                                            <td>Drywall Stripper</td>
                                            <td data-value="250833505574">13 Dec 1977</td>
                                            <td data-value="1">
                                                <span class="status-metro status-active" title="Active">Active</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Maple</td>
                                            <td>Halladay</td>
                                            <td>Aviation Tactical Readiness Officer</td>
                                            <td data-value="694116650726">30 Dec 1991</td>
                                            <td data-value="3">
                                                <span class="status-metro status-suspended" title="Suspended">Suspended</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Maxine</td>
                                            <td><a href="#">Woldt</a>
                                            </td>
                                            <td><a href="#">Business Services Sales Representative</a>
                                            </td>
                                            <td data-value="561440464855">17 Oct 1987</td>
                                            <td data-value="2">
                                                <span class="status-metro status-disabled" title="Disabled">Disabled</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lorraine</td>
                                            <td>Mcgaughy</td>
                                            <td>Hemodialysis Technician</td>
                                            <td data-value="437400551390">11 Nov 1983</td>
                                            <td data-value="2">
                                                <span class="status-metro status-disabled" title="Disabled">Disabled</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lizzee</td>
                                            <td><a href="#">Goodlow</a>
                                            </td>
                                            <td>Technical Services Librarian</td>
                                            <td data-value="-257733999319">1 Nov 1961</td>
                                            <td data-value="3">
                                                <span class="status-metro status-suspended" title="Suspended">Suspended</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Judi</td>
                                            <td>Badgett</td>
                                            <td>Electrical Lineworker</td>
                                            <td data-value="362134712000">23 Jun 1981</td>
                                            <td data-value="1">
                                                <span class="status-metro status-active" title="Active">Active</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Lauri</td>
                                            <td>Hyland</td>
                                            <td>Blackjack Supervisor</td>
                                            <td data-value="500874333932">15 Nov 1985</td>
                                            <td data-value="3">
                                                <span class="status-metro status-suspended" title="Suspended">Suspended</span>
                                            </td>
                                        </tr>
                                    </tbody> -->
                                    <!-- <tfoot>
                                        <tr>
                                            <td colspan="9">
                                                <div class="pagination pagination-centered"></div>
                                            </td>
                                        </tr>
                                    </tfoot> -->
                                </table>

                            </div>

                        </div>
            </div>
            <!-- end table request -->
            <!-- start block -->
                <div class="row">
                  <?php
                    foreach ($result_leave_balance->result() as $row)
                    {
                    echo
                   
                    '<!-- new Main block -->
                    
                    <div class="col-sm-4" style="padding:2%;-webkit-box-shadow: 0 8px 6px -6px black;
       -moz-box-shadow: 0 8px 6px -6px black;
            box-shadow: 0 8px 6px -6px black;">
                            
                           <!-- header block -->

                           <div class="profit" id="profitClose" style="background:#E6FFFF;margin-bottom: 0px;">
                                

                                <div class="value">
                                    
                                    <div id="getting-started">
                                        

                                        <h3 style="color:#65C3DF;">( '.$row->leave_description.' )</h3> 
                                    </div>


                                </div>
                            </div>

                            <!-- leave block -->
                            
                            <div class="profit" id="profitClose" style="background:#D9EDF7;margin-bottom: 0px;">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                               
                                            <i class="maki-airport"></i>&#160;&#160;'.$row->leave_description.' - ใช้ไป
                                        
                                        </span>
                                    </h3>
                                    <div class="titleClose">
                                        <a href="#profitClose" class="gone">
                                           <!-- <span class="entypo-cancel"></span> -->
                                        </a>
                                    </div>
                                </div>

                                <div class="value">
                                    <span class="pull-left"><i class="entypo-clock clock-position"></i>
                                    </span>
                                    <div id="getting-started">
                                        <!-- <span>ใช้ไป</span> -->

                                        <span>'.$row->total_leave_hour.'</span> 
                                    </div>


                                </div>

                                <div class="progress-tinny">
                                    <div style="width: 50%" class="bar"></div>
                                </div>
                                <div class="profit-line">
                                 <h4> ใช้ไป (ชั่วโมง)<br><br> หรือ '.$row->leave_use_day.' วัน '.$row->leave_use_hour.' ชั่วโมง </h4>
                                </div>
                            </div>

                            <!-- bonus block -->
                            <div class=" member" id="memberClose" style="background:#E6FFFF;margin-bottom: 0px;">
                                    <div class="headline">
                                        <h3>
                                            <span style="background:#FFA200">
                                                <i class="fa fa-truck"></i>
                                                &#160;&#160;'.$row->leave_description.' - ตามประเมินผล โบนัส
                                            </span>
                                        </h3>
                                        <div class="titleClose">
                                            <a href="#memberClose" class="gone">
                                               <!-- <span class="entypo-cancel"></span> -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="value">
                                       <!-- <span><i class="maki-warehouse"></i> -->
                                        </span>'.$row->bonus_balance.'<b>
                                        </b>

                                    </div>
                                    <div class="progress-tinny">
                                        <div style="width: 50%" class="bar"></div>
                                    </div>
                                    <div class="profit-line">
                                       <h4> คงเหลือ (ชั่วโมง)<br><br> หรือ '.$row->bonus_balance_day.' วัน '.$row->bonus_balance_hour.' ชั่วโมง </h4>
                                    </div>
                            </div>

                            <!-- legal block -->
                            <div class=" member" id="memberClose" style="background:#E7FAFF">
                                    <div class="headline">
                                        <h3>
                                            <span style="background:#65C3DF">
                                                <i class="fa fa-truck"></i>
                                                &#160;&#160;'.$row->leave_description.' - ตามข้อบังคับ บริษัท
                                            </span>
                                        </h3>
                                        <div class="titleClose">
                                            <a href="#memberClose" class="gone">
                                               <!-- <span class="entypo-cancel"></span> -->
                                            </a>
                                        </div>
                                    </div>
                                    <div class="value">
                                       <!-- <span><i class="maki-warehouse"></i> -->
                                        </span>'.$row->legal_balance.'<b>
                                        </b>

                                    </div>
                                    <div class="progress-tinny">
                                        <div style="width: 50%" class="bar"></div>
                                    </div>
                                    <div class="profit-line">
                                       <h4> คงเหลือ (ชั่วโมง)<br><br> หรือ '.$row->legal_balance_day.' วัน '.$row->legal_balance_hour.' ชั่วโมง </h4>
                                    </div>
                            </div>
                        </div>';

}
?> 
                    <!-- end new block -->

                </div>
            <!-- start table request-->
            <div class="row">
            <div class="nest" id="FootableClose">
                            <div class="title-alt">
                                <h6>
                                     รายการใบลาที่ขออนุมัติ</h6>
                                <div class="titleClose">
                                    <a class="gone" href="#FootableClose">
                                        <!-- <span class="entypo-cancel"></span> -->
                                    </a>
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#Footable">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="Footable">

        <!-- <p class="lead well">FooTable is a jQuery plugin that aims to make HTML tables on smaller devices look awesome - No matter how many columns of data you may have in them. And it's responsive i think this better than DataTable in some way</p>-->
            <table class="table-striped footable-res footable metro-blue" data-page-size="6">
                <thead>
                      <thead>
                      <tr>
                        <th>ลำดับที่<i class="fa fa-sort"></i></th>
                        <th>วันที่ลา <i class="fa fa-sort"></i></th>
                        <th>ประเภทการลา <i class="fa fa-sort"></i></th>
                        <th>เริ่ม <i class="fa fa-sort"></i></th>
                        <th>สิ้นสุด<i class="fa fa-sort"></i></th>
                        <th>จำนวนชั่วโมงที่ลา <i class="fa fa-sort"></i></th>
                        <th>เหตุผล <i class="fa fa-sort"></i></th>
                        <th>สถานะ <i class="fa fa-sort"></i></th>
                        <!-- <th>ฝ่ายบุคคล <i class="fa fa-sort"></i></th> -->
                        <th>ดูรายละเอียด <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>

                <tbody>                      
                      <?php
                        if(count($rs)==0)
                        {
                          echo "<tr><td> -- ไม่มีข้อมูลใบลาที่ ขออนุมัติ --</td></tr>";
                        }
                        else
                        {
                          $no=1;
                          foreach($rs as $r)
                          {
                            echo"<tr>";
                              echo"<td align='center'>$no</td>";
                              echo"<td>".$r['leave_date']."</td>";
                              echo"<td>".$r['leave_description']."</td>";
                              echo"<td>".$r['start_leave']."</td>";
                              echo"<td>".$r['end_leave']."</td>";
                              echo"<td>".$r['leave_hour']."</td>";
                              echo"<td>".$r['leave_reason']."</td>";
                              echo"<td><span class='status-metro status-suspended'><b>".$r['approve_desc']."</span></b></td>";
                              
                              //echo"<td><b>".$r['hr_confirm_desc']."</b></td>";

                              //echo'<td align="center"><button type="button" class="btn btn-primary">แก้ไข</button></td>';
                             
                              echo'<td align="center"><button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                              echo "'".base_url()."index.php/request_list_c/view_request_detail/".$r['trans_id']."'";
                              echo '">รายละเอียด</button></td>';
                              
                              //echo'<td align="center"><button type="button" class="btn btn-danger" ONCLICK="window.location.href=';
                              //echo "'".base_url()."index.php/company_c/del_comp/".$r['id']."'";
                              //echo '">ลบช้อมูล</button></td>';

                            echo"</tr>";
                            $no++;
                          }
                        }
                      ?>                     
                </tbody>                                 
            </table>
             </div>
        </div>
        </div>

            <!-- end table request -->

            </div>
<div id="placeholder" style="width:100%;height:200px;"></div>
           
                <!-- /END OF CONTENT -->

                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Oleave Managment
                        <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
                    <div class="devider-footer"></div>
                </div>
                <!-- / END OF FOOTER -->


            </div>
        </div>  <!--  END OF PAPER WRAP -->
    </div>
    <!--  END OF PAPER WRAP -->

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>




    <!-- GAGE -->

<!--
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/chart/jquery.flot.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/chart/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/chart/realTime.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/speed/canvasgauge-coustom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/countdown/jquery.countdown.js"></script>



    <script src="<?php echo base_url();?>assets/theme/js/jhere-custom.js"></script>

    <script>
    var gauge4 = new Gauge("canvas4", {
        'mode': 'needle',
        'range': {
            'min': 0,
            'max': 90
        }
    });
    gauge4.draw(Math.floor(Math.random() * 90));
    var run = setInterval(function() {
        gauge4.draw(Math.floor(Math.random() * 90));
    }, 3500);
    </script>


    <script type="text/javascript">
    /* Javascript
     *
     * See http://jhere.net/docs.html for full documentation
     */
    $(window).on('load', function() {
        $('#mapContainer').jHERE({
            center: [52.500556, 13.398889],
            type: 'smart',
            zoom: 10
        }).jHERE('marker', [52.500556, 13.338889], {
            icon: 'assets/img/marker.png',
            anchor: {
                x: 12,
                y: 32
            },
            click: function() {
                alert('Hallo from Berlin!');
            }
        })
            .jHERE('route', [52.711, 13.011], [52.514, 13.453], {
                color: '#FFA200',
                marker: {
                    fill: '#86c440',
                    text: '#'
                }
            });
    });
    </script>
    <script type="text/javascript">
    var output, started, duration, desired;

    // Constants
    duration = 5000;
    desired = '50';

    // Initial setup
    output = $('#speed');
    started = new Date().getTime();

    // Animate!
    animationTimer = setInterval(function() {
        // If the value is what we want, stop animating
        // or if the duration has been exceeded, stop animating
        if (output.text().trim() === desired || new Date().getTime() - started > duration) {
            console.log('animating');
            // Generate a random string to use for the next animation step
            output.text('' + Math.floor(Math.random() * 60)

            );

        } else {
            console.log('animating');
            // Generate a random string to use for the next animation step
            output.text('' + Math.floor(Math.random() * 120)

            );
        }
    }, 5000);
    </script>
    <script type="text/javascript">
    $('#getting-started').countdown('2015/01/01', function(event) {
        $(this).html(event.strftime(

            '<span>%M</span>' + '<span class="start-min">:</span>' + '<span class="start-min">%S</span>'));
    });
    </script>
 -->



</body>

</html>
