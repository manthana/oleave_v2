<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Approve List - Leave managemant system</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>/assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/font-awesome/css/font-awesome.min.css">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?>  
        
      <div id="page-wrapper">

        <div class="row">
          <div class="col-lg-12">
            
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Filter <a class="alert-link" href=""> เลือกตัวกรอง</a>
            </div>
          </div>
        </div><!-- /.row -->

      
      <div class="row">

          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> Approve request list ( รายการใบลา ที่รอ อนุมัติ )</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                      <tr>
                        <th>ลำดับที่<i class="fa fa-sort"></i></th>
                        <th>วันที่ลา <i class="fa fa-sort"></i></th>
                        <th>ประเภทการลา <i class="fa fa-sort"></i></th>
                        <th>เริ่ม <i class="fa fa-sort"></i></th>
                        <th>สิ้นสุด<i class="fa fa-sort"></i></th>
                        <th>จำนวนชั่วโมงที่ลา <i class="fa fa-sort"></i></th>
                        <th>เหตุผล <i class="fa fa-sort"></i></th>
                        <!-- <th>สถานะ <i class="fa fa-sort"></i></th> -->
                        <th>ผู้ขออนุมัติ <i class="fa fa-sort"></i></th>
                        <th>อนุมัติ <i class="fa fa-sort"></i></th>
                        <th>ไม่อนุมัติ <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                        if(count($rs)==0)
                        {
                          echo "<tr><td> -- no data --</td></tr>";
                        }
                        else
                        {
                          $no=1;
                          foreach($rs as $r)
                          {
                            echo"<tr>";
                              echo"<td align='center'>$no</td>";
                              echo"<td>".$r['leave_date']."</td>";
                              echo"<td>".$r['leave_description']."</td>";
                              echo"<td>".$r['start_leave']."</td>";
                              echo"<td>".$r['end_leave']."</td>";
                              echo"<td>".$r['leave_hour']."</td>";
                              echo"<td>".$r['leave_reason']."</td>";
                              //echo"<td><b>".$r['approve_desc']."</b></td>";
                              echo"<td><b>".$r['emp_name']."</b></td>";

                              //echo'<td align="center"><button type="button" class="btn btn-primary">แก้ไข</button></td>';
                             
                              echo'<td align="center"><button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                              echo "'".base_url()."index.php/approve_list_c/approve_leave/".$r['trans_id']."/".$r['request_email']."/".$r['leave_date']."/".$r['leave_description']."'";
                              echo '">อนุมัติ</button></td>';

                              echo'<td align="center"><button type="button" class="btn btn-danger" ONCLICK="window.location.href=';
                              echo "'".base_url()."index.php/approve_list_c/not_approve_leave/".$r['trans_id']."/".$r['request_email']."/".$r['leave_date']."/".$r['leave_description']."'";
                              echo '">ไม่อนุมัติ</button></td>';
                              
                              //echo'<td align="center"><button type="button" class="btn btn-danger" ONCLICK="window.location.href=';
                              //echo "'".base_url()."index.php/company_c/del_comp/".$r['id']."'";
                              //echo '">ลบช้อมูล</button></td>';

                            echo"</tr>";
                            $no++;
                          }
                        }
                      ?>
                       

                    </tbody>
                  </table>
                </div>
                <!-- <div class="text-right">
                  <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                </div> -->
              </div>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
    <script src="<?php echo base_url();?>/assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.js"></script>

    <!-- Page Specific Plugins -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/morris/chart-data-morris.js"></script>
    <script src="<?php echo base_url();?>/assets/js/tablesorter/jquery.tablesorter.js"></script>
    <script src="<?php echo base_url();?>/assets/js/tablesorter/tables.js"></script>

  </body>
</html>