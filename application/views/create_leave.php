<!DOCTYPE html>
<html lang="en">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Year Setting - Leave managemant system</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>

    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />

   

    <!-- date picker -->
    <link rel="stylesheet" href="assets/js/timepicker/bootstrap-timepicker.css">
    <link rel="stylesheet" href="assets/js/datepicker/datepicker.css">
    <link rel="stylesheet" href="assets/js/datepicker/clockface.css">
    <link rel="stylesheet" href="assets/js/datepicker/bootstrap-datetimepicker.css">

    <link rel="stylesheet" type="text/css" href="assets/js/tag/jquery.tagsinput.css">











    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>


                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
               
                <ul class="nav navbar-nav">
                </ul>
              


                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ คุณ <b><?php echo $emp_name;?>&nbsp;<?php echo $emp_lastname;?></b> <b class="caret"></b> 
                            
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                   
                    <!-- <li class="hidden-xs">
                        <a class="toggle-left" href="#">
                            <span style="font-size:20px;" class="entypo-list-add"></span>
                        </a>
                    </li> -->
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
    ?>

    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr" style = "min-height:600px;">




            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>Active Year
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                               <!--  ยินดีต้อนรับ คุณ &nbsp;
                                <strong><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></strong> -->
                                <!-- &nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM -->
                                
                            </div>


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        
                        

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Dashboard</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>

                
                <li class="pull-right">
                   <!--  <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div> -->
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->






           <!--  <div id="paper-middle">
                <div id="mapContainer"></div>
            </div> -->

            <!--  DEVICE MANAGER -->
            <div class="content-wrap">
                <!-- <div class="row">
                   
  

                </div> -->
            </div>
            <!--  / DEVICE MANAGER -->








            <div class="content-wrap">
            <!-- start table request-->
           
            
            <div class="row">
                  <?php echo form_open('create_leave_c/add_leave');?>
            <div class="col-lg-12">
            <div class="panel panel-info">

              <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <h3>Create leave request application ( สร้าง ใบลา เพื่อขออนุมัติ)</h3>
                    </div>  
                  </div>
                </div>

               <div class="panel-heading">

                              <div class="row">
                  <div class="col-xs-3">
                    <!-- <i class="fa fa-comments fa-5x"></i> -->
                    วันที่ลา
                  </div>
                    <div class="col-xs-9 text-left"/>
                     
                    <input type="text" class="form-control" id="dp1" value="01-01-2015">                  
                  
                 <!---<input type="text" size="40" id="datepicker_start" name="leave_date" required onchange="chooseDate()"/>
                <select class="form-control">
                  <option></option>
                  <option>ลาเป็นรายชั่วโมง</option>
                 </select> -->
                  </div>
                </div>
            </div>

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">


                    ประเภทการลา
                  </div>
                 
                  <div class="col-xs-9 text-right">

                <select class="form-control" id="leave_type" name="leave_type" Disabled>
<!--
<?php
//foreach ($result_leave_type->result() as $row)
{

    //  echo   '<option value = '.$row->type_id.'>'.$row->leave_description.'</option>';
}
?>
-->
<?php
foreach($result_leave_type as $r)
echo '<option value = '.$r['type_id'].'>'.$r['leave_description'].'</option>';;
?>
                </select>

                 

                  </div>
                </div>  

              </div>

               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    ระยะเวลาการลา
                  </div>
                  <div class="col-xs-9 text-right">

                <select class="form-control" id="working_hour" name = "working_hour" onchange="checkLeavePeriod()" Disabled>
<!--
<?php
//foreach ($result_working_hour->result() as $row)
{

  //    echo   '<option value = '.$row->working_hour_id.'>'.$row->work_description.'</option>';
}
?>
-->       
        <option value='-1'>เลือกช่วงระยะเวลาที่ต้องการลา
        </option>
        <?php
        foreach($result_working_hour as $r)
        echo '<option value = '.$r['working_hour'].'>'.$r['work_description'].'</option>';;
        ?>           
                </select>
                  </div>
                </div>  

              </div>


              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    ใส่เวลาเริ่มต้นที่ลา
                  </div>
                  <div class="col-xs-2 text-right">

                      <select class="form-control" id="leave_start" name="leave_start" onchange="checkTime()" DISABLED>
                      
                     
<!--
<?php
//foreach ($result_leave_time->result() as $row)
{

  //    echo   '<option value = '.$row->time_slot.'>'.$row->time_slot.'</option>';
}
?>  
-->
<?php
foreach($result_leave_time as $r)
echo '<option value = '.$r['time_slot'].'>'.$r['time_slot'].'</option>';;
?>   

                   
                  </select>
                  <input type="hidden" id="leave_start_hid" name="leave_start_hid">
                  </div>
                  <div class="col-xs-2" style="text-align:center;">
                   
                    ใส่เวลาสิ้นสุดที่ลา
                  </div>
                  <div class="col-xs-2 text-right">

                     <select class="form-control" id="leave_end" name="leave_end" onchange="checkTime()" DISABLED>
                     
<!--                 
<?php
//foreach ($result_leave_time->result() as $row)
{

//      echo   '<option>'.$row->time_slot.'</option>';
}
?>  
-->
<?php
foreach($result_leave_time as $r)
echo '<option value = '.$r['time_slot'].'>'.$r['time_slot'].'</option>';
?>   
        
                      </select>
                      <input type="hidden" id="leave_end_hid" name="leave_end_hid">
                  </div>
                </div>  

              </div>

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    รวมจำนวนชั่วโมงที่ขอลา
                  </div>
                  <div class="col-xs-3 text-right">
                  <input class="form-control" id="leave_hour" name="leave_hour" DISABLED>
                          
                   <input type="hidden" id="leave_hour_hid" name="leave_hour_hid">
                  
                  </div>
                  <div class="col-xs-3">
                    
                    ชั่วโมง
                  </div>
                  
                </div>  

              </div>

        <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                  เหตุผลในการลา
                  </div>
                  <div class="col-xs-9 text-right">

                    <textarea class="form-control" rows="3" name="leave_reason"></textarea>
                  </div>
                </div>  

              </div>

<!--                <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    เอกสารแนบ
                  </div>
                  <div class="col-xs-9 text-right">

                <input type="file">
                  </div>
                </div>  

              </div> -->

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                   <input type="hidden" name="emp_id" value="<?php echo $emp_id;?>"> 
                  </div>
                  <div class="col-xs-9 text-left">

                 
                  <input type="submit" name="btsave" class="btn btn-primary" id="btsave" value ="บันทึกข้อมูล ส่งใบลาเพื่อขออนุมัติ" DISABLED></button> 
                  </div>
                </div>  

              </div>















          </div>
          <?php echo form_close();?>  
                <!-- end of filter input เส้นคั้นก่อน footer-->
                </div>

              </div>
         </div><!-- /#page-wrapper -->


<!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Oleave Managment
                        <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->


                </div>  


                </div>
           

<!-- 
<div id="placeholder" style="width:100%;height:200px;"></div>
 -->

           
                <!-- /END OF CONTENT -->


                

            </div>
            </div>
            </div>
    <!--  END OF PAPER WRAP -->

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>



    <!-- script date picker -->
    <script type="text/javascript" src="assets/js/timepicker/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="assets/js/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/js/datepicker/clockface.js"></script>
    <script type="text/javascript" src="assets/js/datepicker/bootstrap-datetimepicker.js"></script>


    <script type="text/javascript" src="assets/js/tag/jquery.tagsinput.js"></script>

    <script type="text/javascript">
    $('#datetimepicker1').datetimepicker({
        language: 'pt-BR'
    });
    $('#dp1').datepicker()
    $('#dpYears').datepicker();
    $('#timepicker1').timepicker();
    $('#t1').clockface();
    $('#t2').clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });

    $('#toggle-btn').click(function(e) {
        e.stopPropagation();
        $('#t2').clockface('toggle');
    });
    </script>
    <script>
    $(document).ready(function() {
        //Validation
        $('#contact-form').validate({
            rules: {
                name: {
                    minlength: 2,
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                subject: {
                    minlength: 2,
                    required: true
                },
                message: {
                    minlength: 2,
                    required: true
                }
            },
            highlight: function(element) {
                $(element).closest('.control-group').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
            }
        });

        // MASKED INPUT

        $("#date").mask("99/99/9999", {
            completed: function() {
                alert("Your birthday was: " + this.val());
            }
        });
        $("#phone").mask("(999) 999-9999");

        $("#money").mask("99.999.9999", {
            placeholder: "*"
        });
        $("#ssn").mask("99--AAA--9999", {
            placeholder: "*"
        });


    <!-- /MAIN EFFECT -->

     <!-- JavaScript -->
   
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    
   <!-- JavaScript for check field rules--> 
    <script>
      function chooseDate() 
      {
        document.getElementById('working_hour').disabled = false;
        document.getElementById('leave_type').disabled = false;
      }
    </script>





    <!--
    <script type="text/javascript" src="assets/js/timepicker/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="assets/js/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/js/datepicker/clockface.js"></script>
    <script type="text/javascript" src="assets/js/datepicker/bootstrap-datetimepicker.js"></script>

    <script type="text/javascript" src="assets/js/tag/jquery.tagsinput.js"></script>

    <script type="text/javascript">
    $('#datetimepicker1').datetimepicker({
        language: 'pt-BR'
    });
    $('#dp1').datepicker()
    $('#dpYears').datepicker();
    $('#timepicker1').timepicker();
    $('#t1').clockface();
    $('#t2').clockface({
        format: 'HH:mm',
        trigger: 'manual'
    });

    $('#toggle-btn').click(function(e) {
        e.stopPropagation();
        $('#t2').clockface('toggle');
    });
    </script>
-->

    <!-- GAGE -->

<!--
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/chart/jquery.flot.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/chart/jquery.flot.resize.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/chart/realTime.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/speed/canvasgauge-coustom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/countdown/jquery.countdown.js"></script>



    <script src="<?php echo base_url();?>assets/theme/js/jhere-custom.js"></script>

    <script>
    var gauge4 = new Gauge("canvas4", {
        'mode': 'needle',
        'range': {
            'min': 0,
            'max': 90
        }
    });
    gauge4.draw(Math.floor(Math.random() * 90));
    var run = setInterval(function() {
        gauge4.draw(Math.floor(Math.random() * 90));
    }, 3500);
    </script>


    <script type="text/javascript">
    /* Javascript
     *
     * See http://jhere.net/docs.html for full documentation
     */
    $(window).on('load', function() {
        $('#mapContainer').jHERE({
            center: [52.500556, 13.398889],
            type: 'smart',
            zoom: 10
        }).jHERE('marker', [52.500556, 13.338889], {
            icon: 'assets/img/marker.png',
            anchor: {
                x: 12,
                y: 32
            },
            click: function() {
                alert('Hallo from Berlin!');
            }
        })
            .jHERE('route', [52.711, 13.011], [52.514, 13.453], {
                color: '#FFA200',
                marker: {
                    fill: '#86c440',
                    text: '#'
                }
            });
    });
    </script>
    <script type="text/javascript">
    var output, started, duration, desired;

    // Constants
    duration = 5000;
    desired = '50';

    // Initial setup
    output = $('#speed');
    started = new Date().getTime();

    // Animate!
    animationTimer = setInterval(function() {
        // If the value is what we want, stop animating
        // or if the duration has been exceeded, stop animating
        if (output.text().trim() === desired || new Date().getTime() - started > duration) {
            console.log('animating');
            // Generate a random string to use for the next animation step
            output.text('' + Math.floor(Math.random() * 60)

            );

        } else {
            console.log('animating');
            // Generate a random string to use for the next animation step
            output.text('' + Math.floor(Math.random() * 120)

            );
        }
    }, 5000);
    </script>
    <script type="text/javascript">
    $('#getting-started').countdown('2015/01/01', function(event) {
        $(this).html(event.strftime(

            '<span>%M</span>' + '<span class="start-min">:</span>' + '<span class="start-min">%S</span>'));
    });
    </script>
 -->



</body>

</html>
