<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Request Report - Leave managemant system</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/font-awesome/css/font-awesome.min.css">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
    <script src="<?php echo base_url();?>assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery-ui-1.11.0.js"></script>
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="<?php echo base_url();?>assets/js/script.js"></script>


    


  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?> 
        
      <div id="page-wrapper">

       <!-- filter section-->
       <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> เงื่อนไขในการค้นหาข้อมูล</h3>
              </div>
              <div class="panel-body">
                
                <div class="row">
                <!-- filter input-->
           <?php echo form_open('request_list_c/request_report_filter');?>
           <div class="col-lg-12">
            <div class="panel panel-info">

             
              
               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">
                   
                    ช่วงวันที่ลา เริ่มต้น
                  </div>
                  <div class="col-xs-3 text-left">

                <input type="text" size="10" id="datepicker_start" name="leave_date_start" required onchange="chooseDate()"/>
              
                  </div>
                  <div class="col-xs-2">
                   
                    ช่วงวันที่ลา สิ้นสุด
                  </div>
                  <div class="col-xs-3 text-left">

                <input type="text" size="10" id="datepicker_end" name="leave_date_end" required onchange="chooseDate()"/>
              
                  </div>
                </div>  

              </div>

           
               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">

                    ประเภทการลา
                  </div>
                  <div class="col-xs-5 text-right">

                <select class="form-control" id="leave_type" name="leave_type" onchange="setFilterLeaveType()">

<option value = '99'>ประเภท การลา ทั้งหมด</option>
<?php
foreach($result_leave_type as $r)
echo '<option value = '.$r['type_id'].'>'.$r['leave_description'].'</option>';;
?>
                </select>
<input type="hidden" id="leave_type_hid" name="leave_type_hid" value="<>">                 

                  </div>
                </div>

              </div>

              <div class="panel-heading">
                <div class="row">
                 

                   <div class="col-xs-2">

                    สถานะอนุมัติใบลา
                  </div>
                  <div class="col-xs-3 text-right">

                <select class="form-control" id="leave_approve" name="leave_approve" onchange="setFilterApprove()">
<option value = '99'>ทั้งหมด</option>
<?php
foreach($result_approve_stat as $r)
echo '<option value = '.$r['approve_id'].'>'.$r['approve_desc'].'</option>';;
?>
                </select>
    <input type="hidden" id="approve_stat_hid" name="approve_stat_hid" value="<>">
                </div>
                <div class="col-xs-2">

                    สถานะยกเลิกใบลา
                  </div>
                  <div class="col-xs-3 text-right">

                <select class="form-control" id="leave_reject" name="leave_reject" onchange="setFilterReject()">
<option value = '99'>ทั้งหมด</option>
<?php
foreach($result_reject_stat as $r)
echo '<option value = '.$r['id_reject'].'>'.$r['id_desc'].'</option>';;
?>
                </select>
<input type="hidden" id="reject_stat_hid" name="reject_stat_hid" value="<>">
                </div>
                </div>  





              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">
                   
                   <input type="hidden" name="emp_id" value="<?php echo $emp_id;?>"> 
                  </div>
                  <div class="col-xs-10 text-left">

                 <br/>
                  <input type="submit" name="btfind" class="btn btn-primary" id="btfind" value ="ค้นหาข้อมูลการลา"></button> 
                  </div>
                </div>  

              </div>




              
            </div>
          </div>
          <?php echo form_close();?>  
                <!-- end of filter input-->
                </div>

              </div>
            </div>
          </div>
       <!-- end of filter section-->

        <div class="row">

          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> Leave request report ( ประวัติการ ขอลางาน )</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped tablesorter" id="resultTb">
                    <thead>
                      <tr>
                        <th>ลำดับที่<i class="fa fa-sort"></i></th>
                        <th>วันที่ลา <i class="fa fa-sort"></i></th>
                        <th>ประเภทการลา <i class="fa fa-sort"></i></th>
                        <th>เริ่ม <i class="fa fa-sort"></i></th>
                        <th>สิ้นสุด<i class="fa fa-sort"></i></th>
                        <th>จำนวนชั่วโมงที่ลา <i class="fa fa-sort"></i></th>
                        <th>สถานะอนุมัติ <i class="fa fa-sort"></i></th>
                        <th>สถานะยกเลิก <i class="fa fa-sort"></i></th>
                        <!--<th>ฝ่ายบุคคล <i class="fa fa-sort"></i></th>-->
                        <th>ดูรายละเอียด <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                        if(count($rs)==0)
                        {
                          echo "<tr><td> -- ไม่มีข้อมูลใบลา ตรงตามเงื่อนไขที่ค้นหา --</td></tr>";
                        }
                        else
                        {
                          $no=1;
                          foreach($rs as $r)
                          {
                            echo"<tr>";
                              echo"<td align='center'>$no</td>";
                              echo"<td>".$r['leave_date']."</td>";
                              echo"<td>".$r['leave_description']."</td>";
                              echo"<td>".$r['start_leave']."</td>";
                              echo"<td>".$r['end_leave']."</td>";
                              echo"<td>".$r['leave_hour']."</td>";
                              echo"<td>".$r['approve_desc']."</td>";
                              echo"<td><b>".$r['id_desc']."</b></td>";
                              // echo"<td><b>".$r['hr_confirm_desc']."</b></td>";

                              //echo'<td align="center"><button type="button" class="btn btn-primary">แก้ไข</button></td>';
                             
                              echo'<td align="center"><button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                              echo "'".base_url()."index.php/request_list_c/report_request_detail/".$r['trans_id']."'";
                              echo '">รายละเอียด</button></td>';
                              
                              //echo'<td align="center"><button type="button" class="btn btn-danger" ONCLICK="window.location.href=';
                              //echo "'".base_url()."index.php/company_c/del_comp/".$r['id']."'";
                              //echo '">ลบช้อมูล</button></td>';

                            echo"</tr>";
                            $no++;
                          }
                        }
                      ?>
                       

                    </tbody>
                  </table>
                </div>
                 <div class="text-right">
                  <div class="export_excel" onclick="write_to_excel()";>export data to excel files <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- /.row -->

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->

    <script>
      function setFilterLeaveType(){
        
        var leave_type = document.getElementById('leave_type').value ;  
        var leave_type_filter = document.getElementById('leave_type_hid').value ;

        //alert(leave_type_filter);

        if (leave_type != 99) {
            document.getElementById('leave_type_hid').value = "="
            var new_leave_type_filter = document.getElementById('leave_type_hid').value
          // alert(new_leave_type_filter);
          // alert(leave_type);
        } else if(leave_type == 99) {
            document.getElementById('leave_type_hid').value = "<>"
            var new1_leave_type_filter = document.getElementById('leave_type_hid').value
          // alert(new1_leave_type_filter);
          // alert(leave_type);
        } 


      }
    </script>

    <script>
      function setFilterApprove(){
        
        var approve_stat = document.getElementById('leave_approve').value ;  
        var approve_stat_filter = document.getElementById('approve_stat_hid').value ;

       // alert(approve_stat_filter);

        if (approve_stat != 99) {
            document.getElementById('approve_stat_hid').value = "="
         //   var new_approve_stat_filter = document.getElementById('approve_stat_hid').value
         //  alert(new_approve_stat_filter);
         //  alert(approve_stat);
        } else if(approve_stat == 99) {
            document.getElementById('approve_stat_hid').value = "<>"
         //   var new1_approve_stat_filter = document.getElementById('approve_stat_hid').value
         //  alert(new1_approve_stat_filter);
         //  alert(approve_stat);
        } 


      }
    </script>

     <script>
      function setFilterReject(){
        
        var reject_stat = document.getElementById('leave_reject').value ;  
        var reject_stat_filter = document.getElementById('reject_stat_hid').value ;

        //alert(reject_stat_filter);
        //alert(reject_stat);

        if (reject_stat != 99) {
            document.getElementById('reject_stat_hid').value = "="
           // var new_reject_stat_filter = document.getElementById('reject_stat_hid').value
           //alert(new_reject_stat_filter);
           //alert(reject_stat);
        } else if(reject_stat == 99) {
            document.getElementById('reject_stat_hid').value = "<>"
           // var new1_reject_stat_filter = document.getElementById('reject_stat_hid').value
           //alert(new1_reject_stat_filter);
           //alert(reject_stat);
        } 


      }
    </script>

    <script>
      function write_to_excel() {
    alert('yee');
    str = "";
    var mytable = document.getElementById("resultTb");
    var rowCount = mytable.rows.length;
    var colCount = mytable.getElementsByTagName("tr")[0].getElementsByTagName("th").length;
    var ExcelApp = new ActiveXObject("Excel.Application");
    var ExcelSheet = new ActiveXObject("Excel.Sheet");
    //ExcelSheet.Application.Visible = true;
    for (var i = 0; i < rowCount; i++) {
        for (var j = 0; j < colCount; j++) {
            if (i == 0) {
                str = mytable.getElementsByTagName("tr")[i].getElementsByTagName("th")[j].innerText;
            }
            else {
                str = mytable.getElementsByTagName("tr")[i].getElementsByTagName("td")[j].innerText;
            }
            ExcelSheet.ActiveSheet.Cells(i + 1, j + 1).Value = str;
        }
    }
    ExcelSheet.autofit;
    ExcelSheet.Application.Visible = true;
    DisplayAlerts = true;
    CollectGarbage();
}
    </script>

    
    <script src="<?php echo base_url();?>/assets/js/bootstrap.js"></script>

    <!-- Page Specific Plugins -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/morris/chart-data-morris.js"></script>
    <script src="<?php echo base_url();?>/assets/js/tablesorter/jquery.tablesorter.js"></script>
    <script src="<?php echo base_url();?>/assets/js/tablesorter/tables.js"></script>

  </body>
</html>
