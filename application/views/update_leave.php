<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <title>Update Leave Category</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel="stylesheet">
  
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>/assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css"> 

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>

    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />

    <link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">


    <!-- Bootstrap core CSS -->
    <!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>/assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/font-awesome/css/font-awesome.min.css">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
   
  </head>
  <body>

  <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>


                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>
            </div>


      <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
              <ul class="nav navbar-nav">
                </ul>

                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                  <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ </b> <b class="caret"></b> 
                            
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>             
            </div><!-- /.navbar-collapse -->
       
      </div><!-- /.container-fluid -->
    </nav>


      <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?>

      <!--  PAPER WRAP -->
    <div class="wrap-fluid">
      <div class="container-fluid paper-wrap bevel tlbr" style = "min-height:600px;">


        <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>ประเภทวันลา
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>                 
                    </div>
                </div>
            </div><!--END OF TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Dashboard</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>
                
               <!--  <li class="pull-right">
                    <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div>
                </li> -->
            </ul>
            <!-- END OF BREADCRUMB -->


    <div class="content-wrap">
      <div class="col-sm-12">
        <div id="page-wrapper">
          <div class="nest" id="FootableClose">
           
            <div class="row">

              <div class="col-lg-12">

                <div class="panel panel-info">

              
               
            </div>

            
               
              </div>
            </div><!-- /.row -->


      <div class="row">
      <div class="col-lg-12">
      <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> แก้ไขข้อมูล ประเภทการลา </h3>
              </div>
        <div class="panel-body">
                
          <div class="row">
          <!-- start entry leave -->
        <?php echo form_open('company_c/update_leave/'.$rs['type_id']);?>
        <div class="col-lg-12">

            <div class="panel panel-info">
              <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <h3>Update Leave Category ( แก้ไขข้อมูล ประเภทวันลา )</h3>
                    </div>  
                  </div>
              </div>

              

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">                    
                    ประเภทการลา
                  </div>
                  <div class="col-xs-6 text-right">
                      <input class="form-control" id="leave_description" name ="leave_description" value = "<?php echo $rs['leave_description'];?>" required="">
                  </div>
                </div> 
              </div>



               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    จำนวนวัน ตามประเมินผล
                  </div>
                  <div class="col-xs-2 text-right">
                <input type ="number" step="0.5"  disabled="" class="form-control" required="" id = "bonus_limit_days" name = "bonus_limit_days" value="<?php echo $rs['bonus_limit_days'];?>" onchange="change_days()" >
                  </div>
                <div class="col-xs-1">
                    วัน
                </div>
                <div class="col-xs-3">
                    จำนวนชั่วโมง ตามประเมินผล
                </div>
                <div class="col-xs-2 text-right">
                <input type ="number" class="form-control" required=""  id = "bonus_limit" name = "bonus_limit" value="<?php echo $rs['bonus_limit'];?>" disabled="" >
                  </div>
                  <div class="col-xs-1">
                    ชั่วโมง
                  </div>
                </div>
              </div>


              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    จำนวนวัน ตามข้อกำหนดบริษัท
                  </div>                  
                  <div class="col-xs-2 text-right">
                <input type ="number" step="0.5" disabled="" class="form-control" required="" id="legal_limit_days" name = "legal_limit_days" value="<?php echo $rs['legal_limit_days'];?>" onchange="change_days()" >
                  </div>
                  <div class="col-xs-1">
                    วัน
                  </div>
                <div class="col-xs-3">
                    จำนวนชั่วโมง ตามข้อกำหนดบริษัท
                </div>
                <div class="col-xs-2 text-right">
               <input type ="number" class="form-control" required="" id="legal_limit" name = "legal_limit" value="<?php echo $rs['legal_limit'];?>" disabled="" >
                  </div>
                  <div class="col-xs-1">
                    ชั่วโมง
                  </div>
                </div>
              </div>



              <input type="number" id="bonus_limit_hid" name="legal_limit_hid" hidden="" value="<?php echo $rs['bonus_limit'];?>"> 
              <input type="number" id="legal_limit_hid" name="legal_limit_hid" hidden="" value="<?php echo $rs['legal_limit'];?>"> 

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">                    
                    แสดงในหน้าแรก
                  </div>
                  <div class="col-xs-2 text-right">
                      <select class="form-control" id="fav_flag" name = "fav_flag" >  
            <?php
            echo '<option value = '.$rs['fav_flag'].'>'.$rs['flag'].'</option>';;
            ?> 
                        <option value='0'>No</option>
                        <option value='1'>Yes</option>
                      </select>
                  </div>
                </div>
              </div>




              <div class="panel-heading">
                <div class="row">
                      <div class="col-xs-3">
                                           
                      </div>
                      <div class="col-xs-9 text-left">
                          <input type="submit" name="btedit" class="btn btn-primary" value ="บันทึก การแก้ไขข้อมูล"></button> 
                          &nbsp;&nbsp;&nbsp;
                          <button type="button" class="btn btn-danger" ONCLICK="window.location.href='<?php echo base_url();?>index.php/company_c/leave_show'">&nbsp;&nbsp;&nbsp;ยกเลิก การแก้ไข&nbsp;&nbsp;&nbsp;</button>
                      </div>
                </div>
              </div>              
            </div><!-- /class="panel panel-info" -->
            <?php echo form_close();?>       
                
        </div><!-- /class="col-lg-12"-->

          </div><!-- /class="row" -->
        </div><!-- /class="panel-body" -->

      </div><!-- /class="panel panel-primary" -->
      </div>
      </div>
    </div>
    </div>
    </div>
    </div> <!-- END OF class="content-wrap" -->    
     <!--  </div>  -->


            <!-- FOOTER -->
            <div class="footer-space"></div>
            <div id="footer">
              <div class="devider-footer-left"></div>
              <div class="time">
                  <p id="spanDate"></p>
                  <p id="clock"></p>
              </div>
              <div class="copyright">Oleave Managment
              <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
              <div class="devider-footer"></div>
            </div>
                <!-- / END OF FOOTER -->
        </div><!-- /class="nest" id="FootableClose" -->
      </div><!-- /id="page-wrapper" -->



  <!-- END OF RIGHT SLIDER CONTENT-->

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>


      <!-- MAIN EFFECT -->
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>


    <!-- JavaScript -->
    <script src="<?php echo base_url();?>/assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.js"></script>
      

    <!-- JavaScript -->
     <script> 
    function change_days(){
      // bonus limit
      var bonus_limit_day  = document.getElementById('bonus_limit_days').value;
      var bonus_limit  = document.getElementById('bonus_limit').value;
      document.getElementById('bonus_limit').value = bonus_limit_day*8;
      document.getElementById('bonus_limit_hid').value = bonus_limit_day*8;
      
      //new_bonus_limit = document.getElementById('bonus_limit').value;


      // legal limit
      var legal_limit_day  = document.getElementById('legal_limit_days').value;
      var legal_limit  = document.getElementById('legal_limit').value;
      document.getElementById('legal_limit').value = legal_limit_day*8;
      document.getElementById('legal_limit_hid').value = legal_limit_day*8;
      
      //new_legal_limit = document.getElementById('legal_limit').value;

   
      //alert("yes");
      //alert(legal_limit_day);
      //alert(legal_limit);
      //alert(new_legal_limit);
      

    }
   </script>

    <script src="<?php echo base_url();?>/assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.js"></script>
   

  </body>
</html>