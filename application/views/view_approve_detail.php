<!DOCTYPE html>
<html lang="en">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>view Approve Detail</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>

    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />

   

    <!-- date picker -->
    <link rel="stylesheet" href="assets/js/timepicker/bootstrap-timepicker.css">
    <link rel="stylesheet" href="assets/js/datepicker/datepicker.css">
    <link rel="stylesheet" href="assets/js/datepicker/clockface.css">
    <link rel="stylesheet" href="assets/js/datepicker/bootstrap-datetimepicker.css">

    <link rel="stylesheet" type="text/css" href="assets/js/tag/jquery.tagsinput.css">


    <!-- Add css and javascript for date pciker-->
    
    <link type="text/css" href="<?php echo base_url();?>assets/css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
    <script type="text/javascript">
     $(function () {
        var d = new Date();
        var toDay = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getFullYear() + 543);


        // กรณีต้องการใส่ปฏิทินลงไปมากกว่า 1 อันต่อหน้า ก็ให้มาเพิ่ม Code ที่บรรทัดด้านล่างด้วยครับ (1 ชุด = 1 ปฏิทิน)

       /* $("#datepicker-th").datepicker({ dateFormat: 'dd/mm/yy', isBuddhist: true, defaultDate: toDay, dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
              dayNamesMin: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.'],
              monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
              monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.']});

        $("#datepicker-th-2").datepicker({ changeMonth: true, changeYear: true,dateFormat: 'dd/mm/yy', isBuddhist: true, defaultDate: toDay,dayNames: ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์'],
              dayNamesMin: ['อา.','จ.','อ.','พ.','พฤ.','ศ.','ส.'],
              monthNames: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],
              monthNamesShort: ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.']});
       
         $("#inline").datepicker({ dateFormat: 'dd/mm/yy', inline: true });
          */

            $("#datepicker-en").datepicker({ dateFormat: 'yy-mm-dd 00:00:00'});

       


      });
    </script>

    <style type="text/css">

      .demoHeaders { margin-top: 2em; }
      #dialog_link {padding: .4em 1em .4em 20px;text-decoration: none;position: relative;}
      #dialog_link span.ui-icon {margin: 0 5px 0 0;position: absolute;left: .2em;top: 50%;margin-top: -8px;}
      ul#icons {margin: 0; padding: 0;}
      ul#icons li {margin: 2px; position: relative; padding: 4px 0; cursor: pointer; float: left;  list-style: none;}
      ul#icons span.ui-icon {float: left; margin: 0 4px;}
      ul.test {list-style:none; line-height:30px;}
    </style>
    <!-- end of date picker-->


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>


                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
               
                <ul class="nav navbar-nav">
                </ul>
              


                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ คุณ <b><?php echo $emp_name;?>&nbsp;<?php echo $emp_lastname;?></b> <b class="caret"></b> 
                            
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                   
                    <!-- <li class="hidden-xs">
                        <a class="toggle-left" href="#">
                            <span style="font-size:20px;" class="entypo-list-add"></span>
                        </a>
                    </li> -->
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

   <!-- Sidebar -->
      <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?>

    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr" style = "min-height:600px;">




            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>leave application request
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                               <!--  ยินดีต้อนรับ คุณ &nbsp;
                                <strong><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></strong> -->
                                <!-- &nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM -->
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        
                        

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Dashboard</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>

                
                <li class="pull-right">
                   <!--  <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div> -->
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <!--  / DEVICE MANAGER -->



            <div class="content-wrap">
            <!-- start table request-->
        <div id="page-wrapper">


        <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> ข้อมูลรายละเอียดใบลา </h3>
              </div>
              <div class="panel-body">
                
                <div class="row">

          <!-- start entry leave -->
          
          <?php echo form_open('approve_list_c/not_approve_leave');?>
           <div class="col-lg-12">
            <div class="panel panel-info">

              <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <h3>leave application request ( ข้อมูล รายละเอียด ใบลา ที่ขออนุมัติ )</h3>
                    </div>  
                  </div>
                </div>

              <div class="panel-heading">
                <div class="row">
                    
                    <div class="col-xs-3">
                      ชื่อผู้ขอลา
                    </div>
                    <div class="col-xs-6 text-left">
                      <input class="form-control" id="inputSuccess" type="text" value="<?php echo $rs['emp_name'];?> <?php echo $rs['emp_lastname'];?>" disabled="">
                    </div>
                    
                </div>  

              </div>



              <div class="panel-heading">
                <div class="row">
                    <input type="hidden" id="trans_id_hid" name="trans_id_hid" value="<?php echo $rs['trans_id'];?>">
                   
                    <input type="hidden" id="reject_stat_hid" name="reject_stat_hid">
                    <input type="hidden" id="reject_date_hid" name="reject_date_hid">
                    <input type="hidden" id="approve_stat_hid" name="approve_stat_hid" value="<?php echo $rs['approve_stat'];?>">

                    <input type="hidden" id="request_email_hid" name="request_email_hid" value="<?php echo $rs['request_email'];?>">
                    <input type="hidden" id="leave_date_hid" name="leave_date_hid" value="<?php echo $rs['leave_date'];?>">
                    <input type="hidden" id="leave_description_hid" name="leave_description_hid" value="<?php echo $rs['leave_description'];?>">
                    
                    
                    <div class="col-xs-3">
                      สถานะของใบลา
                    </div>
                    <div class="col-xs-3 text-left">
                      <input class="form-control" id="inputSuccess" type="text" value="<?php echo $rs['approve_desc'];?>" disabled="">
                    </div>
                    <div class="col-xs-1">
                      วันที่ลา
                    </div>
                    <div class="col-xs-2 text-center">
                      <input class="form-control" name="approve_desc" value="<?php echo $rs['leave_date'];?>" disabled="">
                    </div>
                </div>  

              </div>

            
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">

                    ประเภทการลา
                  </div>
                  <div class="col-xs-3 text-right">
                   <input class="form-control" name="approve_desc" value="<?php echo $rs['leave_description'];?>" disabled="">
                  </div>
                   <div class="col-xs-1">
                      จำนวน
                    </div>
                    <div class="col-xs-2 text-center">
                      <input class="form-control" name="approve_desc" value="<?php echo $rs['leave_hour'];?>" disabled="">
                    </div>
                    <div class="col-xs-1">
                      ชั่วโมง
                    </div>
                </div>  

              </div>

             <!--   <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    ระยะเวลาการลา
                  </div>
                  <div class="col-xs-9 text-right">

                <select class="form-control" name = "working_hour" disabled="" >

                <option><?php //echo $rs['work_description'];?></option>

                </select>
                  </div>
                </div>  

              </div> -->

               

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                  เวลาเริ่มต้นที่ลา
                  </div>
                  <div class="col-xs-2 text-right">

                  <select class="form-control" name="leave_start" disabled="" >

                    <option><?php echo $rs['start_leave'];?></option>
                        
                  </select>
                  </div>
                  <div class="col-xs-2" style="text-align:center;">
                   
                  เวลาสิ้นสุดที่ลา
                  </div>
                  <div class="col-xs-2 text-right">

                      <select class="form-control" name="leave_end" disabled="" >
                    
                        <option><?php echo $rs['end_leave'];?></option>                 
        
                      </select>
                  </div>
                </div>  

              </div>

             <!--  <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    รวมจำนวนชั่วโมงที่ขอลา
                  </div>
                  <div class="col-xs-3 text-right">
                  <input class="form-control" name="leave_hour" value="<?php //echo $rs['leave_hour'];?>" disabled="">
                  
                  </div>
                  <div class="col-xs-3">
                    
                    ชั่วโมง
                  </div>
                  
                </div>  

              </div> -->

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                  เหตุผลในการลา
                  </div>
                  <div class="col-xs-9 text-right">

                    <textarea class="form-control" rows="3" name="leave_reason" disabled="" ><?php echo $rs['leave_reason'];?></textarea>
                  </div>
                </div>  

              </div>
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                  เหตุผลที่ไม่อนุมัติใบลา
                  </div>
                  <div class="col-xs-9 text-right">

                    <textarea class="form-control" required="" rows="3" id ="reject_leave_reason" name="reject_leave_reason" onchange="check_reason()" placeholder="ใส่เหตุผลที่ไม่อนุมัติ เพื่อเปิดปุ่มไม่อนุมัติใบลา" ></textarea>
                  </div>
                </div>  

              </div>

<!--                <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    เอกสารแนบ
                  </div>
                  <div class="col-xs-9 text-right">

                <input type="file">
                  </div>
                </div>  

              </div> -->

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                   
                   <input type="hidden" name="emp_id" value="<?php echo $emp_id;?>"> 

                  </div>
                  <div class="col-xs-3 text-left">

                 
                  <!--<input type="button" name="btback" class="btn btn-primary" value ="กลับไปหน้า รายการใบลา"></button>-->
                  <?php
                  echo'<button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                  echo "'".base_url()."index.php/approve_list_c'";
                  echo '">กลับไปหน้า รายการใบลา</button></td>';
                  ?>
                  
                  
                  </div>
                  <div class="col-xs-3 text-left">

                  <!--
                  <input type="submit" id ="btcancel" name="btcancel" class="btn btn-danger" value ="ขอยกเลิก รายการใบลานี้" disabled=""></button> 
                  -->
                  
                  <?php
                  echo'<button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                  echo "'".base_url()."index.php/approve_list_c/approve_leave/".$rs['trans_id']."/".$rs['request_email']."/".$rs['leave_date']."/".$rs['leave_description']."'";
                  echo '">อนุมัติ ใบรายการขอลา</button>';
                  ?>
                  </div>
                  
                  <div class="col-xs-3 text-left">
                  <!--
                  <?php
                  //echo'<button type="button" class="btn btn-danger" id ="btcancel" name="btcancel" ONCLICK="window.location.href=';
                  //echo "'".base_url()."index.php/approve_list_c/not_approve_leave/".$rs['trans_id']."/".$rs['request_email']."/".$rs['leave_date']."/".$rs['leave_description']."'";
                  //echo '">ไม่อนุมัติ ใบรายการขอลา</button>';
                  ?>
                  -->
                  <input type="submit" class="btn btn-danger" id ="btcancel" name="btcancel" value="ไม่อนุมัติ ใบรายการขอลา" ></button>
                  <!--
                  <input type="submit" name="btsave" class="btn btn-primary" id="btsave" value ="บันทึกข้อมูล ส่งใบลาเพื่อขออนุมัติ" DISABLED></button> 
                  -->
                  </div>
                </div>  

              </div>


              
            </div>
          </div>
          <?php echo form_close();?>  
          <!-- -->

        </div>

              </div>
            </div>
          </div>

      <!-- end of my page-->


             </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
   
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
   
    <script>
        function getDate(){

          

          //alert("load");
         // dd = Date();
          var d = new Date();
          var curr_date = d.getDate();
          var curr_month = d.getMonth();
          var curr_year = d.getFullYear();
          //reject_date_hid
          var curr_month_add = curr_month+1;

          //alert(curr_month);
          //alert(curr_month_add);
          if (curr_month_add<10){
              var current_date = curr_year+'-0'+curr_month_add+'-'+curr_date;            
          } else{
              var current_date = curr_year+'-'+curr_month_add+'-'+curr_date;
          }

          //alert(current_date);
          document.getElementById('reject_date_hid').value = current_date;

         
         


          var approve_stat = document.getElementById('approve_stat_hid').value;

          // alert(approve_stat);

          if (approve_stat == 0 ) {
           
            document.getElementById('reject_stat_hid').value = 1;
            //alert("app start = 1");

          } else if (approve_stat == 1 ) {
           
            document.getElementById('reject_stat_hid').value = 2;
            //alert("app start = 1");

          } else if (approve_stat == 2){
            //alert("app start = 2");
            document.getElementById('reject_leave_reason').disabled = true ;
          }

           document.getElementById('btcancel').disabled = true;
          
          
         

          

        }
    </script>

    <script>
    function check_reason(){
      var reject_reason = document.getElementById('reject_leave_reason').value ;
      //var len_reason = reject_reason;
      //alert(reject_reason);

      if (reject_reason = null){
          document.getElementById('btcancel').disabled = true;
          //alert("null");
        }  else{
          document.getElementById('btcancel').disabled = false;
          //alert("not");
        }
      
      
      
    }
    </script>

    <script>
    function check_value(){
     a = document.getElementById('trans_id_hid').value 
     b = document.getElementById('request_email_hid').value 
     c = document.getElementById('leave_date_hid').value 
     d = document.getElementById('leave_description_hid').value 
     e = document.getElementById('reject_leave_reason').value 
     alert(a);
     alert(b);
     alert(c);
     alert(d);
     alert(e);
    }
    </script>


<!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Oleave Managment
                        <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
                    <div class="devider-footer"></div>


                </div>
                <!-- / END OF FOOTER -->


                </div>  


                </div>
           

<!-- 
<div id="placeholder" style="width:100%;height:600px;"></div> -->


           
                <!-- /END OF CONTENT -->


                

            </div>
            </div>
            </div>
    <!--  END OF PAPER WRAP -->

    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>

</body>

</html>
