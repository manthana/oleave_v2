<!DOCTYPE html>
<html lang="en">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Year Setting - Leave managemant system</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/loader-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.css">

    <style type="text/css">
    canvas#canvas4 {
        position: relative;
        top: 20px;
    }
    </style>

    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.core.css?v=2-0-1" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/theme/js/footable/css/footable-demos.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/lib/jquery.dataTables/css/DT_bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/theme/js/dataTable/css/datatables.responsive.css" />


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/theme/ico/minus.png">
</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>


                <div id="logo-mobile" class="visible-xs">
                    <h1>Oleave
                        <span>version 2.0</span>
                    </h1>
                </div>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
               
                <ul class="nav navbar-nav">
                </ul>
              

                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <!-- <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/10.jpg">ยินดีต้อนรับ คุณ <b><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></b> <b class="caret"></b> -->
                            <img alt="" class="admin-pic img-circle" src="https://www.jobbkk.com/upload/employer/0E/F8E/00CF8E/images/53134.gif">ยินดีต้อนรับ คุณ <b><?php echo $emp_name;?>&nbsp;<?php echo $emp_lastname;?></b> <b class="caret"></b> 
                            
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <li>
                                <a href="#">
                                    <span class="entypo-user"></span>&#160;&#160;My Profile</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-vcard"></span>&#160;&#160;Account Setting</a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;Help</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <span class="entypo-logout"></span>&#160;&#160;Logout</a>
                            </li>
                        </ul>
                    </li>
                   
                    <!-- <li class="hidden-xs">
                        <a class="toggle-left" href="#">
                            <span style="font-size:20px;" class="entypo-list-add"></span>
                        </a>
                    </li> -->
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

     <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
      ?>

    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr" style = "min-height:600px;">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-window"></i> 
                            <span>Active Year
                            </span>
                        </h2>

                    </div>


                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                               <!--  ยินดีต้อนรับ คุณ &nbsp;
                                <strong><?php //echo $emp_name;?>&nbsp;<?php //echo $emp_lastname;?></strong> -->
                                <!-- &nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM -->
                                
                            </div>


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                        
                        

                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Home</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Dashboard</a>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li><a href="#" title="Sample page 1">Britishdispensary Group Online leave management system </a>
                </li>

                
                <li class="pull-right">
                   <!--  <div class="input-group input-widget">

                        <input style="border-radius:15px" type="text" placeholder="Search..." class="form-control">
                    </div> -->
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


           <!--  <div id="paper-middle">
                <div id="mapContainer"></div>
            </div> -->

            <!--  DEVICE MANAGER -->
            <div class="content-wrap">

                <!-- <div class="row">
                  

                </div> -->
            </div>
            <!--  / DEVICE MANAGER -->

            <div class="content-wrap">
            <!-- start table request-->
                     
            <div class="col-sm-12">

                <div id="page-wrapper">

                        <div class="row">
          <div class="col-lg-12">
            
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              ข้อมูลส่วนของ <a class="alert-link" href=""> ชื่อ ที่อยู่ ของบริษัท</a>
            </div>
          </div>
        </div><!-- /.row -->



        <div class="row">


          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> เปลี่ยนปีทำงาน ปัจจุบัน</h3>
              </div>
              <div class="panel-body">
                
                <div class="row">
                <!-- filter input-->

<?php echo form_open('company_c/change_active_year');?>

            <div class="col-lg-12">
            <div class="panel panel-info">

             
              
               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">
                   
                    ปีทำงานปัจจุบัน
                  </div>
              <div class="col-xs-2 text-left">

              <input type = "hidden" class="active_year" size="15" id="active_year" name="active_year"  disabled="" value="<?php
              foreach($rs_year_active as $r)
              echo ''.$r['id_year'].'';
              ?>">
              
              <input type = "text" class="active_year_show" size="15" id="active_year_show" name="active_year_show"  disabled="" value="<?php
              foreach($rs_year_active as $r)
              echo ''.$r['year'].'';
              ?>">
               

              </div>
                  <div class="col-xs-2">
                   
                    วันที่ลา เริ่มต้น
                  </div>
                  <div class="col-xs-2 text-left">

              <input type = "text" class="start_date" size="15" id="start_date" name="start_date" disabled="" value="<?php
              foreach($rs_year_active as $r)
              echo ''.$r['start_date'].'';
              ?>">

                  </div>
                   <div class="col-xs-2">
                   
                    วันที่ลา สิ้นสุด
                  </div>
                  <div class="col-xs-2 text-left">

                <input type = "text" class="end_date" size="15" id="end_date" name="end_date" disabled="" value="<?php
              foreach($rs_year_active as $r)
              echo ''.$r['end_date'].'';
              ?>">
                  </div>
                </div>  

              </div>


              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">

                    เลือกปีปัจจุบันใหม่
                  </div>
                  <div class="col-xs-6 text-right">

                <select class="form-control" id="form-control" name="new_year" >

                <?php
                foreach($rs_year_no_active as $r)
                echo '<option value = '.$r['id_year'].'>ปี '.$r['year'].' ( ช่วงวันที่ '.$r['start_date'].' ถึง '.$r['end_date'].' )</option>';;
                ?>

                </select>


                  </div>
                </div>

              </div>
<input id="active_year_hid" name="active_year_hid" value="" type="hidden">
<input id="new_year_hid" name="new_year_hid" value="" type="hidden">

<?php echo form_close();?>

        

        <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-2">
                   
                   <input name="emp_id" value="1921" type="hidden"> 
                  </div>
                  <div class="col-xs-6 text-left">

                 <br>
                  <input name="btsave" class="btn btn-primary" id="btsave" value="บันทึกเปลี่ยนปีปัจจุบันใหม่" type="submit" onclick="change_year()"> 
                  </div>
                </div>  

              </div>




              
            </div>
          </div>
            
                <!-- end of filter input-->
                </div></form>

              </div>
            </div>
          </div>
       <!-- end of filter section-->



        <!-- end of change active year-->

          <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                
                  <h3 class="panel-title"><i class="fa fa-edit"></i> Active Year ( ปีทำงานปัจจุบัน )</h3>
                  <br>
                  <button type="button" class="btn btn-default" ONCLICK="window.location.href='<?php echo base_url();?>index.php/company_c/add_comp'">เพิ่ม ช่วงปีทำงาน</button>
                
                
              </div>

              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped tablesorter">
                    <thead>
                      <tr>
                        <th>ลำดับที่<i class="fa fa-sort"></i></th>
                        <th>ปี <i class="fa fa-sort"></i></th>
                        <th>วันที่เริ่มต้น <i class="fa fa-sort"></i></th>
                        <th>วันที่สิ้นสุด <i class="fa fa-sort"></i></th>
                        <th>ปีทำงานปัจจุบัน <i class="fa fa-sort"></i></th>
                        <th>แก้ไขข้อมูล <i class="fa fa-sort"></i></th>
                        <th>ลบข้อมูล <i class="fa fa-sort"></i></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
                        if(count($rs)==0)
                        {
                          echo "<tr><td> -- no data --</td></tr>";
                        }
                        else
                        {
                          $no=1;
                          foreach($rs as $r)
                          {
                            echo"<tr>";
                              echo"<td align='center'>$no</td>";
                              echo"<td>".$r['year']."</td>";
                              echo"<td>".$r['start_date']."</td>";
                              echo"<td>".$r['end_date']."</td>";
                              echo"<td>".$r['year_active']."</td>";
                              //echo'<td align="center"><button type="button" class="btn btn-primary">แก้ไข</button></td>';
                             
                              echo'<td align="center"><button type="button" class="btn btn-primary" ONCLICK="window.location.href=';
                              echo "'".base_url()."index.php/company_c/update_comp/".$r['id_year']."'";
                              echo '">แก้ไขช้อมูล</button></td>';
                              
                              echo'<td align="center"><button type="button" class="btn btn-danger" disabled="" ONCLICK="window.location.href=';
                              echo "'".base_url()."index.php/company_c/del_comp/".$r['id_year']."'";
                              echo '">ลบช้อมูล</button></td>';

                            echo"</tr>";
                            $no++;
                          }
                        }
                      ?>
                       

                    </tbody>
                  </table>
                </div>
                <!-- <div class="text-right">
                  <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                </div> -->
              </div>
            </div>
          </div>
        </div><!-- /.row --> 
    </div><!-- /#page-wrapper -->


            </div>
            </div>       
           
                <!-- /END OF CONTENT -->

                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate"></p>
                        <p id="clock"></p>
                    </div>
                    <div class="copyright">Oleave Managment
                        <span class="entypo-copyright"></span>@ 2015 <a href="http://www.medeeem.com/leave"></a> All Rights Reserved</div>
                    <div class="devider-footer"></div>
                </div><!-- / END OF FOOTER -->

            </div>
        </div><!-- End class="wrap-fluid" -->
    
    <!--  END OF PAPER WRAP -->
     


    <!-- END OF RIGHT SLIDER CONTENT-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/src/jquery.velocity.min.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/number-pb.js"></script>
    <script src="<?php echo base_url();?>assets/theme/js/progress-bar/progress-app.js"></script>



    <!-- MAIN EFFECT -->
   
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/preloader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/load.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/theme/js/main.js"></script>


    
    <!-- /MAIN EFFECT -->

     <!-- JavaScript -->

      <script>
    function change_year(){
      
      new_year  = document.getElementById('form-control').value 
      document.getElementById('new_year_hid').value  =  new_year

      active_year  = document.getElementById('active_year').value 
      document.getElementById('active_year_hid').value  =  active_year
      
      //year_hid = document.getElementById('new_year_hid').value
      //active_year_hid = document.getElementById('active_year_hid').value
      //alert(year_hid)
      //alert("yes");
      //alert(year_hid);
      //alert(active_year_hid);
      //alert(active_year_hid);

    }
   </script>


   
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    
   <!-- JavaScript for check field rules--> 
    <script>
      function chooseDate() 
      {
        document.getElementById('working_hour').disabled = false;
        document.getElementById('leave_type').disabled = false;
      }
    </script>

</body>

</html>
