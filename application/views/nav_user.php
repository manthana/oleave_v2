

    <!-- SIDE MENU -->
    <div id="skin-select">
        <div id="logo">
            <h1>Oleave
                <span>v2.0</span>
            </h1>
        </div>

        <a id="toggle">
            <span class="entypo-menu"></span>
        </a>
        <div class="dark">
            
        </div>

        <!-- <div class="search-hover">
            <form id="demo-2">
                <input type="search" placeholder="Search Menu..." class="id_search">
            </form>
        </div> -->




        <div class="skin-part">
            <div id="tree-wrap">
                <div class="side-bar">
                    <ul class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span style="font-size: small;">เมนูหลัก ใบลา</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>

                        
                        <li>
                            <a class="tooltip-tip ajax-load" href="<?php echo base_url();?>dashboard_c" title="Social">
                                <i class="icon-monitor"></i>
                                <span>หน้าหลัก</span>

                            </a>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="<?php echo base_url();?>create_leave_c" title="Social">
                                <i class="icon-document-edit"></i>
                                <span>เขียนใบลา</span>

                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip ajax-load" href="#" title="Blog App">
                                <i class="icon-mail"></i>
                                <span>ใบลาที่ขออนุมัติ</span>

                            </a>
                            <ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="<?php echo base_url();?>request_list_c" title="Blog List"><i class="entypo-doc-text"></i><span>ขออนุมัติ 
                                    <?php
                                      foreach($rs_count_request as $r)
                                      echo '( '.$r['leave_num'].' )';
                                    ?>
                                    </span></a>
                                </li>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="<?php echo base_url();?>request_list_c/approved_request_list" title="Blog Detail"><i class="entypo-newspaper"></i><span>ผ่านการ อนุมัติแล้ว 
                                    <?php
                                      foreach($rs_count_approved_request as $r)
                                      echo '( '.$r['leave_num'].' )';
                                    ?></span></a>
                                </li>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="<?php echo base_url();?>request_list_c/cancel_request_list" title="Blog Detail"><i class="entypo-newspaper"></i><span>รายการที่ขอยกเลิก</span></a>
                                </li>
                            </ul>
                        </li>
                        
                    </ul>

                    <ul class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span style="font-size: small;">อนุมัติ ใบลา</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>

                        
                        <li>
                            <a class="tooltip-tip ajax-load" href="<?php echo base_url();?>approve_list_c" title="Social">
                                <i class="icon-monitor"></i>
                                <span>รายการใบลาที่ รออนุมัติ 
                        <?php
                            foreach($rs_count_approve as $r)
                            echo '( '.$r['leave_num'].' )';
                        ?>
                                </span>

                            </a>
                        </li>
                        

                        <li>
                            <a class="tooltip-tip ajax-load" href="#" title="Blog App">
                                <i class="icon-mail"></i>
                                <span>รายงานการลา</span>

                            </a>
                            <ul>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="<?php echo base_url();?>request_list_c/request_report" title="Blog List"><i class="entypo-doc-text"></i><span>ประวัติการ ขอลางาน</span></a>
                                </li>
                                <li>
                                    <a class="tooltip-tip2 ajax-load" href="<?php echo base_url();?>approve_list_c/approve_report" title="Blog Detail"><i class="entypo-newspaper"></i><span>ประวัติการ อนุมัติใบลา</span></a>
                                </li>
                                
                            </ul>
                        </li>
                        
                    </ul>

                  

                     <ul class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">

                                <span style="font-size: small;">รายละเอียดอื่นๆ</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>

                            </a>
                        </li>

                        
                        <li>
                            <a class="tooltip-tip ajax-load" href="<?php echo base_url();?>employee_c" title="Social">
                                <i class="icon-monitor"></i>
                                <span>เปลี่ยนรหัสผ่าน</span>

                            </a>
                        </li>
                        <li>
                            <a class="tooltip-tip ajax-load" href="<?php echo base_url();?>dashboard_c/logout" title="Social">
                                <i class="icon-document-edit"></i>
                                <span>ออกจากระบบ</span>


                            </a>
                        </li>
                        
                    </ul>

                   
                </div>
            </div>
        </div>
    </div>
