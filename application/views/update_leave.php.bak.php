<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Update Leave Category</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel="stylesheet">
  
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>/assets/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/font-awesome/css/font-awesome.min.css">
    

   
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <?php 
      if ($role_id == 1) {
        $this->load->view('nav_user');
      } else if ($role_id > 1) {
        $this->load->view('nav');
      }
         
      ?>



      <div id="page-wrapper">


        <div class="col-lg-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-edit"></i> แก้ไขข้อมูล ประเภทการลา </h3>
              </div>
              <div class="panel-body">
                
                <div class="row">


          <!-- start entry leave -->
        <?php echo form_open('company_c/update_leave/'.$rs['type_id']);?>
           <div class="col-lg-12">

            <div class="panel panel-info">

              <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                      <h3>Update Leave Category ( แก้ไขข้อมูล ประเภทวันลา )</h3>
                    </div>  
                  </div>
                </div>

              

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    ประเภทการลา
                  </div>
                  <div class="col-xs-6 text-right">
                      <input class="form-control" id="leave_description" name ="leave_description" value = "<?php echo $rs['leave_description'];?>" required="">
                  </div>
                </div>  

              </div>

               <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    จำนวนวัน ตามประเมินผล
                  </div>
                  <div class="col-xs-2 text-right">

                <input type ="number" step="0.5"  disabled="" class="form-control" required="" id = "bonus_limit_days" name = "bonus_limit_days" value="<?php echo $rs['bonus_limit_days'];?>" onchange="change_days()" >
                  </div>
                <div class="col-xs-1">
                    วัน
                  </div>

                <div class="col-xs-3">
                    จำนวนชั่วโมง ตามประเมินผล
                </div>
                <div class="col-xs-2 text-right">

                <input type ="number" class="form-control" required=""  id = "bonus_limit" name = "bonus_limit" value="<?php echo $rs['bonus_limit'];?>" disabled="" >
                  </div>

                  <div class="col-xs-1">
                    ชั่วโมง
                  </div>
                </div>  

              </div>


              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    จำนวนวัน ตามข้อกำหนดบริษัท
                  </div>
                  
                  <div class="col-xs-2 text-right">

                <input type ="number" step="0.5" disabled="" class="form-control" required="" id="legal_limit_days" name = "legal_limit_days" value="<?php echo $rs['legal_limit_days'];?>" onchange="change_days()" >
                  </div>

                  <div class="col-xs-1">
                    วัน
                  </div>

                <div class="col-xs-3">
                    จำนวนชั่วโมง ตามข้อกำหนดบริษัท
                </div>
                <div class="col-xs-2 text-right">

               <input type ="number" class="form-control" required="" id="legal_limit" name = "legal_limit" value="<?php echo $rs['legal_limit'];?>" disabled="" >
                  </div>
                  <div class="col-xs-1">
                    ชั่วโมง
                  </div>
                </div>  

              </div>

              <input type="number" id="bonus_limit_hid" name="legal_limit_hid" hidden="" value="<?php echo $rs['bonus_limit'];?>"> 
              <input type="number" id="legal_limit_hid" name="legal_limit_hid" hidden="" value="<?php echo $rs['legal_limit'];?>"> 

              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    
                    แสดงในหน้าแรก
                  </div>
                  <div class="col-xs-2 text-right">
                      <select class="form-control" id="fav_flag" name = "fav_flag" >
      

<?php

echo '<option value = '.$rs['fav_flag'].'>'.$rs['flag'].'</option>';;
?> 

<option value='0'>No</option>
<option value='1'>Yes</option>
                </select>
                  </div>
                </div>  

              </div>

              <div class="panel-heading">
                <div class="row">
                      <div class="col-xs-3">
                    
                        
                      </div>
                      <div class="col-xs-9 text-left">

                          <input type="submit" name="btedit" class="btn btn-primary" value ="บันทึก การแก้ไขข้อมูล"></button> 
                          &nbsp;&nbsp;&nbsp;
                          <button type="button" class="btn btn-danger" ONCLICK="window.location.href='<?php echo base_url();?>index.php/company_c/leave_show'">&nbsp;&nbsp;&nbsp;ยกเลิก การแก้ไข&nbsp;&nbsp;&nbsp;</button>
                      </div>
                </div>  

              </div>
            </div>
  
        <?php echo form_close();?>       
          <!-- -->

        </div>

              </div>
            </div>
          </div>

      <!-- end of my page-->


      
        

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->
     <script> 
    function change_days(){
      // bonus limit
      var bonus_limit_day  = document.getElementById('bonus_limit_days').value;
      var bonus_limit  = document.getElementById('bonus_limit').value;
      document.getElementById('bonus_limit').value = bonus_limit_day*8;
      document.getElementById('bonus_limit_hid').value = bonus_limit_day*8;
      
      //new_bonus_limit = document.getElementById('bonus_limit').value;


      // legal limit
      var legal_limit_day  = document.getElementById('legal_limit_days').value;
      var legal_limit  = document.getElementById('legal_limit').value;
      document.getElementById('legal_limit').value = legal_limit_day*8;
      document.getElementById('legal_limit_hid').value = legal_limit_day*8;
      
      //new_legal_limit = document.getElementById('legal_limit').value;

   
      //alert("yes");
      //alert(legal_limit_day);
      //alert(legal_limit);
      //alert(new_legal_limit);
      

    }
   </script>

    <script src="<?php echo base_url();?>/assets/js/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.js"></script>
   

  </body>
</html>